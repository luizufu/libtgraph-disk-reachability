#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <iomanip>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class ttc_level_fenwick
{
    using vec = disk::vector<uint32_t, Alloc, PAGE_SIZE>;
    std::vector<uint32_t> _levels;

public:
    struct header
    {
        typename vec::header header = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
        std::array<uint32_t, (PAGE_SIZE - 36) / 4> levels;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c, Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i, const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto add_(const header& h, uint32_t u, uint32_t v, uint32_t t1,
                     uint32_t t2, Alloc<PAGE_SIZE>* alloc) -> bool
    {
        return add(h, u, v, t1, t2, alloc);
    }

    static auto sum_(const header& h, uint32_t u, uint32_t v, uint32_t t1,
                     const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        return sum(h, u, v, t1, alloc);
    }

    static auto find_(const header& h, uint32_t u, uint32_t v, uint32_t t2,
                      const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        return find(h, u, v, t2, alloc);
    }

    static void print_(const header& h, uint32_t u, uint32_t v,
                       const Alloc<PAGE_SIZE>& alloc)
    {
        for(uint32_t i = 0; i < h.tau + 1; ++i)
            std::cout << std::setw(3) << i << ", ";
        std::cout << '\n' << std::endl;

        // for(uint32_t i = 0; i < h.tau; ++i)
        //     std::cout << std::setw(3) << inverse(i, h.tau) << ", ";
        // std::cout << '\n' << std::endl;

        std::vector<uint32_t> tplus(h.tau);
        vec::read_n(h.header, access(u, v, 0, h), h.tau, tplus.data(), alloc);
        for(auto& t: tplus)
        {
            // if(t == h.tau)
            // {
            //     --t;
            // }
            std::cout << std::setw(3) << (t) << ", ";
        }
        std::cout << std::endl;

        for(uint32_t i = 0; i < h.tau; ++i)
        {
            auto opt = sum(h, u, v, i, alloc);
            std::string str = opt ? std::to_string(*opt) : "-1";
            std::cout << std::setw(3) << str << ", ";
        }
        std::cout << std::endl;

        // std::cout << std::setw(5) << " ";
        for(uint32_t i = 0; i < h.tau + 1; ++i)
        {
            auto opt = find(h, u, v, i, alloc);
            std::string str = opt ? std::to_string(*opt) : "-1";
            std::cout << std::setw(3) << str << ", ";
        }
        std::cout << std::endl;
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif
private:
    static auto access(uint64_t u, uint64_t v, uint64_t t, const header& h)
        -> uint64_t
    {
        return (u * h.n + v) * h.tau + t;
    }

    static auto inverse(uint32_t i, uint32_t bit_size) -> uint32_t
    {
        return (bit_size - i - 1);
    }

    static auto rho(uint64_t word) -> int
    {
        return __builtin_ctzll(word);
    }
    static auto lambda(uint64_t word) -> int
    {
        return 63 ^ __builtin_clzll(word);
    }
    static auto clear_rho(uint64_t word) -> uint64_t
    {
#ifdef __haswell__
        return _blsr_u64(word);
#else
        return word & (word - 1);
#endif
    }
    static auto mask_rho(uint64_t word) -> uint64_t
    {
        return word & (-word);
    }
    static auto mask_lambda(uint64_t word) -> uint64_t
    {
        return 0x8000000000000000ULL >> __builtin_clzll(word);
    }

    static auto sum(const header& h, uint32_t u, uint32_t v, uint32_t tminus,
                    const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        if(tminus >= h.tau)
        {
            return std::nullopt;
        }

        std::vector<uint64_t> indexes;
        {
            uint64_t first = access(u, v, 0, h);
            uint32_t i = inverse(tminus, h.tau);
            for(++i; i > 0; i = clear_rho(i))
            {
                uint32_t height = rho(i);
                uint32_t level_idx = i >> (height + 1);
                indexes.push_back(first + h.levels[height] + level_idx);
            }
        }

        uint32_t tplus = h.tau;
        if(!indexes.empty())
        {
            uint32_t cur = 0;
            vec::read_lazy(
                h.header, indexes[cur],
                [&tplus, &indexes,
                 &cur](const uint32_t& val) -> std::optional<uint64_t>
                {
                    tplus = std::min(tplus, val);
                    ++cur;

                    return (cur < indexes.size())
                               ? std::make_optional(indexes[cur])
                               : std::nullopt;
                },
                alloc);
        }
        tplus += h.delta;
        return tplus < h.tau + h.delta ? std::make_optional(tplus)
                                       : std::nullopt;
    }

    static auto add(const header& h, uint32_t u, uint32_t v, uint32_t tminus,
                    uint32_t tplus, Alloc<PAGE_SIZE>* alloc) -> bool
    {
        if(tminus >= h.tau || tplus >= h.tau + h.delta)
        {
            return false;
        }

        std::vector<uint64_t> indexes;
        {
            uint64_t first = access(u, v, 0, h);
            uint32_t i = inverse(tminus, h.tau);
            for(++i; i <= h.tau; i += mask_rho(i))
            {
                uint32_t height = rho(i);
                uint32_t level_idx = i >> (height + 1);
                indexes.push_back(first + h.levels[height] + level_idx);
            }
        }

        bool ret = false;
        if(!indexes.empty())
        {
            tplus -= h.delta;
            uint32_t cur = 0;
            vec::write_lazy(
                h.header, indexes[0],
                [&indexes, &cur, &ret,
                 tplus](uint32_t* val) -> std::optional<uint64_t>
                {
                    if(tplus < *val)
                    {
                        *val = tplus;
                        ret = true;
                    }
                    ++cur;

                    return (cur < indexes.size())
                               ? std::make_optional(indexes[cur])
                               : std::nullopt;
                },
                alloc);
        }

        return ret;
    }

    static auto find(const header& h, uint32_t u, uint32_t v, uint32_t tplus,
                     const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        if(tplus >= h.tau + h.delta)
        {
            return std::nullopt;
        }

        uint64_t first = first = access(u, v, 0, h);
        uint32_t tminus = 0;
        uint32_t height = lambda(h.tau) + 1;
        uint32_t level_idx = 0;
        vec::read_lazy(
            h.header, first + h.levels[height - 1] + level_idx,
            [first, &tminus, &height, &level_idx, tplus,
             h](const uint32_t& val) -> std::optional<uint64_t>
            {
                level_idx <<= 1U;
                if(val + h.delta > tplus)
                {
                    ++level_idx;
                    tminus += 1U << (height - 1);
                }

                do
                {
                    --height;
                } while(height > 0
                        && h.levels[height - 1] + level_idx > h.tau);

                return (height > 0) ? std::make_optional(
                           first + h.levels[height - 1] + level_idx)
                                    : std::nullopt;
            },
            alloc);

        return tminus < h.tau ? std::make_optional(inverse(tminus, h.tau))
                              : std::nullopt;
    }
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-level-fenwick.ixx>
