#include <algorithm>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                        uint32_t delta,
                                        Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    h.set_header = set::create(alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto baseline<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                        uint32_t delta, It contacts_begin,
                                        It contacts_end,
                                        Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    // TODO add bulk_load operation for btree
    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        add_contact(&h, *it, alloc);
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void baseline<Alloc, PAGE_SIZE>::destroy(header* h, Alloc<PAGE_SIZE>* alloc)
{
    set::destroy(&h->set_header, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void baseline<Alloc, PAGE_SIZE>::add_contact(header* h, contact c,
                                             Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

#ifdef BENCHMARK_ON
    ++total_inserts;
    ++total_binary_searches;
#endif
    set::insert(&h->set_header, {u, v, t}, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::can_reach(const header& h, uint32_t u,
                                           uint32_t v, interval i,
                                           const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return construct_trivial(h, u, v, i, alloc).count(v) > 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    auto trivial_journeys = construct_trivial(h, u, v, i, alloc);

    if(trivial_journeys.count(v) == 0)
    {
        return {};
    }

    journey j = trivial_journeys[v].back();

    while(j.from() != u)
    {
        auto& vec = trivial_journeys[j.from()];

        for(auto it = vec.rbegin(); it != vec.rend(); ++it)
        {
            if(it->arrival() <= j.departure())
            {
                j = concatenate(*it, j);
                break;
            }
        }
    }

    return j;
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void baseline<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_binary_searches = 0;
    total_sequential_searches = 0;
    total_inserts = 0;
}

#endif

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto baseline<Alloc, PAGE_SIZE>::construct_trivial(
    const header& h, uint32_t source, uint32_t target, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> journey_map
{
    journey_map map;

    auto is_concatenable = [&map, source](const journey& candidate) -> bool
    {
        if(auto it = map.find(candidate.from()); it != map.end())
        {
            return std::any_of(it->second.begin(), it->second.end(),
                               [source, candidate](const journey& old)
                               {
                                   return old.arrival()
                                          <= candidate.departure();
                               });
        }

        return false;
    };

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    auto it = set::lower_bound(h.set_header, {source, 0, i.left},
                               std::less<contact>(), alloc);
    auto end = set::end(h.set_header, alloc);

    while(it != end && it->u != source)
    {
        ++it;
    }

    for(; it != end && it->t + h.delta < i.right; ++it)
    {
        journey j(*it, h.delta);

        if(it->u == source || is_concatenable(j))
        {
            map[it->v].push_back(j);

            if(it->v == target)
            {
                break;
            }
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    return map;
}

} // namespace tgraph_disk_reachability
