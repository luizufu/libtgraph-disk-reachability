#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    uint64_t size = static_cast<uint64_t>(n)
                    * static_cast<uint64_t>(tau)
                    * static_cast<uint64_t>(n);
    h.out = vec::create(size, alloc);
    h.in = vec::create(size, alloc);
    vec::fill(h.in, {NIL, n}, alloc);
    vec::fill(h.out, {NIL, n}, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, It contacts_begin,
    It contacts_end, Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        if(vec::read(h.out, out_access(u, t, v, h), *alloc).t
           != t + h.delta)
        {
            std::vector<std::pair<uint32_t, entry>> out;
            out.push_back({v, entry {t + h.delta, v}});

            std::vector<std::pair<uint32_t, entry>> in;
            {
                std::vector<entry> row(h.n);
                if(t >= h.delta)
                {
                    for(uint32_t wminus = 0; wminus < h.n; ++wminus)
                    {
                        if(wminus != u || wminus != v)
                        {
                            row[wminus] = vec::read(
                                h.in, in_access(u, t, wminus, h),
                                *alloc);
                        }
                    }
                }
                row[u] = {t, v};

                for(uint32_t wminus = 0; wminus < h.n; ++wminus)
                {
                    if(wminus != v && row[wminus].t != NIL)
                    {
                        in.emplace_back(wminus, row[wminus]);
                    }
                }
            }

            for(const auto& [wminus, e]: in)
            {
                std::deque<std::tuple<uint32_t, uint32_t, uint32_t>> q;
                for(const auto& [wplus, e2]: out)
                {
                    q.emplace_back(e.t, wplus, e2.t);
                }

                while(!q.empty())
                {
                    auto& [tminus, wplus, tplus] = q.back();
                    q.pop_back();

                    uint64_t idx = out_access(wminus, tminus, wplus, h);
                    uint32_t t2 = vec::read(h.out, idx, *alloc).t;
                    if(t2 == NIL || tplus < t2)
                    {
                        vec::write(h.out, idx, {tplus, e.succ}, alloc);

                        if(tminus > 0)
                        {
                            q.emplace_front(tminus - 1, wplus, tplus);
                        }
                    }
                }
            }

            for(const auto& [wplus, e]: out)
            {
                std::deque<std::tuple<uint32_t, uint32_t, uint32_t>> q;
                for(const auto& [wminus, e2]: in)
                {
                    q.emplace_back(e.t, wminus, e2.t);
                }

                while(!q.empty())
                {
                    auto& [tplus, wminus, tminus] = q.front();
                    q.pop_front();

                    uint64_t idx = in_access(wplus, tplus, wminus, h);
                    uint32_t t1 = vec::read(h.in, idx, *alloc).t;
                    if(t1 == NIL || tminus > t1)
                    {
                        vec::write(h.in, idx, {tminus, e.succ}, alloc);
                        if(tplus + 1 < h.tau + h.delta)
                        {
                            q.emplace_back(tplus + 1, wminus, tminus);
                        }
                    }
                }
            }
        }
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_bycol<Alloc, PAGE_SIZE>::destroy(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->in, alloc);
    vec::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_bycol<Alloc, PAGE_SIZE>::clear(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    vec::fill(*h->in, {NIL, h->n}, alloc);
    vec::fill(*h->out, {NIL, h->n}, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_bycol<Alloc, PAGE_SIZE>::add_contact(
    header* h, contact c, Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    if(vec::read(h->out, out_access(u, t, v, *h), *alloc).t
       != t + h->delta)
    {
        std::vector<std::pair<uint32_t, entry>> out;
        {
            std::vector<entry> row(h->n);
            if(t + h->delta < h->tau)
            {
                for(uint32_t wplus = 0; wplus < h->n; ++wplus)
                {
                    if(wplus != u && wplus != v)
                    {
                        row[wplus] = vec::read(
                            h->out,
                            out_access(v, t + h->delta, wplus, *h),
                            *alloc);
                    }
                }
            }
            row[v] = {t + h->delta, v};

            for(uint32_t wplus = 0; wplus < h->n; ++wplus)
            {
                if(wplus != u && row[wplus].t != NIL)
                {
                    out.emplace_back(wplus, row[wplus]);
                }
            }
        }

        std::vector<std::pair<uint32_t, entry>> in;
        {
            std::vector<entry> row(h->n);
            if(t >= h->delta)
            {
                for(uint32_t wminus = 0; wminus < h->n; ++wminus)
                {
                    if(wminus != u && wminus != v)
                    {
                        row[wminus] = vec::read(
                            h->in, in_access(u, t, wminus, *h), *alloc);
                    }
                }
            }
            row[u] = {t, v};

            for(uint32_t wminus = 0; wminus < h->n; ++wminus)
            {
                if(wminus != v && row[wminus].t != NIL)
                {
                    in.emplace_back(wminus, row[wminus]);
                }
            }
        }

        std::vector<entry> tmp(h->tau);
        for(const auto& [wminus, eminus]: in)
        {
            auto [tminus, succ] = eminus;
            for(const auto& [wplus, eplus]: out)
            {
                auto [tplus, _] = eplus;

                auto it = vec::iterator_at(
                    h->out, out_access(wminus, tminus, wplus, *h),
                    alloc);

                for(uint32_t i = tminus + 1;
                    i > 0 && (it->t == NIL || tplus < it->t); --i, ++it)
                {
                    *it = {tplus, succ};
                }
                it.flush();
            }
        }

        for(const auto& [wplus, eplus]: out)
        {
            auto [tplus, succ] = eplus;
            for(const auto& [wminus, eminus]: in)
            {
                auto [tminus, _] = eminus;

                auto it = vec::iterator_at(
                    h->in, in_access(wplus, tplus, wminus, *h), alloc);

                for(uint32_t i = tplus;
                    i < h->tau && (it->t == NIL || tminus > it->t);
                    ++i, ++it)
                {
                    *it = {tminus, succ};
                }
                it.flush();
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return std::min(
               vec::read(h.out, out_access(u, i.left, v, h), alloc).t,
               h.tau + h.delta - 1)
           < i.right;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    auto [tplus, _] =
        vec::read(h.out, out_access(u, i.left, v, h), alloc);
    if(std::min(tplus, h.tau + h.delta - 1) >= i.right)
    {
        return {};
    }

    auto [tminus, succ] =
        vec::read(h.in, in_access(v, tplus, u, h), alloc);
    journey j({u, succ, tminus}, h.delta);

    while(succ != v)
    {
        auto [next_tminus, next_succ] =
            vec::read(h.in, in_access(v, tplus, succ, h), alloc);

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::sequential_searches()
    -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_bycol<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_bycol<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
}

#endif

} // namespace tgraph_disk_reachability
