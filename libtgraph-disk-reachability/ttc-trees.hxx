#pragma once

#include <libtgraph-disk-reachability/detail/rttree.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

namespace tgraph_disk_reachability
{
template<template<typename, template<size_t> typename, size_t> class Graph,
         template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class ttc_trees
{
    using tree = detail::rttree<uint32_t, Alloc, PAGE_SIZE>;
    using graph = Graph<typename tree::header, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        typename graph::header graph_header = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c, Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i, const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    static auto rttree_header(header* h, uint32_t u, uint32_t v,
                              Alloc<PAGE_SIZE>* alloc) ->
        typename tree::header;
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-trees.ixx>
