#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <deque>
#include <fastpfor/codecfactory.h>
// #include <fastpfor/deltautil.h>
#include <iomanip>
#include <iostream>

namespace tgraph_disk_reachability
{
template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class ttc_compressed
{
    using vec_block = disk::vector<uint32_t, Alloc, PAGE_SIZE>;
    using vec_header =
        disk::vector<typename vec_block::header, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        typename vec_header::header in = {};
        typename vec_header::header out = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
        uint32_t z = 1;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta, uint32_t z,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta, uint32_t z,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c, Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i, const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto space(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> uint64_t
    {
        uint64_t total = 0;

        {
            auto it = vec_header::begin(h.in, alloc);
            auto end = vec_header::end(h.in, alloc);

            for(; it != end; ++it)
            {
                total += sizeof(*it) + it->size * sizeof(uint32_t);
            }
        }

        {
            auto it = vec_header::begin(h.out, alloc);
            auto end = vec_header::end(h.out, alloc);

            for(; it != end; ++it)
            {
                total += sizeof(*it) + it->size * sizeof(uint32_t);
            }
        }

        return total;
    }

    static auto print(const header& h, const Alloc<PAGE_SIZE>& alloc)
    {
        // uint32_t nil = std::numeric_limits<uint32_t>::max();

        // std::cout << "\nout   ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << "    ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << '\n' << std::endl;

        // for(uint32_t u = 0; u < h.n; ++u)
        // {
        //     for(uint32_t t = 0; t < h.tau; ++t)
        //     {
        //         std::cout << std::setw(6) << std::left
        //                   << (std::to_string(u) + "-" + std::to_string(t));
        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h), alloc);
        //             if(tplus == nil)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right << tplus;
        //                 std::cout << " ";
        //             }
        //         }

        //         std::cout << "    ";

        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h), alloc);
        //             if(next == h.n)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right << next;
        //                 std::cout << " ";
        //             }
        //         }

        //         std::cout << std::endl;
        //     }
        // }

        // // std::cout << "\nin    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << "    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << '\n' << std::endl;
        // // for(uint32_t v = 0; v < h.n; ++v)
        // // {
        // //     for(uint32_t t = 0; t < h.tau; ++t)
        // //     {
        // //         std::cout << std::setw(6) << std::left
        // //                   << (std::to_string(v) + "-" +
        // std::to_string(t));
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h.n, h.tau),
        // //                 alloc);
        // //             if(tminus == nil)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // tminus;
        // //                 std::cout << " ";
        // //             }
        // //         }
        // //         std::cout << "    ";
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h),
        // //                 alloc);
        // //             if(next == h.n)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right << next;
        // //                 std::cout << " ";
        // //             }
        // //         }

        // //         std::cout << std::endl;
        // //     }
        // // }
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static auto opt_space() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static uint64_t total_opt_space = 0;
#endif
private:
    [[nodiscard]] static auto out_block_location(uint64_t u, uint64_t tminus,
                                                 const header& h)
        -> std::pair<uint64_t, uint32_t>
    {
        uint64_t block_id = u * std::ceil(h.tau / static_cast<double>(h.z))
                            + (h.tau - (tminus + 1)) / h.z;
        uint32_t i = (h.tau - (tminus + 1)) % h.z;

        return std::make_pair(block_id, i);
    }

    [[nodiscard]] static auto in_block_location(uint64_t v, uint64_t tplus,
                                                const header& h)
        -> std::pair<uint64_t, uint32_t>
    {
        uint64_t block_id = v * std::ceil(h.tau / static_cast<double>(h.z))
                            + (tplus - h.delta) / h.z;
        uint32_t i = (tplus - h.delta) % h.z;

        return std::make_pair(block_id, i);
    }

    static void compress_block(const uint32_t* in, size_t n_in, uint32_t* out,
                               size_t& n_out)
    {
        using namespace FastPForLib;
        IntegerCODEC& codec = *CODECFactory::getFromName(Compressor);
        codec.encodeArray(in, n_in, out, n_out);
    }

    static void decompress_block(const uint32_t* in, size_t n_in,
                                 uint32_t* out, size_t& n_out)
    {
        using namespace FastPForLib;
        IntegerCODEC& codec = *CODECFactory::getFromName(Compressor);
        codec.decodeArray(in, n_in, out, n_out);
    }

    [[nodiscard]] static auto
    is_same_header(const typename vec_block::header& h1,
                   const typename vec_block::header& h2) -> bool
    {
        return h1.head == h2.head && h1.size == h2.size
               && h1.capacity == h2.capacity;
    }
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-compressed.ixx>
