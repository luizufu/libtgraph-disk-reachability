#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/detail/compressor-map.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <deque>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <span>

namespace tgraph_disk_reachability
{

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE = 4096>
class ttc_compressed6
{
    using vec_block = disk::vector<uint32_t, Alloc, PAGE_SIZE>;
    using vec_header =
        disk::vector<typename vec_block::header, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        typename vec_header::header in = {};
        typename vec_header::header out = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
        uint32_t z = 1;
        uint32_t levels = 1;
        uint32_t n_nodes = 1;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c,
                            Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v,
                          interval i, const Alloc<PAGE_SIZE>& alloc)
        -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u,
                                    uint32_t v, interval i,
                                    const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto space(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> uint64_t
    {
        uint64_t total = 0;

        {
            auto it = vec_header::begin(h.in, alloc);
            auto end = vec_header::end(h.in, alloc);

            for(; it != end; ++it)
            {
                total += sizeof(*it) + it->size * sizeof(uint32_t);
            }
        }

        {
            auto it = vec_header::begin(h.out, alloc);
            auto end = vec_header::end(h.out, alloc);

            for(; it != end; ++it)
            {
                total += sizeof(*it) + it->size * sizeof(uint32_t);
            }
        }

        return total;
    }

    static auto print(const header& h, const Alloc<PAGE_SIZE>& alloc)
    {
        // uint32_t nil = std::numeric_limits<uint32_t>::max();

        // std::cout << "\nout   ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << "    ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << '\n' << std::endl;

        // for(uint32_t u = 0; u < h.n; ++u)
        // {
        //     for(uint32_t t = 0; t < h.tau; ++t)
        //     {
        //         std::cout << std::setw(6) << std::left
        //                   << (std::to_string(u) + "-" +
        //                   std::to_string(t));
        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h),
        //                 alloc);
        //             if(tplus == nil)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right <<
        //                 tplus; std::cout << " ";
        //             }
        //         }

        //         std::cout << "    ";

        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h),
        //                 alloc);
        //             if(next == h.n)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right <<
        //                 next; std::cout << " ";
        //             }
        //         }

        //         std::cout << std::endl;
        //     }
        // }

        // // std::cout << "\nin    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << "    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << '\n' << std::endl;
        // // for(uint32_t v = 0; v < h.n; ++v)
        // // {
        // //     for(uint32_t t = 0; t < h.tau; ++t)
        // //     {
        // //         std::cout << std::setw(6) << std::left
        // //                   << (std::to_string(v) + "-" +
        // std::to_string(t));
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h.n,
        // h.tau),
        // //                 alloc);
        // //             if(tminus == nil)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // tminus;
        // //                 std::cout << " ";
        // //             }
        // //         }
        // //         std::cout << "    ";
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h),
        // //                 alloc);
        // //             if(next == h.n)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // next;
        // //                 std::cout << " ";
        // //             }
        // //         }

        // //         std::cout << std::endl;
        // //     }
        // // }
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static auto opt_space() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static uint64_t total_opt_space = 0;
#endif
private:
    struct location
    {
        uint32_t _z;
        uint32_t _div;
        uint64_t base;
        uint32_t _id;
        uint32_t pos;
        uint32_t _i;

        location() = default;
        location(uint32_t w, uint32_t t, const header& h)
            : _z(h.z)
            , _div(std::pow(_z, h.levels - 1))
            , base(w * h.n_nodes)
            , _id(0)
            , pos(t / _div)
            , _i(t % _div)
        {
            _div /= _z;
        }

        [[nodiscard]] auto id() const -> uint64_t
        {
            return base + _id;
        }

        auto next(uint32_t p) const -> location
        {
            location other;
            other._z = _z;
            other.base = base;
            other._id = _id * _z + p + 1;
            other.pos = p > pos ? 0 : _i / _div;
            other._i = p > pos ? 0 : _i % _div;
            other._div = _div / _z;

            return other;
        }

        [[nodiscard]] auto has_next() const -> bool
        {
            return _div > 0;
        }
    };

    inline static struct buffer
    {
        std::vector<uint32_t> original = {};
        std::vector<uint32_t> compressed = {};

        void resize(size_t size)
        {
            original.resize(size + 32);
            compressed.resize(size + 1024);
        }
    } buf;

    static void read_block(const header& h,
                           const typename vec_block::header& hb,
                           const Alloc<PAGE_SIZE>& alloc)
    {
        vec_block::read_n(hb, 0, hb.size, buf.compressed.data(), alloc);

        size_t size = h.n * (h.z + 1);
        codec_map(Compressor)
            .decodeArray(buf.compressed.data(), hb.size,
                         buf.original.data(), size);

        for(uint32_t i = 0; i < h.n; ++i)
        {
            std::partial_sum(&buf.original[i * (h.z + 1) + 1],
                             &buf.original[(i + 1) * (h.z + 1)],
                             &buf.original[i * (h.z + 1) + 1]);
        }
    }

    static auto write_block(const header& h,
                            const typename vec_block::header& hb,
                            Alloc<PAGE_SIZE>* alloc) ->
        typename vec_block::header
    {
        for(uint32_t i = 0; i < h.n; ++i)
        {
            std::adjacent_difference(&buf.original[i * (h.z + 1) + 1],
                                     &buf.original[(i + 1) * (h.z + 1)],
                                     &buf.original[i * (h.z + 1) + 1]);
        }

        size_t size = h.n * (h.z + 1);
        codec_map(Compressor)
            .encodeArray(buf.original.data(), size,
                         buf.compressed.data(), size);

        auto tmp = hb;
        vec_block::resize(&tmp, size, alloc);
        vec_block::write_n(tmp, 0, size, buf.compressed.data(), alloc);

        return tmp;
    }

    static void get_adjacency(const header& h, uint32_t w1,
                              const uint32_t* w2s, uint32_t t1,
                              uint32_t* t2s, uint32_t n, bool out,
                              const Alloc<PAGE_SIZE>& alloc)
    {
        location loc(w1, t1, h);
        auto header = out ? h.out : h.in;
        read_block(h, vec_header::read(header, loc.id(), alloc), alloc);

        for(uint32_t i = 0; i < n; ++i)
        {
            t2s[i] += buf.original[w2s[i] * (h.z + 1) + loc.pos];
        }

        if(auto loc2 = loc.next(loc.pos); loc2.pos > 0)
        {
            std::vector<std::pair<uint32_t, uint32_t>> remaining;
            for(uint32_t i = 0; i < n; ++i)
            {
                if(buf.original[w2s[i] * (h.z + 1) + loc.pos + 1]
                   > buf.original[w2s[i] * (h.z + 1) + loc.pos])
                {
                    remaining.emplace_back(
                        i, buf.original[w2s[i] * (h.z + 1) + loc.pos]);
                }
            }

            if(!remaining.empty())
            {
                read_block(h,
                           vec_header::read(header, loc2.id(), alloc),
                           alloc);

                for(auto& [i, parent]: remaining)
                {
                    uint32_t old_parent =
                        buf.original[w2s[i] * (h.z + 1)];
                    if(old_parent
                           + buf.original[w2s[i] * (h.z + 1) + loc2.pos]
                       > parent)
                    {
                        t2s[i] += old_parent
                                  + buf.original[w2s[i] * (h.z + 1)
                                                 + loc2.pos]
                                  - parent;
                    }
                }
            }
        }
    }

    static void update_adjacency(const header& h, uint32_t w1,
                                 const uint32_t* w2s, uint32_t t1,
                                 const uint32_t* t2s, uint32_t n,
                                 bool out, Alloc<PAGE_SIZE>* alloc)
    {
        location loc(w1, t1, h);
        auto header = out ? h.out : h.in;

        auto hb = vec_header::read(header, loc.id(), *alloc);
        read_block(h, hb, *alloc);

        uint32_t max_j = 0;
        bool updated_i1 = false;
        std::vector<std::pair<typename vec_block::header, uint64_t>>
            updated_headers;
        std::vector<std::pair<uint32_t, uint32_t>> remaining;
        for(uint32_t i = 0; i < n; ++i)
        {
            uint32_t j = loc.pos;
            if(t2s[i] > buf.original[w2s[i] * (h.z + 1) + j])
            {
                bool updated_i1_local = false;
                if(loc.next(loc.pos).pos == 0)
                {
                    updated_i1_local = true;
                    buf.original[w2s[i] * (h.z + 1) + j] = t2s[i];
                }

                ++j;

                for(; j < (h.z + 1)
                      && t2s[i] > buf.original[w2s[i] * (h.z + 1) + j];
                    ++j)
                {
                    buf.original[w2s[i] * (h.z + 1) + j] = t2s[i];
                }

                if(buf.original[w2s[i] * (h.z + 1) + loc.pos + 1]
                   > buf.original[w2s[i] * (h.z + 1) + loc.pos])
                {
                    remaining.emplace_back(
                        i, buf.original[w2s[i] * (h.z + 1) + loc.pos]);
                }

                max_j = std::max(j, max_j);
                updated_i1 |= updated_i1_local;
            }
        }

        if(updated_i1 || max_j > loc.pos + 1)
        {
            auto tmp = write_block(h, hb, alloc);
            if(!is_same_header(tmp, hb))
            {
                updated_headers.push_back({tmp, loc.id()});
            }
        }

        if(!remaining.empty())
        {
            auto loc2 = loc.next(loc.pos);
            hb = vec_header::read(header, loc2.id(), *alloc);
            read_block(h, hb, *alloc);

            for(auto [i, parent]: remaining)
            {
                if(uint32_t old_parent =
                       buf.original[w2s[i] * (h.z + 1)];
                   old_parent != parent)
                {
                    buf.original[w2s[i] * (h.z + 1)] = parent;
                    for(uint32_t j = 1; j < h.z + 1; ++j)
                    {
                        buf.original[w2s[i] * (h.z + 1) + j] +=
                            old_parent;
                        buf.original[w2s[i] * (h.z + 1) + j] -= parent;
                    }
                }

                for(uint32_t j = loc2.pos;
                    j < (h.z + 1)
                    && t2s[i]
                           > parent
                                 + buf.original[w2s[i] * (h.z + 1) + j];
                    ++j)
                {
                    buf.original[w2s[i] * (h.z + 1) + j] =
                        t2s[i] - parent;
                }
            }

            auto tmp = write_block(h, hb, alloc);
            if(!is_same_header(tmp, hb))
            {
                updated_headers.push_back({tmp, loc2.id()});
            }
        }

        if(!updated_headers.empty())
        {
            uint32_t cur = 0;
            vec_header::write_lazy(
                header, updated_headers[cur].second,
                [&updated_headers,
                 &cur](auto* hb) -> std::optional<uint64_t>
                {
                    *hb = updated_headers[cur].first;
                    ++cur;
                    return (cur < updated_headers.size())
                               ? std::make_optional(
                                   updated_headers[cur].second)
                               : std::nullopt;
                },
                alloc);
        }
    }

    [[nodiscard]] static auto
    is_same_header(const typename vec_block::header& h1,
                   const typename vec_block::header& h2) -> bool
    {
        return h1.head == h2.head && h1.size == h2.size
               && h1.capacity == h2.capacity;
    }
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-compressed6.ixx>
