#include <libdisk/detail/utility.hxx>

#include <algorithm>

namespace tgraph_disk_reachability::detail
{
template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::create(Alloc<PAGE_SIZE>* alloc) -> header
{
    return map::create(alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void rttree<T, Alloc, PAGE_SIZE>::destroy(header* h, Alloc<PAGE_SIZE>* alloc)
{
    map::destroy(h, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void rttree<T, Alloc, PAGE_SIZE>::clear(header* h, Alloc<PAGE_SIZE>* alloc)
{
    map::clear(h, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::insert(header* h, interval interv, T elem,
                                         Alloc<PAGE_SIZE>* alloc) -> bool
{
    if(auto range = none_contained_by_(*h, interv, *alloc))
    {
        if(range->first != range->second)
        {
            remove_range(h, *range, alloc);
        }
        return insert_(h, interv, elem, alloc);
    }

    return false;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::insert_(header* h, interval interv, T elem,
                                          Alloc<PAGE_SIZE>* alloc) -> bool
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
    ++total_inserts;
#endif
    return map::insert(h, interv, elem, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::none_contained_by_(
    const header& h, interval interv, const Alloc<PAGE_SIZE>& alloc)
    -> std::optional<std::pair<const_iterator, const_iterator>>
{
    if(h.size == 0)
    {
        return std::make_pair(map::end(h, alloc), map::end(h, alloc));
    }

    auto it1 = find_prev_(h, interv.right, alloc);
    if(it1 != map::end(h, alloc) && interv.left <= it1->first.left)
    {
        return {};
    }

    auto it2 = find_next_(h, interv.left, alloc);
    if(it2 != map::end(h, alloc) && interv.right >= it2->first.right)
    {
        return {};
    }

    return std::make_pair(it1 == map::end(h, alloc) ? map::begin(h, alloc)
                          : it1->first.right < interv.right ? std::next(it1)
                                                            : it1,
                          it2 == map::end(h, alloc)       ? it2
                          : it2->first.left > interv.left ? it2
                                                          : std::next(it2));
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::find_prev(const header& h, uint32_t t,
                                            const Alloc<PAGE_SIZE>& alloc)
    -> std::optional<std::pair<interval, uint32_t>>
{
    auto it = find_prev_(h, t, alloc);
    return it != map::end(h, alloc) ? std::make_optional(*it) : std::nullopt;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::find_next(const header& h, uint32_t t,
                                            const Alloc<PAGE_SIZE>& alloc)
    -> std::optional<std::pair<interval, uint32_t>>
{
    auto it = find_next_(h, t, alloc);
    return it != map::end(h, alloc) ? std::make_optional(*it) : std::nullopt;
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::find_prev_(const header& h, uint32_t t,
                                             const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    interval key = {0, t};
    by_arrival comp;

    auto ctx = map::find_leaf(h, key, comp, alloc);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    typename map::node n = {};
    uint64_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);

    size_t n_i = disk::detail::ub(n.leaf, key, comp);

    return n_i == 0 ? const_iterator(0, 0, alloc)
                    : const_iterator(n_id, n_i - 1, alloc);
}

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto rttree<T, Alloc, PAGE_SIZE>::find_next_(const header& h, uint32_t t,
                                             const Alloc<PAGE_SIZE>& alloc)
    -> const_iterator
{
    interval key = {t, 0};
    by_departure comp;

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    auto ctx = map::find_leaf(h, key, comp, alloc);
    typename map::node n = {};
    uint64_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);

    size_t n_i = disk::detail::lb(n.leaf, key, comp);

    return n_i < n.leaf.size ? const_iterator(n_id, n_i, alloc)
                             : const_iterator(n.leaf.next, 0, alloc);
}

/* template<typename T, template <size_t> typename Alloc, size_t PAGE_SIZE> */
/* void rttree<T, Alloc, PAGE_SIZE>::remove_range( */
/*     header* h, std::pair<const_iterator, const_iterator> range, */
/*     Alloc<PAGE_SIZE>* alloc) */
/* { */
/*     auto first = range.first != map::end(*h, *alloc) */
/*                      ? std::make_optional(range.first->first) */
/*                      : std::nullopt; */

/*     auto last = range.second != map::end(*h, *alloc) */
/*                     ? std::make_optional(range.second->first) */
/*                     : std::nullopt; */

/* #ifdef BENCHMARK_ON */
/*     ++total_binary_searches; */
/* #endif */
/*     auto mid = map::split(h, *first, alloc); */
/*     if(last) */
/*     { */
/* #ifdef BENCHMARK_ON */
/*         ++total_binary_searches; */
/* #endif */
/*         auto right = map::split(&mid, *last, alloc); */
/*         map::join(h, &right, alloc); */
/*     } */
/*     map::destroy_lazy(&mid, alloc); */
/* } */

template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
void rttree<T, Alloc, PAGE_SIZE>::remove_range(
    header* h, std::pair<const_iterator, const_iterator> range,
    Alloc<PAGE_SIZE>* alloc)
{
    auto& [first, last] = range;
    auto begin = map::begin(*h, *alloc);
    auto end = map::end(*h, *alloc);

    if(std::next(first) == last)
    {
        map::remove(h, first->first, alloc);
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
    }
    else if(first != begin && last != end)
    {
        auto mid = map::split(h, first->first, alloc);
        auto right = map::split(&mid, last->first, alloc);
        map::join(h, &right, alloc);
        map::destroy_lazy(&mid, alloc);
#ifdef BENCHMARK_ON
        total_binary_searches += 3;
#endif
    }
    else if(first != begin)
    {
        auto right = map::split(h, first->first, alloc);
        map::destroy_lazy(&right, alloc);
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
    }
    else if(last != end)
    {
        auto right = map::split(h, last->first, alloc);
        map::destroy_lazy(h, alloc);
        *h = right;
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
    }
    else
    {
        map::clear_lazy(h, alloc);
    }
}

} // namespace tgraph_disk_reachability::detail
