#include <libdisk/detail/utility.hxx>

#include <algorithm>

namespace tgraph_disk_reachability::detail
{
template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::create(disk::allocator<PAGE_SIZE>* alloc)
    -> header
{
    return map::create(alloc);
}

template<typename T, size_t PAGE_SIZE>
void rttree_simple<T, PAGE_SIZE>::destroy(header* h,
                                          disk::allocator<PAGE_SIZE>* alloc)
{
    map::destroy(h, alloc);
}

template<typename T, size_t PAGE_SIZE>
void rttree_simple<T, PAGE_SIZE>::clear(header* h,
                                        disk::allocator<PAGE_SIZE>* alloc)
{
    map::clear(h, alloc);
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::insert(header* h, interval interv, T elem,
                                         disk::allocator<PAGE_SIZE>* alloc)
    -> bool
{
    if(auto range = none_contained_by_(*h, interv, *alloc))
    {
        if(range->first != range->second)
        {
            remove_range(h, *range, alloc);
        }
        return insert_(h, interv, elem, alloc);
    }

    return false;
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::insert_(header* h, interval interv, T elem,
                                          disk::allocator<PAGE_SIZE>* alloc)
    -> bool
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
    ++total_inserts;
#endif
    return map::insert(h, interv, elem, alloc);
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::remove_(header* h, interval interv,
                                          disk::allocator<PAGE_SIZE>* alloc)
    -> bool
{
    if(map::remove(h, interv, alloc))
    {
#ifdef BENCHMARK_ON
        ++total_erases;
#endif
        return true;
    }
    return false;
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::none_contained_by_(
    const header& h, interval interv, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::optional<std::pair<const_iterator, const_iterator>>
{
    if(h.size == 0)
    {
        return std::make_pair(map::end(h, alloc), map::end(h, alloc));
    }

    auto it1 = find_prev_(h, interv.right, alloc);
    if(it1 != map::end(h, alloc) && interv.left <= it1->first.left)
    {
        return {};
    }

    auto it2 = find_next_(h, interv.left, alloc);
    if(it2 != map::end(h, alloc) && interv.right >= it2->first.right)
    {
        return {};
    }

    return std::make_pair(it1 == map::end(h, alloc) ? map::begin(h, alloc)
                          : it1->first.right < interv.right ? std::next(it1)
                                                            : it1,
                          it2 == map::end(h, alloc)       ? it2
                          : it2->first.left > interv.left ? it2
                                                          : std::next(it2));
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::find_prev(
    const header& h, uint32_t t, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::optional<std::pair<interval, uint32_t>>
{
    auto it = find_prev_(h, t, alloc);
    return it != map::end(h, alloc) ? std::make_optional(*it) : std::nullopt;
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::find_next(
    const header& h, uint32_t t, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::optional<std::pair<interval, uint32_t>>
{
    auto it = find_next_(h, t, alloc);
    return it != map::end(h, alloc) ? std::make_optional(*it) : std::nullopt;
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::find_prev_(
    const header& h, uint32_t t, const disk::allocator<PAGE_SIZE>& alloc)
    -> const_iterator
{
    interval key = {0, t};
    by_arrival comp;

    auto ctx = map::find_leaf(h, key, comp, alloc);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    typename map::node n = {};
    uint32_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);

    size_t n_i = disk::detail::ub(n.leaf, key, comp);

    return n_i == 0 ? const_iterator(0, 0, alloc)
                    : const_iterator(n_id, n_i - 1, alloc);
}

template<typename T, size_t PAGE_SIZE>
auto rttree_simple<T, PAGE_SIZE>::find_next_(
    const header& h, uint32_t t, const disk::allocator<PAGE_SIZE>& alloc)
    -> const_iterator
{
    interval key = {t, 0};
    by_departure comp;

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    auto ctx = map::find_leaf(h, key, comp, alloc);
    typename map::node n = {};
    uint32_t n_id = ctx.top().first;
    alloc.pread(n_id, n.bytes);

    size_t n_i = disk::detail::lb(n.leaf, key, comp);

    return n_i < n.leaf.size ? const_iterator(n_id, n_i, alloc)
                             : const_iterator(n.leaf.next, 0, alloc);
}

template<typename T, size_t PAGE_SIZE>
void rttree_simple<T, PAGE_SIZE>::remove_range(
    header* h, std::pair<const_iterator, const_iterator> range,
    disk::allocator<PAGE_SIZE>* alloc)
{
    std::vector<interval> to_remove;
    for(auto it = range.first; it != range.second; ++it)
    {
        to_remove.push_back(it->first);
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    for(const auto& key: to_remove)
    {
        remove_(h, key, alloc);
    }
}

} // namespace tgraph_disk_reachability::detail
