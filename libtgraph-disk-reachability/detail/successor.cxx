#include <libtgraph-disk-reachability/detail/successor.hxx>

#include <cmath>

namespace tgraph_disk_reachability::detail
{
using std::log2;

successor::successor(uint32_t u)
    : _words(std::ceil(static_cast<double>(u) / W), 0)
{
}

void successor::set(uint32_t i)
{
    uint64_t& w = _words[i / W];
    w |= 1U << (W - (i % W) - 1U);
}

auto successor::next(uint32_t i) -> std::optional<uint32_t>
{
    for(uint32_t j = i % W; j < _words.size(); ++j)
    {
        uint64_t w = _words[i / W];
        if(w == 0)
        {
            continue;
        }

        return j * W + __builtin_clz(_words[i % W]);
    }

    return {};
}

} // namespace tgraph_disk_reachability::detail
