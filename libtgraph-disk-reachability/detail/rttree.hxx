#pragma once

#include <libdisk/map.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <optional>
#include <tuple>

namespace tgraph_disk_reachability::detail
{
template<typename T, template<size_t> typename Alloc, size_t PAGE_SIZE>
class rttree
{
public:
    struct by_departure
    {
        auto operator()(const interval& lhs, const interval& rhs) const -> bool
        {
            return lhs.left < rhs.left;
        }
    };

    struct by_arrival
    {
        auto operator()(const interval& lhs, const interval& rhs) const -> bool
        {
            return lhs.right < rhs.right;
        }
    };

    using map = disk::map<interval, uint32_t, Alloc, by_arrival, PAGE_SIZE>;
    using header = typename map::header;
    using const_iterator = typename map::const_iterator;

    static auto create(Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static auto insert(header* h, interval interv, T elem,
                       Alloc<PAGE_SIZE>* alloc) -> bool;

    static auto insert_(header* h, interval interv, T elem,
                        Alloc<PAGE_SIZE>* alloc) -> bool;

    static auto remove_(header* h, interval interv, Alloc<PAGE_SIZE>* alloc)
        -> bool;

    static auto none_contained_by_(const header& h, interval interv,
                                   const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<std::pair<const_iterator, const_iterator>>;

    static auto find_next(const header& h, uint32_t t,
                          const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<std::pair<interval, uint32_t>>;

    static auto find_prev(const header& h, uint32_t t,
                          const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<std::pair<interval, uint32_t>>;

#ifdef BENCHMARK_ON
    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_erases = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif

private:
    static auto find_next_(const header& h, uint32_t t,
                           const Alloc<PAGE_SIZE>& alloc) -> const_iterator;
    static auto find_prev_(const header& h, uint32_t t,
                           const Alloc<PAGE_SIZE>& alloc) -> const_iterator;
    static void remove_range(header* h,
                             std::pair<const_iterator, const_iterator> range,
                             Alloc<PAGE_SIZE>* alloc);
};

} // namespace tgraph_disk_reachability::detail

#include <libtgraph-disk-reachability/detail/rttree.ixx>
