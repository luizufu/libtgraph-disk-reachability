#pragma once

#include <fastpfor/codecs.h>
#include <fastpfor/simple8b.h>
#include <fastpfor/simple8b_rle.h>
#include <memory>

namespace tgraph_disk_reachability
{

inline auto codec_map(const std::string& key)
    -> FastPForLib::IntegerCODEC&
{
    using MapType =
        std::map<std::string,
                 std::shared_ptr<FastPForLib::IntegerCODEC>>;
    static const MapType codecs = {
        {"simple8b", std::shared_ptr<FastPForLib::IntegerCODEC>(
                         new FastPForLib::Simple8b<false>())},
        {"simple8b_rle", std::shared_ptr<FastPForLib::IntegerCODEC>(
                             new FastPForLib::Simple8b_RLE<false>())},
    };
    
    return *codecs.at(key);
}

} // namespace tgraph_disk_reachability
