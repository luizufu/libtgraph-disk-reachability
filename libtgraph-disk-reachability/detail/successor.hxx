#pragma once

#include <vector>
#include <optional>
#include <cstdint>

namespace tgraph_disk_reachability::detail
{
class successor
{
    static constexpr size_t W = sizeof(uint64_t);

public:
    explicit successor(uint32_t u);

    void set(uint32_t i);
    auto next(uint32_t i) -> std::optional<uint32_t>;

private:
    std::vector<uint64_t> _words;
};

} // namespace tgraph_disk_reachability::detail
