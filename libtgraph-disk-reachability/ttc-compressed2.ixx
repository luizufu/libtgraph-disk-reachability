#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                               uint32_t delta, uint32_t z,
                                               Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    uint64_t header_size =
        static_cast<uint64_t>(n)
        * static_cast<uint64_t>(std::ceil(tau / static_cast<double>(z)));

    size_t original_size = n * z;
    size_t compressed_size = n * z;
    std::vector<uint32_t> original(original_size + 32);
    std::vector<uint32_t> compressed(compressed_size + 1024);
    std::vector<typename vec_block::header> headers(header_size);

    h.in = vec_header::create(header_size, alloc);
    std::fill(original.begin(), original.end(), 0);
    block_adjacent_difference(original.data(), n, z, true);
    compress_block(original.data(), original_size, compressed.data(),
                   compressed_size);
    for(uint32_t i = 0; i < header_size; ++i)
    {
        headers[i] = vec_block::create(compressed_size, alloc);
        vec_block::write_n(headers[i], 0, compressed_size, compressed.data(),
                           alloc);
    }
    vec_header::write_n(h.in, 0, header_size, headers.data(), alloc);

    h.out = vec_header::create(header_size, alloc);
    std::fill(original.begin(), original.end(), tau);
    block_adjacent_difference(original.data(), n, z, false);
    compressed_size = n * z;
    compress_block(original.data(), original_size, compressed.data(),
                   compressed_size);
    for(uint32_t i = 0; i < header_size; ++i)
    {
        headers[i] = vec_block::create(compressed_size, alloc);
        vec_block::write_n(headers[i], 0, compressed_size, compressed.data(),
                           alloc);
    }
    vec_header::write_n(h.out, 0, header_size, headers.data(), alloc);

    h.n = n;
    h.tau = tau;
    h.delta = delta;
    h.z = z;

    return h;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                               uint32_t delta, uint32_t z,
                                               It contacts_begin,
                                               It contacts_end,
                                               Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        add_contact(&h, *it, alloc);
    }

    return h;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::destroy(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    uint64_t header_size =
        static_cast<uint64_t>(h->n)
        * static_cast<uint64_t>(std::ceil(h->tau / static_cast<double>(h->z)));
    std::vector<typename vec_block::header> headers(header_size);

    vec_header::read_n(h->in, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::destroy(&header, alloc);
    }
    vec_header::destroy(&h->in, alloc);

    vec_header::read_n(h->out, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::destroy(&header, alloc);
    }
    vec_header::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
    h->z = 0;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::clear(header* h,
                                              Alloc<PAGE_SIZE>* alloc)
{
    uint64_t header_size =
        static_cast<uint64_t>(h->n)
        * static_cast<uint64_t>(std::ceil(h->tau / static_cast<double>(h->z)));
    std::vector<typename vec_block::header> headers(header_size);

    vec_header::read_n(h->in, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::fill(&header, 0, alloc);
    }

    vec_header::read_n(h->out, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::fill(&header, h->tau, alloc);
    }
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::add_contact(header* h, contact c,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    size_t original_size = h->n * h->z;
    size_t compressed_size = h->n * h->z;
    std::vector<uint32_t> original(original_size + 32);
    std::vector<uint32_t> compressed(compressed_size + 1024);

    uint32_t test;
    {
        auto [block_id, i] = out_block_location(u, t, *h);
        auto hb = vec_header::read(h->out, block_id, *alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
        original_size = h->n * h->z;
        decompress_block(compressed.data(), hb.size, original.data(),
                         original_size);
        block_adjacent_sum(original.data(), h->n, h->z, false);
        test = original[v * h->z + i];
    }

    if(test != t)
    {
        std::vector<uint32_t> out(h->n, h->tau);
        {
            if(t + h->delta < h->tau)
            {
                auto [block_id, i] = out_block_location(v, t + h->delta, *h);
                auto hb = vec_header::read(h->out, block_id, *alloc);
                vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
                original_size = h->n * h->z;
                decompress_block(compressed.data(), hb.size, original.data(),
                                 original_size);
                block_adjacent_sum(original.data(), h->n, h->z, false);
                for(uint32_t wplus = 0; wplus < h->n; ++wplus)
                {
                    out[wplus] = original[wplus * h->z + i];
                }
            }
        }
        out[v] = t;

        std::vector<uint32_t> in(h->n, 0);
        {
            if(t >= h->delta)
            {
                auto [block_id, i] = in_block_location(u, t, *h);
                auto hb = vec_header::read(h->in, block_id, *alloc);
                vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
                original_size = h->n * h->z;
                decompress_block(compressed.data(), hb.size, original.data(),
                                 original_size);
                block_adjacent_sum(original.data(), h->n, h->z, true);

                for(uint32_t wminus = 0; wminus < h->n; ++wminus)
                {
                    in[wminus] = original[wminus * h->z + i];
                }
            }
        }
        in[u] = t + 1;

        std::vector<uint32_t> wpluses;
        for(uint32_t wplus = 0; wplus < h->n; ++wplus)
        {
            if(wplus != u && out[wplus] < h->tau)
            {
                wpluses.push_back(wplus);
            }
        }

        std::vector<uint32_t> wminuses;
        for(uint32_t wminus = 0; wminus < h->n; ++wminus)
        {
            if(wminus != v && in[wminus] > 0)
            {
                wminuses.push_back(wminus);
            }
        }

        for(uint32_t wminus: wminuses)
        {
            auto [block_id, i] =
                out_block_location(wminus, in[wminus] - 1, *h);
            auto [end_block_id, end_i] =
                out_block_location(wminus + 1, h->tau - 1, *h);

            auto it = vec_header::iterator_at(h->out, block_id, *alloc);
            auto end = vec_header::iterator_at(h->out, end_block_id, *alloc);
            std::vector<std::pair<typename vec_block::header, uint64_t>>
                updated_headers;

            for(uint64_t current_block_id = block_id; it != end;
                ++it, i = 0, ++current_block_id)
            {
                typename vec_block::header hb = *it;
                vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
                original_size = h->n * h->z;
                decompress_block(compressed.data(), hb.size, original.data(),
                                 original_size);
                block_adjacent_sum(original.data(), h->n, h->z, false);

                uint32_t max_j = 0;
                for(uint32_t wplus: wpluses)
                {
                    uint32_t j = i;
                    for(; j < h->z && out[wplus] < original[wplus * h->z + j];
                        ++j)
                    {
                        original[wplus * h->z + j] = out[wplus];
                    }
                    max_j = std::max(max_j, j);
                }

                if(max_j > i)
                {
                    block_adjacent_difference(original.data(), h->n, h->z,
                                              false);
                    compressed_size = h->n * h->z;
                    compress_block(original.data(), h->n * h->z,
                                   compressed.data(), compressed_size);
                    vec_block::resize(&hb, compressed_size, alloc);
                    vec_block::write_n(hb, 0, compressed_size,
                                       compressed.data(), alloc);
                    if(!is_same_header(hb, *it))
                    {
                        updated_headers.push_back({hb, current_block_id});
                    }
                }

                if(max_j < h->z)
                {
                    break;
                }
            }

            if(!updated_headers.empty())
            {
                uint32_t cur = 0;
                vec_header::write_lazy(
                    h->out, updated_headers[cur].second,
                    [&updated_headers,
                     &cur](auto* hb) -> std::optional<uint64_t>
                    {
                        *hb = updated_headers[cur].first;
                        ++cur;
                        return (cur < updated_headers.size())
                                   ? std::make_optional(
                                       updated_headers[cur].second)
                                   : std::nullopt;
                    },
                    alloc);
            }
        }

        for(uint32_t wplus: wpluses)
        {
            auto [block_id, i] =
                in_block_location(wplus, out[wplus] + h->delta, *h);
            auto [end_block_id, end_i] =
                in_block_location(wplus + 1, h->delta, *h);

            auto it = vec_header::iterator_at(h->in, block_id, *alloc);
            auto end = vec_header::iterator_at(h->in, end_block_id, *alloc);
            std::vector<std::pair<typename vec_block::header, uint64_t>>
                updated_headers;

            for(uint64_t current_block_id = block_id; it != end;
                ++it, i = 0, ++current_block_id)
            {
                typename vec_block::header hb = *it;
                vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
                original_size = h->n * h->z;
                decompress_block(compressed.data(), hb.size, original.data(),
                                 original_size);
                block_adjacent_sum(original.data(), h->n, h->z, true);

                uint32_t max_j = 0;
                for(uint32_t wminus: wminuses)
                {
                    uint32_t j = i;
                    for(; j < h->z && in[wminus] > original[wminus * h->z + j];
                        ++j)
                    {
                        original[wminus * h->z + j] = in[wminus];
                    }
                    max_j = std::max(max_j, j);
                }

                if(max_j > i)
                {
                    block_adjacent_difference(original.data(), h->n, h->z,
                                              true);
                    compressed_size = h->n * h->z;
                    compress_block(original.data(), h->n * h->z,
                                   compressed.data(), compressed_size);
                    vec_block::resize(&hb, compressed_size, alloc);
                    vec_block::write_n(hb, 0, compressed_size,
                                       compressed.data(), alloc);
                    if(!is_same_header(hb, *it))
                    {
                        updated_headers.push_back({hb, current_block_id});
                    }
                }

                if(max_j < h->z)
                {
                    break;
                }
            }

            if(!updated_headers.empty())
            {
                uint32_t cur = 0;
                vec_header::write_lazy(
                    h->in, updated_headers[cur].second,
                    [&updated_headers,
                     &cur](auto* hb) -> std::optional<uint64_t>
                    {
                        *hb = updated_headers[cur].first;
                        ++cur;
                        return (cur < updated_headers.size())
                                   ? std::make_optional(
                                       updated_headers[cur].second)
                                   : std::nullopt;
                    },
                    alloc);
            }
        }
    }
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval interv,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    interv.right = std::min(interv.right, h.tau + h.delta);

    if(interv.left >= h.tau || interv.right - interv.left < h.delta)
    {
        return false;
    }

    uint32_t test;
    {
        size_t original_size = h.n * h.z;
        size_t compressed_size = h.n * h.z;
        std::vector<uint32_t> original(original_size + 32);
        std::vector<uint32_t> compressed(compressed_size + 1024);

        auto [block_id, i] = out_block_location(u, interv.left, h);
        auto hb = vec_header::read(h.out, block_id, alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
        decompress_block(compressed.data(), hb.size, original.data(),
                         original_size);
        block_adjacent_sum(original.data(), h.n, h.z, false);
        test = original[v * h.z + i];
    }
    return std::min(test + h.delta, h.tau + h.delta - 1) < interv.right;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    return {};
    // if(u == v)
    // {
    //     return {};
    // }

    // i.right = std::min(i.right, h.tau + h.delta);

    // if(i.left >= h.tau || i.right - i.left < h.delta)
    // {
    //     return {};
    // }

    // auto [tplus, _] = vec::read(h.out, out_access(u, i.left, v, h),
    // alloc); tplus += h.delta; if(std::min(tplus, h.tau + h.delta - 1) >=
    // i.right)
    // {
    //     return {};
    // }

    // auto [tminus, succ] = vec::read(h.in, in_access(v, tplus, u, h),
    // alloc);
    // --tminus;
    // journey j({u, succ, tminus}, h.delta);

    // while(succ != v)
    // {
    //     auto [next_tminus, next_succ] =
    //         vec::read(h.in, in_access(v, tplus, succ, h), alloc);
    //     next_tminus--;

    //     j.push_back({succ, next_succ, next_tminus});

    //     succ = next_succ;
    //     tminus = next_tminus;
    // }

    // return j;
}

#ifdef BENCHMARK_ON

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::opt_space() -> uint64_t
{
    return total_opt_space;
}

template<const char* Compressor, template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed2<Compressor, Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
    total_opt_space = 0;
}

#endif

} // namespace tgraph_disk_reachability
