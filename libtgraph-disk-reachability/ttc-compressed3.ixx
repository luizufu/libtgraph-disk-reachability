#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, Alloc<PAGE_SIZE>* alloc)
    -> header
{
    uint32_t z = std::ceil(std::sqrt(tau));
    header h = {};
    uint64_t headers_per_vertex =
        static_cast<uint64_t>(std::ceil(tau / static_cast<double>(z)))
        + 1;
    uint64_t header_size =
        static_cast<uint64_t>(n) * headers_per_vertex;

    uint32_t parent_size = std::ceil(tau / static_cast<double>(z)) + 1;
    uint32_t child_size = z - 1;

    uint32_t original_size = n * parent_size;
    std::vector<uint32_t> original(original_size + 32, 0);
    std::vector<uint32_t> compressed1(original_size + 1024);
    std::vector<uint32_t> compressed2(original_size + 1024);
    std::vector<typename vec_block::header> headers(header_size);

    size_t compressed_size1 = n * parent_size;
    compress_block(original.data(), n * parent_size, compressed1.data(),
                   compressed_size1);

    size_t compressed_size2 = n * child_size;
    compress_block(original.data(), n * child_size, compressed2.data(),
                   compressed_size2);

    h.in = vec_header::create(header_size, alloc);
    for(uint32_t i = 0; i < header_size; ++i)
    {
        if(i % headers_per_vertex == 0)
        {
            headers[i] = vec_block::create(compressed_size1, alloc);
            vec_block::write_n(headers[i], 0, compressed_size1,
                               compressed1.data(), alloc);
        }
        else
        {
            headers[i] = vec_block::create(compressed_size2, alloc);
            vec_block::write_n(headers[i], 0, compressed_size2,
                               compressed2.data(), alloc);
        }
    }
    vec_header::write_n(h.in, 0, header_size, headers.data(), alloc);

    h.out = vec_header::create(header_size, alloc);
    for(uint32_t i = 0; i < header_size; ++i)
    {
        if(i % headers_per_vertex == 0)
        {
            headers[i] = vec_block::create(compressed_size1, alloc);
            vec_block::write_n(headers[i], 0, compressed_size1,
                               compressed1.data(), alloc);
        }
        else
        {
            headers[i] = vec_block::create(compressed_size2, alloc);
            vec_block::write_n(headers[i], 0, compressed_size2,
                               compressed2.data(), alloc);
        }
    }
    vec_header::write_n(h.out, 0, header_size, headers.data(), alloc);

    h.n = n;
    h.tau = tau;
    h.delta = delta;
    h.z = z;
    h.parent_size = parent_size;
    h.child_size = child_size;

    return h;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
template<typename It>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, It contacts_begin,
    It contacts_end, Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        add_contact(&h, *it, alloc);
    }

    return h;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::destroy(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    uint64_t headers_per_vertex =
        static_cast<uint64_t>(
            std::ceil(h->tau / static_cast<double>(h->z)))
        + 1;
    uint64_t header_size =
        static_cast<uint64_t>(h->n) * headers_per_vertex;
    std::vector<typename vec_block::header> headers(header_size);

    vec_header::read_n(h->in, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::destroy(&header, alloc);
    }
    vec_header::destroy(&h->in, alloc);

    vec_header::read_n(h->out, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::destroy(&header, alloc);
    }
    vec_header::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
    h->z = 0;
    h->parent_size = 0;
    h->child_size = 0;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::clear(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    header tmp = *h;
    destroy(h, alloc);
    *h = create(tmp.n, tmp.tau, tmp.delta, tmp.z, alloc);
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::add_contact(
    header* h, contact c, Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    size_t original_size = h->n * h->parent_size;
    size_t compressed_size = h->n * h->parent_size;
    std::vector<uint32_t> original1(original_size + 32);
    std::vector<uint32_t> original2(original_size + 32);
    std::vector<uint32_t> compressed(original_size + 1024);

    uint32_t test;
    {
        auto [block_id1, i1] = root_out_block_location(u, t, *h);
        auto [block_id2, i2] = leaf_out_block_location(u, t, *h);

        auto hb = vec_header::read(h->out, block_id1, *alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
        original_size = h->n * h->parent_size;
        decompress_block(compressed.data(), hb.size, original1.data(),
                         original_size);
        block_adjacent_sum(original1.data(), h->n, h->parent_size);
        test = h->tau - original1[v * h->parent_size + i1];

        if(i2 > 0
           && h->tau - original1[v * h->parent_size + i1 + 1] < test)
        {
            hb = vec_header::read(h->out, block_id2, *alloc);
            vec_block::read_n(hb, 0, hb.size, compressed.data(),
                              *alloc);
            original_size = h->n * h->child_size;
            decompress_block(compressed.data(), hb.size,
                             original2.data(), original_size);
            block_adjacent_sum(original2.data(), h->n, h->child_size);
            test -= original2[v * h->child_size + i2 - 1];
        }
    }

    if(test != t)
    {
        std::vector<uint32_t> out(h->n, h->tau);
        {
            if(t + h->delta < h->tau)
            {
                auto [block_id1, i1] =
                    root_out_block_location(v, t + h->delta, *h);
                auto [block_id2, i2] =
                    leaf_out_block_location(v, t + h->delta, *h);

                auto hb = vec_header::read(h->out, block_id1, *alloc);
                vec_block::read_n(hb, 0, hb.size, compressed.data(),
                                  *alloc);
                original_size = h->n * h->parent_size;
                decompress_block(compressed.data(), hb.size,
                                 original1.data(), original_size);
                block_adjacent_sum(original1.data(), h->n,
                                   h->parent_size);
                for(uint32_t wplus = 0; wplus < h->n; ++wplus)
                {
                    out[wplus] -=
                        original1[wplus * h->parent_size + i1];
                }

                if(i2 > 0)
                {
                    std::vector<uint32_t> wpluses_remaining;
                    for(uint32_t wplus = 0; wplus < h->n; ++wplus)
                    {
                        if(h->tau
                               - original1[wplus * h->parent_size + i1
                                           + 1]
                           < out[wplus])
                        {
                            wpluses_remaining.push_back(wplus);
                        }
                    }

                    if(!wpluses_remaining.empty())
                    {
                        hb =
                            vec_header::read(h->out, block_id2, *alloc);
                        vec_block::read_n(hb, 0, hb.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& wplus: wpluses_remaining)
                        {
                            out[wplus] -=
                                original2[wplus * h->child_size + i2
                                          - 1];
                        }
                    }
                }
            }
            out[v] = t;

            std::vector<uint32_t> in(h->n, 0);
            {
                if(t >= h->delta)
                {
                    auto [block_id1, i1] =
                        root_in_block_location(u, t, *h);
                    auto [block_id2, i2] =
                        leaf_in_block_location(u, t, *h);

                    auto hb =
                        vec_header::read(h->in, block_id1, *alloc);
                    vec_block::read_n(hb, 0, hb.size, compressed.data(),
                                      *alloc);
                    original_size = h->n * h->parent_size;
                    decompress_block(compressed.data(), hb.size,
                                     original1.data(), original_size);
                    block_adjacent_sum(original1.data(), h->n,
                                       h->parent_size);
                    for(uint32_t wminus = 0; wminus < h->n; ++wminus)
                    {
                        in[wminus] +=
                            original1[wminus * h->parent_size + i1];
                    }

                    if(i2 > 0)
                    {
                        std::vector<uint32_t> wminuses_remaining;
                        for(uint32_t wminus = 0; wminus < h->n;
                            ++wminus)
                        {
                            if(original1[wminus * h->parent_size + i1
                                         + 1]
                               > in[wminus])
                            {
                                wminuses_remaining.push_back(wminus);
                            }
                        }

                        if(!wminuses_remaining.empty())
                        {
                            hb = vec_header::read(h->in, block_id2,
                                                  *alloc);
                            vec_block::read_n(hb, 0, hb.size,
                                              compressed.data(),
                                              *alloc);
                            original_size = h->n * h->child_size;
                            decompress_block(compressed.data(), hb.size,
                                             original2.data(),
                                             original_size);
                            block_adjacent_sum(original2.data(), h->n,
                                               h->child_size);

                            for(auto& wminus: wminuses_remaining)
                            {
                                in[wminus] +=
                                    original2[wminus * h->child_size
                                              + i2 - 1];
                            }
                        }
                    }
                }
            }
            in[u] = t + 1;

            std::vector<uint32_t> wpluses;
            for(uint32_t wplus = 0; wplus < h->n; ++wplus)
            {
                if(wplus != u && out[wplus] < h->tau)
                {
                    wpluses.push_back(wplus);
                }
            }

            std::vector<uint32_t> wminuses;
            for(uint32_t wminus = 0; wminus < h->n; ++wminus)
            {
                if(wminus != v && in[wminus] > 0)
                {
                    wminuses.push_back(wminus);
                }
            }

            for(uint32_t wminus: wminuses)
            {
                auto [block_id1, i1] =
                    root_out_block_location(wminus, in[wminus] - 1, *h);
                auto [_, i2] =
                    leaf_out_block_location(wminus, in[wminus] - 1, *h);

                std::vector<
                    std::pair<typename vec_block::header, uint64_t>>
                    updated_headers;

                auto hb1 = vec_header::read(h->out, block_id1, *alloc);
                vec_block::read_n(hb1, 0, hb1.size, compressed.data(),
                                  *alloc);
                original_size = h->n * h->parent_size;
                decompress_block(compressed.data(), hb1.size,
                                 original1.data(), original_size);
                block_adjacent_sum(original1.data(), h->n,
                                   h->parent_size);

                struct child_info
                {
                    uint32_t wplus;
                    uint32_t old_parent;
                    bool need_cleaning;
                };

                uint32_t max_j = 0;
                bool updated_i1 = false;
                std::map<uint32_t, std::vector<child_info>>
                    children_to_update;
                std::vector<uint32_t> old_values(h->parent_size);
                for(uint32_t wplus: wpluses)
                {
                    std::copy(&original1[wplus * h->parent_size],
                              &original1[(wplus + 1) * h->parent_size],
                              &old_values[0]);

                    uint32_t j = i1;
                    if(out[wplus]
                       < h->tau - original1[wplus * h->parent_size + j])
                    {
                        bool updated_i1_local = false;
                        if(i2 == 0)
                        {
                            updated_i1_local = true;
                            original1[wplus * h->parent_size + j] =
                                h->tau - out[wplus];
                        }

                        ++j;

                        for(; j < h->parent_size
                              && out[wplus]
                                     < h->tau
                                           - original1
                                               [wplus * h->parent_size
                                                + j];
                            ++j)
                        {
                            original1[wplus * h->parent_size + j] =
                                h->tau - out[wplus];
                        }

                        if(updated_i1_local && i1 > 0
                           && old_values[i1 - 1] == old_values[i1])
                        {
                            children_to_update[i1 - 1].push_back({
                                .wplus = wplus,
                            });
                        }

                        if(original1[wplus * h->parent_size + i1]
                           < original1[wplus * h->parent_size + i1 + 1])
                        {
                            children_to_update[i1].push_back(
                                {.wplus = wplus,
                                 .old_parent = h->tau - old_values[i1],
                                 .need_cleaning =
                                     old_values[i1]
                                     == old_values[i1 + 1]});
                        }

                        if(j - 1 > i1 && j < h->parent_size
                           && original1[wplus * h->parent_size + j - 1]
                                  < original1[wplus * h->parent_size
                                              + j])
                        {
                            children_to_update[j - 1].push_back(
                                {.wplus = wplus,
                                 .old_parent =
                                     h->tau - old_values[j - 1],
                                 .need_cleaning = old_values[j - 1]
                                                  == old_values[j]});
                        }

                        max_j = std::max(j, max_j);
                        updated_i1 |= updated_i1_local;
                    }
                }

                if(updated_i1 || max_j > i1 + 1)
                {
                    block_adjacent_difference(original1.data(), h->n,
                                              h->parent_size);
                    compressed_size = h->n * h->parent_size;
                    compress_block(original1.data(),
                                   h->n * h->parent_size,
                                   compressed.data(), compressed_size);
                    auto hb = hb1;
                    vec_block::resize(&hb, compressed_size, alloc);
                    vec_block::write_n(hb, 0, compressed_size,
                                       compressed.data(), alloc);
                    if(!is_same_header(hb, hb1))
                    {
                        updated_headers.push_back({hb, block_id1});
                    }
                }

                if(!children_to_update.empty())
                {
                    auto it = children_to_update.begin();
                    auto end = children_to_update.end();

                    if(i1 > 0 && it->first == i1 - 1)
                    {
                        auto& [pos, info] = *it;

                        uint32_t current_block_id = block_id1 + pos + 1;
                        auto hb2 = vec_header::read(
                            h->out, current_block_id, *alloc);
                        vec_block::read_n(hb2, 0, hb2.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb2.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& [wplus, _1, _2]: info)
                        {
                            std::fill(
                                &original2[wplus * h->child_size],
                                &original2[(wplus + 1) * h->child_size],
                                0);
                        }

                        block_adjacent_difference(original2.data(),
                                                  h->n, h->child_size);
                        compressed_size = h->n * h->child_size;
                        compress_block(
                            original2.data(), h->n * h->child_size,
                            compressed.data(), compressed_size);

                        auto hb = hb2;
                        vec_block::resize(&hb, compressed_size, alloc);
                        vec_block::write_n(hb, 0, compressed_size,
                                           compressed.data(), alloc);
                        if(!is_same_header(hb, hb2))
                        {
                            updated_headers.push_back(
                                {hb, current_block_id});
                        }

                        ++it;
                    }

                    if(it->first == i1 && i2 > 0)
                    {
                        auto& [pos, info] = *it;

                        uint32_t current_block_id = block_id1 + pos + 1;
                        auto hb2 = vec_header::read(
                            h->out, current_block_id, *alloc);
                        vec_block::read_n(hb2, 0, hb2.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb2.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& [wplus, old_parent, need_cleaning]:
                            info)
                        {
                            if(need_cleaning)
                            {
                                std::fill(
                                    &original2[wplus * h->child_size],
                                    &original2[(wplus + 1)
                                               * h->child_size],
                                    0);
                            }

                            for(uint32_t j = i2 - 1;
                                j < h->child_size
                                && out[wplus]
                                       < old_parent
                                             - original2
                                                 [wplus * h->child_size
                                                  + j];
                                ++j)
                            {
                                original2[wplus * h->child_size + j] =
                                    old_parent - out[wplus];
                            }
                        }

                        block_adjacent_difference(original2.data(),
                                                  h->n, h->child_size);
                        compressed_size = h->n * h->child_size;
                        compress_block(
                            original2.data(), h->n * h->child_size,
                            compressed.data(), compressed_size);

                        auto hb = hb2;
                        vec_block::resize(&hb, compressed_size, alloc);
                        vec_block::write_n(hb, 0, compressed_size,
                                           compressed.data(), alloc);
                        if(!is_same_header(hb, hb2))
                        {
                            updated_headers.push_back(
                                {hb, current_block_id});
                        }

                        ++it;
                    }

                    for(; it != end; ++it)
                    {
                        auto& [pos, info] = *it;

                        uint32_t current_block_id = block_id1 + pos + 1;
                        auto hb2 = vec_header::read(
                            h->out, current_block_id, *alloc);
                        vec_block::read_n(hb2, 0, hb2.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb2.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& [wplus, old_parent, need_cleaning]:
                            info)
                        {
                            if(need_cleaning)
                            {
                                std::fill(
                                    &original2[wplus * h->child_size],
                                    &original2[(wplus + 1)
                                               * h->child_size],
                                    0);
                            }

                            uint32_t diff = old_parent - out[wplus];
                            for(uint32_t j = 0; j < h->child_size; ++j)
                            {
                                original2[wplus * h->child_size + j] =
                                    original2[wplus * h->child_size + j]
                                            > diff
                                        ? original2[wplus
                                                        * h->child_size
                                                    + j]
                                              - diff
                                        : 0;
                            }
                        }

                        block_adjacent_difference(original2.data(),
                                                  h->n, h->child_size);
                        compressed_size = h->n * h->child_size;
                        compress_block(
                            original2.data(), h->n * h->child_size,
                            compressed.data(), compressed_size);

                        auto hb = hb2;
                        vec_block::resize(&hb, compressed_size, alloc);
                        vec_block::write_n(hb, 0, compressed_size,
                                           compressed.data(), alloc);
                        if(!is_same_header(hb, hb2))
                        {
                            updated_headers.push_back(
                                {hb, current_block_id});
                        }
                    }
                }

                if(!updated_headers.empty())
                {
                    uint32_t cur = 0;
                    vec_header::write_lazy(
                        h->out, updated_headers[cur].second,
                        [&updated_headers,
                         &cur](auto* hb) -> std::optional<uint64_t>
                        {
                            *hb = updated_headers[cur].first;
                            ++cur;
                            return (cur < updated_headers.size())
                                       ? std::make_optional(
                                           updated_headers[cur].second)
                                       : std::nullopt;
                        },
                        alloc);
                }
            }

            for(uint32_t wplus: wpluses)
            {
                auto [block_id1, i1] = root_in_block_location(
                    wplus, out[wplus] + h->delta, *h);
                auto [_, i2] = leaf_in_block_location(
                    wplus, out[wplus] + h->delta, *h);

                std::vector<
                    std::pair<typename vec_block::header, uint64_t>>
                    updated_headers;

                auto hb1 = vec_header::read(h->in, block_id1, *alloc);
                vec_block::read_n(hb1, 0, hb1.size, compressed.data(),
                                  *alloc);
                original_size = h->n * h->parent_size;
                decompress_block(compressed.data(), hb1.size,
                                 original1.data(), original_size);
                block_adjacent_sum(original1.data(), h->n,
                                   h->parent_size);

                struct child_info
                {
                    uint32_t wminus;
                    uint32_t old_parent;
                    bool need_cleaning;
                };

                uint32_t max_j = 0;
                bool updated_i1 = false;
                std::map<uint32_t, std::vector<child_info>>
                    children_to_update;
                std::vector<uint32_t> old_values(h->parent_size);
                for(uint32_t wminus: wminuses)
                {
                    std::copy(&original1[wminus * h->parent_size],
                              &original1[(wminus + 1) * h->parent_size],
                              &old_values[0]);

                    uint32_t j = i1;
                    if(in[wminus]
                       > original1[wminus * h->parent_size + j])
                    {
                        bool updated_i1_local = false;
                        if(i2 == 0)
                        {
                            updated_i1_local = true;
                            original1[wminus * h->parent_size + j] =
                                in[wminus];
                        }

                        ++j;

                        for(; j < h->parent_size
                              && in[wminus]
                                     > original1[wminus * h->parent_size
                                                 + j];
                            ++j)
                        {
                            original1[wminus * h->parent_size + j] =
                                in[wminus];
                        }

                        if(updated_i1_local && i1 > 0
                           && old_values[i1 - 1] == old_values[i1])
                        {
                            children_to_update[i1 - 1].push_back(
                                {.wminus = wminus});
                        }

                        if(original1[wminus * h->parent_size + i1]
                           < original1[wminus * h->parent_size + i1
                                       + 1])
                        {
                            children_to_update[i1].push_back(
                                {.wminus = wminus,
                                 .old_parent = old_values[i1],
                                 .need_cleaning =
                                     old_values[i1]
                                     == old_values[i1 + 1]});
                        }

                        if(j - 1 > i1 && j < h->parent_size
                           && original1[wminus * h->parent_size + j - 1]
                                  < original1[wminus * h->parent_size
                                              + j])
                        {
                            children_to_update[j - 1].push_back(
                                {.wminus = wminus,
                                 .old_parent = old_values[j - 1],
                                 .need_cleaning = old_values[j - 1]
                                                  == old_values[j]});
                        }

                        max_j = std::max(j, max_j);
                        updated_i1 |= updated_i1_local;
                    }
                }

                if(updated_i1 || max_j > i1 + 1)
                {
                    block_adjacent_difference(original1.data(), h->n,
                                              h->parent_size);
                    compressed_size = h->n * h->parent_size;
                    compress_block(original1.data(),
                                   h->n * h->parent_size,
                                   compressed.data(), compressed_size);
                    auto hb = hb1;
                    vec_block::resize(&hb, compressed_size, alloc);
                    vec_block::write_n(hb, 0, compressed_size,
                                       compressed.data(), alloc);
                    if(!is_same_header(hb, hb1))
                    {
                        updated_headers.push_back({hb, block_id1});
                    }
                }

                if(!children_to_update.empty())
                {
                    auto it = children_to_update.begin();
                    auto end = children_to_update.end();

                    if(i1 > 0 && it->first == i1 - 1)
                    {
                        auto& [pos, info] = *it;

                        uint32_t current_block_id = block_id1 + pos + 1;
                        auto hb2 = vec_header::read(
                            h->in, current_block_id, *alloc);
                        vec_block::read_n(hb2, 0, hb2.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb2.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& [wminus, _1, _2]: info)
                        {
                            std::fill(
                                &original2[wminus * h->child_size],
                                &original2[(wminus + 1)
                                           * h->child_size],
                                0);
                        }

                        block_adjacent_difference(original2.data(),
                                                  h->n, h->child_size);
                        compressed_size = h->n * h->child_size;
                        compress_block(
                            original2.data(), h->n * h->child_size,
                            compressed.data(), compressed_size);

                        auto hb = hb2;
                        vec_block::resize(&hb, compressed_size, alloc);
                        vec_block::write_n(hb, 0, compressed_size,
                                           compressed.data(), alloc);
                        if(!is_same_header(hb, hb2))
                        {
                            updated_headers.push_back(
                                {hb, current_block_id});
                        }

                        ++it;
                    }

                    if(it->first == i1 && i2 > 0)
                    {
                        auto& [pos, info] = *it;

                        uint32_t current_block_id = block_id1 + pos + 1;
                        auto hb2 = vec_header::read(
                            h->in, current_block_id, *alloc);
                        vec_block::read_n(hb2, 0, hb2.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb2.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& [wminus, old_parent, need_cleaning]:
                            info)
                        {
                            if(need_cleaning)
                            {
                                std::fill(
                                    &original2[wminus * h->child_size],
                                    &original2[(wminus + 1)
                                               * h->child_size],
                                    0);
                            }

                            for(uint32_t j = i2 - 1;
                                j < h->child_size
                                && in[wminus]
                                       > old_parent
                                             + original2
                                                 [wminus * h->child_size
                                                  + j];
                                ++j)
                            {
                                original2[wminus * h->child_size + j] =
                                    in[wminus] - old_parent;
                            }
                        }

                        block_adjacent_difference(original2.data(),
                                                  h->n, h->child_size);
                        compressed_size = h->n * h->child_size;
                        compress_block(
                            original2.data(), h->n * h->child_size,
                            compressed.data(), compressed_size);

                        auto hb = hb2;
                        vec_block::resize(&hb, compressed_size, alloc);
                        vec_block::write_n(hb, 0, compressed_size,
                                           compressed.data(), alloc);
                        if(!is_same_header(hb, hb2))
                        {
                            updated_headers.push_back(
                                {hb, current_block_id});
                        }

                        ++it;
                    }

                    for(; it != end; ++it)
                    {
                        auto& [pos, info] = *it;

                        uint32_t current_block_id = block_id1 + pos + 1;
                        auto hb2 = vec_header::read(
                            h->in, current_block_id, *alloc);
                        vec_block::read_n(hb2, 0, hb2.size,
                                          compressed.data(), *alloc);
                        original_size = h->n * h->child_size;
                        decompress_block(compressed.data(), hb2.size,
                                         original2.data(),
                                         original_size);
                        block_adjacent_sum(original2.data(), h->n,
                                           h->child_size);

                        for(auto& [wminus, old_parent, need_cleaning]:
                            info)
                        {
                            if(need_cleaning)
                            {
                                std::fill(
                                    &original2[wminus * h->child_size],
                                    &original2[(wminus + 1)
                                               * h->child_size],
                                    0);
                            }

                            uint32_t diff = in[wminus] - old_parent;

                            for(uint32_t j = 0; j < h->child_size; ++j)
                            {
                                original2[wminus * h->child_size + j] =
                                    original2[wminus * h->child_size
                                              + j]
                                            > diff
                                        ? original2[wminus
                                                        * h->child_size
                                                    + j]
                                              - diff
                                        : 0;
                            }
                        }

                        block_adjacent_difference(original2.data(),
                                                  h->n, h->child_size);
                        compressed_size = h->n * h->child_size;
                        compress_block(
                            original2.data(), h->n * h->child_size,
                            compressed.data(), compressed_size);

                        auto hb = hb2;
                        vec_block::resize(&hb, compressed_size, alloc);
                        vec_block::write_n(hb, 0, compressed_size,
                                           compressed.data(), alloc);
                        if(!is_same_header(hb, hb2))
                        {
                            updated_headers.push_back(
                                {hb, current_block_id});
                        }
                    }
                }

                if(!updated_headers.empty())
                {
                    uint32_t cur = 0;
                    vec_header::write_lazy(
                        h->in, updated_headers[cur].second,
                        [&updated_headers,
                         &cur](auto* hb) -> std::optional<uint64_t>
                        {
                            *hb = updated_headers[cur].first;
                            ++cur;
                            return (cur < updated_headers.size())
                                       ? std::make_optional(
                                           updated_headers[cur].second)
                                       : std::nullopt;
                        },
                        alloc);
                }
            }
        }
    }
}

// template<const char* Compressor, template<size_t> typename Alloc,
//          size_t PAGE_SIZE>
// auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::can_reach(
//     const header& h, uint32_t u, uint32_t v, interval interv,
//     const Alloc<PAGE_SIZE>& alloc) -> bool
// {
//     if(u == v)
//     {
//         return true;
//     }

//     interv.right = std::min(interv.right, h.tau + h.delta);

//     if(interv.left >= h.tau || interv.right - interv.left < h.delta)
//     {
//         return false;
//     }

//     uint32_t test;
//     {
//         size_t original_size = h.n * h.parent_size;
//         std::vector<uint32_t> original(original_size + 32);
//         std::vector<uint32_t> compressed(original_size + 1024);

//         auto [block_id1, i1] =
//             root_in_block_location(v, interv.right - 1, h);
//         auto [block_id2, i2] =
//             leaf_in_block_location(v, interv.right - 1, h);

//         auto hb = vec_header::read(h.in, block_id1, alloc);
//         vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
//         original_size = h.n * h.parent_size;
//         decompress_block(compressed.data(), hb.size, original.data(),
//                          original_size);
//         block_adjacent_sum(original.data(), h.n, h.parent_size);
//         test = original[u * h.parent_size + i1];

//         if(i2 > 0 && original[u * h.parent_size + i1 + 1] > test)
//         {
//             hb = vec_header::read(h.in, block_id2, alloc);
//             vec_block::read_n(hb, 0, hb.size, compressed.data(),
//             alloc); original_size = h.n * h.child_size;
//             decompress_block(compressed.data(), hb.size,
//                              original.data(), original_size);
//             block_adjacent_sum(original.data(), h.n, h.child_size);
//             test += original[u * h.child_size + i2 - 1];
//         }
//     }
//     return interv.left <= test;
// }

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval interv,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    interv.right = std::min(interv.right, h.tau + h.delta);

    if(interv.left >= h.tau || interv.right - interv.left < h.delta)
    {
        return false;
    }

    uint32_t test;
    {
        size_t original_size = h.n * h.parent_size;
        std::vector<uint32_t> original(original_size + 32);
        std::vector<uint32_t> compressed(original_size + 1024);

        auto [block_id1, i1] =
            root_out_block_location(u, interv.left, h);
        auto [block_id2, i2] =
            leaf_out_block_location(u, interv.left, h);

        auto hb = vec_header::read(h.out, block_id1, alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
        original_size = h.n * h.parent_size;
        decompress_block(compressed.data(), hb.size, original.data(),
                         original_size);
        block_adjacent_sum(original.data(), h.n, h.parent_size);
        test = h.tau - original[v * h.parent_size + i1];

        if(i2 > 0
           && h.tau - original[v * h.parent_size + i1 + 1] < test)
        {
            hb = vec_header::read(h.out, block_id2, alloc);
            vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
            original_size = h.n * h.child_size;
            decompress_block(compressed.data(), hb.size,
                             original.data(), original_size);
            block_adjacent_sum(original.data(), h.n, h.child_size);
            test -= original[v * h.child_size + i2 - 1];
        }
    }
    return std::min(test + h.delta, h.tau + h.delta - 1) < interv.right;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    return {};
    // if(u == v)
    // {
    //     return {};
    // }

    // i.right = std::min(i.right, h.tau + h.delta);

    // if(i.left >= h.tau || i.right - i.left < h.delta)
    // {
    //     return {};
    // }

    // auto [tplus, _] = vec::read(h.out, out_access(u, i.left, v,
    // h), alloc); tplus += h.delta; if(std::min(tplus, h.tau +
    // h.delta - 1)
    // >= i.right)
    // {
    //     return {};
    // }

    // auto [tminus, succ] = vec::read(h.in, in_access(v, tplus, u,
    // h), alloc);
    // --tminus;
    // journey j({u, succ, tminus}, h.delta);

    // while(succ != v)
    // {
    //     auto [next_tminus, next_succ] =
    //         vec::read(h.in, in_access(v, tplus, succ, h), alloc);
    //     next_tminus--;

    //     j.push_back({succ, next_succ, next_tminus});

    //     succ = next_succ;
    //     tminus = next_tminus;
    // }

    // return j;
}

#ifdef BENCHMARK_ON

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::binary_searches()
    -> uint64_t
{
    return total_binary_searches;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc,
                     PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::tuple_inserts()
    -> uint64_t
{
    return total_inserts;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::tuple_updates()
    -> uint64_t
{
    return total_updates;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::tuple_erases()
    -> uint64_t
{
    return 0;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::opt_space()
    -> uint64_t
{
    return total_opt_space;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed3<Compressor, Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
    total_opt_space = 0;
}

#endif

} // namespace tgraph_disk_reachability
