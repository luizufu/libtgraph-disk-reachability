#include <libtgraph-disk-reachability/journey.hxx>

#include <iostream>

namespace tgraph_disk_reachability
{
auto concatenate(journey lhs, const journey& rhs) -> journey
{
    lhs._contacts.insert(lhs._contacts.end(), rhs._contacts.begin(),
                         rhs._contacts.end());

    return lhs;
}

auto operator==(const journey& lhs, const journey& rhs) -> bool
{
    return lhs.from() == rhs.from() && lhs.departure() == rhs.departure()
           && lhs.to() == rhs.to() && lhs.arrival() == rhs.arrival();
}

auto operator!=(const journey& lhs, const journey& rhs) -> bool
{
    return !(lhs == rhs);
}

auto operator<<(std::ostream& out, const journey& j) -> std::ostream&
{
    if(j._contacts.empty())
    {
        return out;
    }

    out << j._contacts[0];

    for(size_t i = 1; i < j._contacts.size(); ++i)
    {
        out << ", " << j._contacts[i];
    }

    return out;
}

auto operator<<(std::ostream& out, const contact& c) -> std::ostream&
{
    out << "(" << c.u << ", " << c.v << ", " << c.t << ")";
    return out;
}

auto operator<<(std::ostream& out, const interval& i) -> std::ostream&
{
    out << "[" << i.left << ", " << i.right << "]";
    return out;
}

} // namespace tgraph_disk_reachability
