#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                            uint32_t delta,
                                            Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    uint64_t size = static_cast<uint64_t>(n) * static_cast<uint64_t>(tau)
                    * static_cast<uint64_t>(n);
    h.in = vec::create(size, alloc);
    vec::fill(h.in, 0, alloc);
    h.out = vec::create(size, alloc);
    vec::fill(h.out, tau, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

#ifdef BENCHMARK_ON
    total_opt_space = n * n * tau * sizeof(uint32_t) * 2;
#endif

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_expanded<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                            uint32_t delta, It contacts_begin,
                                            It contacts_end,
                                            Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        add_contact(&h, *it, alloc);
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded<Alloc, PAGE_SIZE>::destroy(header* h,
                                             Alloc<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->in, alloc);
    vec::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded<Alloc, PAGE_SIZE>::clear(header* h, Alloc<PAGE_SIZE>* alloc)
{
    vec::fill(*h->in, 0, alloc);
    vec::fill(*h->out, h->tau, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded<Alloc, PAGE_SIZE>::add_contact(header* h, contact c,
                                                 Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    if(vec::read(h->out, out_access(u, t, v, *h), *alloc) != t)
    {
        std::vector<uint32_t> out(h->n, h->tau);
        if(t + h->delta < h->tau)
        {
            vec::read_n(h->out, out_access(v, t + h->delta, 0, *h), h->n,
                        out.data(), *alloc);
        }
        out[v] = t;

        std::vector<uint32_t> wpluses;
        for(uint32_t wplus = 0; wplus < h->n; ++wplus)
        {
            if(wplus != u && out[wplus] < h->tau)
            {
                wpluses.push_back(wplus);
            }
        }

        std::vector<uint32_t> in(h->n, 0);
        if(t >= h->delta)
        {
            vec::read_n(h->in, in_access(u, t, 0, *h), h->n, in.data(),
                        *alloc);
        }
        in[u] = t + 1;

        std::vector<uint32_t> wminuses;
        for(uint32_t wminus = 0; wminus < h->n; ++wminus)
        {
            if(wminus != v && in[wminus] > 0)
            {
                wminuses.push_back(wminus);
            }
        }

        std::vector<uint32_t> tmp(h->n, h->tau);
        for(uint32_t wminus: wminuses)
        {
            bool replaced = true;
            uint32_t tminus = in[wminus];
            while(replaced && tminus > 0)
            {
                vec::read_n(h->out, out_access(wminus, tminus - 1, 0, *h),
                            h->n, tmp.data(), *alloc);

                replaced = false;
                for(uint32_t wplus: wpluses)
                {
                    if(out[wplus] < tmp[wplus])
                    {
                        tmp[wplus] = out[wplus];
                        replaced = true;
                    }
                }

                if(replaced)
                {
                    vec::write_n(h->out, out_access(wminus, tminus - 1, 0, *h),
                                 h->n, tmp.data(), alloc);
                }

                --tminus;
            }
        }

        std::fill(tmp.begin(), tmp.end(), 0);
        for(uint32_t wplus: wpluses)
        {
            bool replaced = true;
            uint32_t tplus = out[wplus] + h->delta;
            while(replaced && tplus < h->tau + h->delta)
            {
                vec::read_n(h->in, in_access(wplus, tplus, 0, *h), h->n,
                            tmp.data(), *alloc);

                replaced = false;
                for(uint32_t wminus: wminuses)
                {
                    if(in[wminus] > tmp[wminus])
                    {
                        tmp[wminus] = in[wminus];
                        replaced = true;
                    }
                }

                if(replaced)
                {
                    vec::write_n(h->in, in_access(wplus, tplus, 0, *h), h->n,
                                 tmp.data(), alloc);
                }

                ++tplus;
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::can_reach(const header& h, uint32_t u,
                                               uint32_t v, interval i,
                                               const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return std::min(vec::read(h.out, out_access(u, i.left, v, h), alloc)
                        + h.delta,
                    h.tau + h.delta - 1)
           < i.right;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    return {};
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded<Alloc, PAGE_SIZE>::opt_space() -> uint64_t
{
    return total_opt_space;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
    total_opt_space = 0;
}

#endif

} // namespace tgraph_disk_reachability
