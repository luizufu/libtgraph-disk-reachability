#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, Alloc<PAGE_SIZE>* alloc)
    -> header
{
    uint32_t z = std::ceil(std::sqrt(tau));
    header h = {};
    uint64_t headers_per_vertex =
        static_cast<uint64_t>(std::ceil(tau / static_cast<double>(z)))
        + 2;
    uint64_t header_size =
        static_cast<uint64_t>(n) * headers_per_vertex;

    uint32_t parent_size = std::ceil(tau / static_cast<double>(z)) + 1;
    uint32_t child_size = z - 1;
    uint32_t levels = std::ceil(std::log(tau) / std::log(z));

    uint32_t original_size = n * parent_size;
    std::vector<uint32_t> original(original_size + 32, 0);
    std::vector<uint32_t> compressed1(original_size + 1024);
    std::vector<uint32_t> compressed2(original_size + 1024);
    std::vector<uint32_t> compressed3(original_size + 1024);
    std::vector<typename vec_block::header> headers(header_size);

    size_t compressed_size1 = n * 2;
    codec_map(Compressor)
        .encodeArray(original.data(), n * 2, compressed1.data(),
                     compressed_size1);

    size_t compressed_size2 = n * parent_size;
    codec_map(Compressor)
        .encodeArray(original.data(), n * parent_size,
                     compressed2.data(), compressed_size2);

    size_t compressed_size3 = n * child_size;
    codec_map(Compressor)
        .encodeArray(original.data(), n * child_size,
                     compressed3.data(), compressed_size3);

    h.in = vec_header::create(header_size, alloc);
    for(uint32_t i = 0; i < header_size; ++i)
    {
        if(i % headers_per_vertex == 0)
        {
            headers[i] = vec_block::create(compressed_size1, alloc);
            vec_block::write_n(headers[i], 0, compressed_size1,
                               compressed1.data(), alloc);
        }
        else if(i % headers_per_vertex == 1)
        {
            headers[i] = vec_block::create(compressed_size2, alloc);
            vec_block::write_n(headers[i], 0, compressed_size2,
                               compressed2.data(), alloc);
        }
        else
        {
            headers[i] = vec_block::create(compressed_size3, alloc);
            vec_block::write_n(headers[i], 0, compressed_size3,
                               compressed3.data(), alloc);
        }
    }
    vec_header::write_n(h.in, 0, header_size, headers.data(), alloc);

    h.out = vec_header::create(header_size, alloc);
    for(uint32_t i = 0; i < header_size; ++i)
    {
        if(i % headers_per_vertex == 0)
        {
            headers[i] = vec_block::create(compressed_size1, alloc);
            vec_block::write_n(headers[i], 0, compressed_size1,
                               compressed1.data(), alloc);
        }
        else if(i % headers_per_vertex == 1)
        {
            headers[i] = vec_block::create(compressed_size2, alloc);
            vec_block::write_n(headers[i], 0, compressed_size2,
                               compressed2.data(), alloc);
        }
        else
        {
            headers[i] = vec_block::create(compressed_size3, alloc);
            vec_block::write_n(headers[i], 0, compressed_size3,
                               compressed3.data(), alloc);
        }
    }
    vec_header::write_n(h.out, 0, header_size, headers.data(), alloc);

    h.n = n;
    h.tau = tau;
    h.delta = delta;
    h.z = z;
    h.parent_size = parent_size;
    h.child_size = child_size;
    h.levels = levels;
    h.n_nodes = headers_per_vertex;

    return h;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
template<typename It>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, It contacts_begin,
    It contacts_end, Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        add_contact(&h, *it, alloc);
    }

    return h;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::destroy(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    uint64_t headers_per_vertex =
        static_cast<uint64_t>(
            std::ceil(h->tau / static_cast<double>(h->z)))
        + 2;
    uint64_t header_size =
        static_cast<uint64_t>(h->n) * headers_per_vertex;
    std::vector<typename vec_block::header> headers(header_size);

    vec_header::read_n(h->in, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::destroy(&header, alloc);
    }
    vec_header::destroy(&h->in, alloc);

    vec_header::read_n(h->out, 0, header_size, headers.data(), *alloc);
    for(auto& header: headers)
    {
        vec_block::destroy(&header, alloc);
    }
    vec_header::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
    h->z = 0;
    h->parent_size = 0;
    h->child_size = 0;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::clear(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    header tmp = *h;
    destroy(h, alloc);
    *h = create(tmp.n, tmp.tau, tmp.delta, tmp.z, alloc);
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::add_contact(
    header* h, contact c, Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    uint32_t test = 0;
    get_adjacency(*h, u, &v, (h->tau - (t + 1)), &test, 1, true,
                  *alloc);

    if(h->tau - test != t)
    {
        std::vector<uint32_t> vertices(h->n, 0);
        std::iota(vertices.begin(), vertices.end(), 0);
        std::vector<uint32_t> tmp(h->n, 0);

        std::vector<uint32_t> wpluses;
        std::vector<uint32_t> tpluses;
        {
            if(t + h->delta < h->tau)
            {
                get_adjacency(*h, v, &vertices[0],
                              (h->tau - (t + h->delta + 1)), &tmp[0],
                              h->n, true, *alloc);
            }
            tmp[v] = h->tau - t;

            for(uint32_t wplus = 0; wplus < h->n; ++wplus)
            {
                if(wplus != u && tmp[wplus] > 0)
                {
                    wpluses.push_back(wplus);
                    tpluses.push_back(tmp[wplus]);
                }
            }
        }

        std::vector<uint32_t> wminuses;
        std::vector<uint32_t> tminuses;
        {
            std::fill(tmp.begin(), tmp.end(), 0);
            if(t >= h->delta)
            {
                get_adjacency(*h, u, &vertices[0], t - h->delta,
                              &tmp[0], h->n, false, *alloc);
            }
            tmp[u] = t + 1;

            for(uint32_t wminus = 0; wminus < h->n; ++wminus)
            {
                if(wminus != v && tmp[wminus] > 0)
                {
                    wminuses.push_back(wminus);
                    tminuses.push_back(tmp[wminus]);
                }
            }
        }

        for(uint32_t i = 0; i < wminuses.size(); ++i)
        {
            update_adjacency(*h, wminuses[i], &wpluses[0],
                             h->tau - tminuses[i], &tpluses[0],
                             wpluses.size(), true, alloc);
        }

        for(uint32_t i = 0; i < wpluses.size(); ++i)
        {
            update_adjacency(*h, wpluses[i], &wminuses[0],
                             (h->tau - tpluses[i]), &tminuses[0],
                             wminuses.size(), false, alloc);
        }
    }
}

// template<const char* Compressor, template<size_t> typename Alloc,
//          size_t PAGE_SIZE>
// auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::can_reach(
//     const header& h, uint32_t u, uint32_t v, interval interv,
//     const Alloc<PAGE_SIZE>& alloc) -> bool
// {
//     if(u == v)
//     {
//         return true;
//     }

//     interv.right = std::min(interv.right, h.tau + h.delta);

//     if(interv.left >= h.tau || interv.right - interv.left < h.delta)
//     {
//         return false;
//     }

//     uint32_t test;
//     {
//         size_t original_size = h.n * h.parent_size;
//         std::vector<uint32_t> original(original_size + 32);
//         std::vector<uint32_t> compressed(original_size + 1024);

//         auto [block_id1, i1] =
//             root_in_block_location(v, interv.right - 1, h);
//         auto [block_id2, i2] =
//             leaf_in_block_location(v, interv.right - 1, h);

//         auto hb = vec_header::read(h.in, block_id1, alloc);
//         vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
//         original_size = h.n * h.parent_size;
//         decompress_block(compressed.data(), hb.size, original.data(),
//                          original_size);
//         block_adjacent_sum(original.data(), h.n, h.parent_size);
//         test = original[u * h.parent_size + i1];

//         if(i2 > 0 && original[u * h.parent_size + i1 + 1] > test)
//         {
//             hb = vec_header::read(h.in, block_id2, alloc);
//             vec_block::read_n(hb, 0, hb.size, compressed.data(),
//             alloc); original_size = h.n * h.child_size;
//             decompress_block(compressed.data(), hb.size,
//                              original.data(), original_size);
//             block_adjacent_sum(original.data(), h.n, h.child_size);
//             test += original[u * h.child_size + i2 - 1];
//         }
//     }
//     return interv.left <= test;
// }

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval interv,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    interv.right = std::min(interv.right, h.tau + h.delta);

    if(interv.left >= h.tau || interv.right - interv.left < h.delta)
    {
        return false;
    }

    uint32_t test = 0;
    get_adjacency(h, u, &v, h.tau - (interv.left + 1), &test, 1, true,
                  alloc);

    return std::min((h.tau - test) + h.delta, h.tau + h.delta - 1)
           < interv.right;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    return {};
    // if(u == v)
    // {
    //     return {};
    // }

    // i.right = std::min(i.right, h.tau + h.delta);

    // if(i.left >= h.tau || i.right - i.left < h.delta)
    // {
    //     return {};
    // }

    // auto [tplus, _] = vec::read(h.out, out_access(u, i.left, v,
    // h), alloc); tplus += h.delta; if(std::min(tplus, h.tau +
    // h.delta - 1)
    // >= i.right)
    // {
    //     return {};
    // }

    // auto [tminus, succ] = vec::read(h.in, in_access(v, tplus, u,
    // h), alloc);
    // --tminus;
    // journey j({u, succ, tminus}, h.delta);

    // while(succ != v)
    // {
    //     auto [next_tminus, next_succ] =
    //         vec::read(h.in, in_access(v, tplus, succ, h), alloc);
    //     next_tminus--;

    //     j.push_back({succ, next_succ, next_tminus});

    //     succ = next_succ;
    //     tminus = next_tminus;
    // }

    // return j;
}

#ifdef BENCHMARK_ON

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::binary_searches()
    -> uint64_t
{
    return total_binary_searches;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc,
                     PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::tuple_inserts()
    -> uint64_t
{
    return total_inserts;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::tuple_updates()
    -> uint64_t
{
    return total_updates;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::tuple_erases()
    -> uint64_t
{
    return 0;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::opt_space()
    -> uint64_t
{
    return total_opt_space;
}

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void ttc_compressed5<Compressor, Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
    total_opt_space = 0;
}

#endif

} // namespace tgraph_disk_reachability
