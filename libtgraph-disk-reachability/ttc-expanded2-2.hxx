#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <deque>
#include <iomanip>
#include <iostream>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class ttc_expanded22
{
    static const uint32_t NIL = std::numeric_limits<uint32_t>::max();
    struct entry
    {
        uint32_t t = NIL;
        uint32_t succ = NIL;
    };
    using vec = disk::vector<entry, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        typename vec::header in = {};
        typename vec::header out = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c, Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i, const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto print(const header& h, const Alloc<PAGE_SIZE>& alloc)
    {
        uint32_t nil = std::numeric_limits<uint32_t>::max();

        std::cout << "\nout   ";
        for(uint32_t k = 0; k < h.n; ++k)
        {
            std::cout << std::setw(3) << std::right << k;
            std::cout << " ";
        }
        std::cout << "    ";
        for(uint32_t k = 0; k < h.n; ++k)
        {
            std::cout << std::setw(3) << std::right << k;
            std::cout << " ";
        }
        std::cout << '\n' << std::endl;

        for(uint32_t u = 0; u < h.n; ++u)
        {
            for(uint32_t t = 0; t < h.tau; ++t)
            {
                std::cout << std::setw(6) << std::left
                          << (std::to_string(u) + "-" + std::to_string(t));
                for(uint32_t v = 0; v < h.n; ++v)
                {
                    auto [tplus, next] =
                        vec::read(h.out, out_access(u, t, v, h), alloc);
                    if(tplus == nil)
                    {
                        std::cout << "  X ";
                    }
                    else
                    {
                        std::cout << std::setw(3) << std::right << tplus;
                        std::cout << " ";
                    }
                }

                std::cout << "    ";

                for(uint32_t v = 0; v < h.n; ++v)
                {
                    auto [tplus, next] =
                        vec::read(h.out, out_access(u, t, v, h), alloc);
                    if(next == h.n)
                    {
                        std::cout << "  X ";
                    }
                    else
                    {
                        std::cout << std::setw(3) << std::right << next;
                        std::cout << " ";
                    }
                }

                std::cout << std::endl;
            }
        }

        // std::cout << "\nin    ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << "    ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << '\n' << std::endl;
        // for(uint32_t v = 0; v < h.n; ++v)
        // {
        //     for(uint32_t t = 0; t < h.tau; ++t)
        //     {
        //         std::cout << std::setw(6) << std::left
        //                   << (std::to_string(v) + "-" + std::to_string(t));
        //         for(uint32_t u = 0; u < h.n; ++u)
        //         {
        //             auto [tminus, next] =
        //                 vec::read(h.in, in_access(v, t, u, h.n, h.tau),
        //                 alloc);
        //             if(tminus == nil)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right << tminus;
        //                 std::cout << " ";
        //             }
        //         }
        //         std::cout << "    ";
        //         for(uint32_t u = 0; u < h.n; ++u)
        //         {
        //             auto [tminus, next] =
        //                 vec::read(h.in, in_access(v, t, u, h),
        //                 alloc);
        //             if(next == h.n)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right << next;
        //                 std::cout << " ";
        //             }
        //         }

        //         std::cout << std::endl;
        //     }
        // }
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif
private:
    [[nodiscard]] static auto out_access(uint64_t u, uint64_t t, uint64_t v,
                                         const header& h) -> uint64_t
    {
        return (v * h.n + u) * h.tau + (h.tau - t - 1);
    }

    [[nodiscard]] static auto in_access(uint64_t u, uint64_t t, uint64_t v,
                                        const header& h) -> uint64_t
    {
        return (v * h.n + u) * h.tau + t - h.delta;
    }
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-expanded2-2.ixx>
