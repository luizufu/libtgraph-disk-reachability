#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                             uint32_t delta,
                                             Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    uint64_t size = static_cast<uint64_t>(n) * static_cast<uint64_t>(tau)
                    * static_cast<uint64_t>(n);
    h.out = vec::create(size, alloc);
    vec::fill(h.out, {NIL, n}, alloc);
    h.in = vec::create(size, alloc);
    vec::fill(h.in, {NIL, n}, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_expanded3<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                             uint32_t delta, It contacts_begin,
                                             It contacts_end,
                                             Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        if(vec::read(h.out, out_access(u, t, v, h), *alloc).t != t + h.delta)
        {
            std::vector<std::pair<uint32_t, entry>> out;
            out.push_back({v, entry {t + h.delta, v}});

            std::vector<std::pair<uint32_t, entry>> in;
            {
                std::vector<entry> row(h.n);
                if(t >= h.delta)
                {
                    for(uint32_t wminus = 0; wminus < h.n; ++wminus)
                    {
                        if(wminus != u || wminus != v)
                        {
                            row[wminus] = vec::read(
                                h.in, in_access(u, t, wminus, h), *alloc);
                        }
                    }
                }
                row[u] = {t, v};

                for(uint32_t wminus = 0; wminus < h.n; ++wminus)
                {
                    if(wminus != v && row[wminus].t != NIL)
                    {
                        in.emplace_back(wminus, row[wminus]);
                    }
                }
            }

            for(const auto& [wminus, e]: in)
            {
                std::deque<std::tuple<uint32_t, uint32_t, uint32_t>> q;
                for(const auto& [wplus, e2]: out)
                {
                    q.emplace_back(e.t, wplus, e2.t);
                }

                while(!q.empty())
                {
                    auto& [tminus, wplus, tplus] = q.back();
                    q.pop_back();

                    uint64_t idx = out_access(wminus, tminus, wplus, h);
                    uint32_t t2 = vec::read(h.out, idx, *alloc).t;
                    if(t2 == NIL || tplus < t2)
                    {
                        vec::write(h.out, idx, {tplus, e.succ}, alloc);

                        if(tminus > 0)
                        {
                            q.emplace_front(tminus - 1, wplus, tplus);
                        }
                    }
                }
            }

            for(const auto& [wplus, e]: out)
            {
                std::deque<std::tuple<uint32_t, uint32_t, uint32_t>> q;
                for(const auto& [wminus, e2]: in)
                {
                    q.emplace_back(e.t, wminus, e2.t);
                }

                while(!q.empty())
                {
                    auto& [tplus, wminus, tminus] = q.front();
                    q.pop_front();

                    uint64_t idx = in_access(wplus, tplus, wminus, h);
                    uint32_t t1 = vec::read(h.in, idx, *alloc).t;
                    if(t1 == NIL || tminus > t1)
                    {
                        vec::write(h.in, idx, {tminus, e.succ}, alloc);
                        if(tplus + 1 < h.tau + h.delta)
                        {
                            q.emplace_back(tplus + 1, wminus, tminus);
                        }
                    }
                }
            }
        }
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded3<Alloc, PAGE_SIZE>::destroy(header* h,
                                              Alloc<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->in, alloc);
    vec::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded3<Alloc, PAGE_SIZE>::clear(header* h, Alloc<PAGE_SIZE>* alloc)
{
    vec::fill(*h->in, {NIL, h->n}, alloc);
    vec::fill(*h->out, {NIL, h->n}, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded3<Alloc, PAGE_SIZE>::add_contact(header* h, contact c,
                                                  Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    if(vec::read(h->out, out_access(u, t, v, *h), *alloc).t != t + h->delta)
    {
        std::vector<entry> out(h->n);
        std::vector<uint32_t> out_i;
        {
            if(t + h->delta < h->tau)
            {
                vec::read_n(h->out, out_access(v, t + h->delta, 0, *h), h->n,
                            out.data(), *alloc);
            }
            out[v] = {t + h->delta, v};
            for(uint32_t wplus = 0; wplus < h->n; ++wplus)
            {
                if(wplus != u && out[wplus].t != NIL)
                {
                    out_i.push_back(wplus);
                }
            }
        }

        std::vector<entry> in(h->n);
        std::vector<uint32_t> in_i;
        {
            if(t >= h->delta)
            {
                vec::read_n(h->in, in_access(u, t, 0, *h), h->n, in.data(),
                            *alloc);
            }
            in[u] = {t, v};
            for(uint32_t wminus = 0; wminus < h->n; ++wminus)
            {
                if(wminus != v && in[wminus].t != NIL)
                {
                    in_i.push_back(wminus);
                }
            }
        }

        for(const auto& wminus: in_i)
        {
            std::vector<uint32_t> out_i2_next = out_i;
            std::vector<uint64_t> indices_next(out_i.size());
            std::vector<entry> tmp_next(out_i.size());

            for(uint32_t i = 0; i < out_i.size(); ++i)
            {
                indices_next[i] =
                    out_access(wminus, in[wminus].t, out_i[i], *h);
            }
            for(uint32_t t = in[wminus].t + 1; !indices_next.empty() && t > 0;
                --t)
            {
                auto out_i2 = std::move(out_i2_next);
                auto indices = std::move(indices_next);
                auto tmp = std::move(tmp_next);

                vec::read_is(h->out, indices.data(), indices.size(),
                             tmp.data(), false, *alloc);

                for(uint32_t i = 0; i < out_i2.size(); ++i)
                {
                    if(tmp[i].t == NIL || out[out_i2[i]].t < tmp[i].t)
                    {
                        out_i2_next.push_back(std::move(out_i2[i]));
                        indices_next.push_back(std::move(indices[i]));
                        tmp_next.push_back(std::move(out[out_i2[i]]));
                    }
                }

                if(!indices_next.empty())
                {
                    vec::write_is(h->out, indices_next.data(),
                                  indices_next.size(), tmp_next.data(), false,
                                  alloc);

                    for(auto& idx: indices_next)
                    {
                        idx += h->n;
                    }
                }
            }
        }

        for(const auto& wplus: out_i)
        {
            std::vector<uint32_t> in_i2_next = in_i;
            std::vector<uint64_t> indices_next(in_i.size());
            std::vector<entry> tmp_next(in_i.size());

            for(uint32_t i = 0; i < in_i.size(); ++i)
            {
                indices_next[i] = in_access(wplus, out[wplus].t, in_i[i], *h);
            }
            for(uint32_t t = out[wplus].t;
                !indices_next.empty() && t < h->tau + h->delta; ++t)
            {
                auto in_i2 = std::move(in_i2_next);
                auto indices = std::move(indices_next);
                auto tmp = std::move(tmp_next);

                vec::read_is(h->in, indices.data(), indices.size(), tmp.data(),
                             false, *alloc);

                for(uint32_t i = 0; i < in_i2.size(); ++i)
                {
                    if(tmp[i].t == NIL || in[in_i2[i]].t > tmp[i].t)
                    {
                        in_i2_next.push_back(std::move(in_i2[i]));
                        indices_next.push_back(std::move(indices[i]));
                        tmp_next.push_back(std::move(in[in_i2[i]]));
                    }
                }

                if(!in_i2_next.empty())
                {
                    vec::write_is(h->in, indices_next.data(),
                                  indices_next.size(), tmp_next.data(), false,
                                  alloc);

                    for(auto& idx: indices_next)
                    {
                        idx += h->n;
                    }
                }
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::can_reach(const header& h, uint32_t u,
                                                uint32_t v, interval i,
                                                const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return std::min(vec::read(h.out, out_access(u, i.left, v, h), alloc).t,
                    h.tau + h.delta - 1)
           < i.right;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    auto [tplus, _] = vec::read(h.out, out_access(u, i.left, v, h), alloc);
    if(std::min(tplus, h.tau + h.delta - 1) >= i.right)
    {
        return {};
    }

    auto [tminus, succ] = vec::read(h.in, in_access(v, tplus, u, h), alloc);
    journey j({u, succ, tminus}, h.delta);

    while(succ != v)
    {
        auto [next_tminus, next_succ] =
            vec::read(h.in, in_access(v, tplus, succ, h), alloc);

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded3<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded3<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
}

#endif

} // namespace tgraph_disk_reachability
