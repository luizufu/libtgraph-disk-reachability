#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability::detail4
{
const static uint32_t null = std::numeric_limits<uint32_t>::max();
} // namespace tgraph_disk_reachability::detail4

namespace tgraph_disk_reachability
{
auto by_in_access3::operator()(uint32_t u, uint32_t t, uint32_t v, uint32_t n,
                               uint32_t tau) const -> uint32_t
{
    return (u * tau + t) * n + v;
    /* return (v * n + u) * tau + t; */ // TODO investigar
}

auto by_out_access3::operator()(uint32_t u, uint32_t t, uint32_t v, uint32_t n,
                                uint32_t tau) const -> uint32_t
{
    return (u * tau + t) * n + v;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, disk::allocator<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    h.in = vec::create(n * tau * n, alloc);
    vec::fill(h.in, {detail4::null, n}, alloc);
    h.out = vec::create(n * tau * n, alloc);
    vec::fill(h.out, {detail4::null, n}, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    return h;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
template<typename It>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, It contacts_begin,
    It contacts_end, disk::allocator<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);
    InAccess in_access;

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        if(insert(&h, u, v, t, t + h.delta, v, alloc))
        {
            if(t >= h.delta)
            {
                for(uint32_t wminus = 0; wminus < h.n; ++wminus)
                {
                    if(wminus == v)
                    {
                        continue;
                    }

                    if(const auto& [tminus, succ] = vec::read(
                           h.in, in_access(u, t - h.delta, wminus, h.n, h.tau),
                           *alloc);
                       tminus != detail4::null)
                    {
                        insert(&h, wminus, v, tminus, t + h.delta, succ, alloc);
                    }
                }
#ifdef BENCHMARK_ON
                ++total_sequential_searches;
#endif
            }
        }
    }

    return h;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::destroy(
    header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->in, alloc);
    vec::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::clear(
    header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    vec::fill(*h->in, {detail4::null, h->n}, alloc);
    vec::fill(*h->out, {detail4::null, h->n}, alloc);
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::add_contact(
    header* h, contact c, disk::allocator<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }
    OutAccess out_access;
    InAccess in_access;

    if(vec::read(h->out, out_access(u, t, v, h->n, h->tau), *alloc).t
       != t + h->delta)
    {
        std::vector<entry> row(h->n);

        if(t + h->delta < h->tau)
        {
            collect_out(*h, v, t + h->delta, out_access, &row, *alloc);
            row[v] = {t + h->delta, v};
            auto out = filter_out(*h, u, t, out_access, row, *alloc);
            insert_from_out(h, u, t, out, v, alloc);

            if(t >= h->delta)
            {
                collect_in(*h, u, t - h->delta, in_access, &row, *alloc);

                for(const auto& [wminus, tminus, succ]:
                    filter_in(*h, v, t + h->delta, out_access, row, *alloc))
                {
                    insert_from_out(h, wminus, tminus, out, succ, alloc);
                }
            }
        }
    }
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval i,
    const disk::allocator<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return std::min(
               vec::read(h.out, OutAccess {}(u, i.left, v, h.n, h.tau), alloc)
                   .t,
               h.tau + h.delta - 1)
           < i.right;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const disk::allocator<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    OutAccess out_access;
    auto [tplus, _] =
        vec::read(h.out, out_access(u, i.left, v, h.n, h.tau), alloc);
    if(std::min(tplus, h.tau + h.delta - 1) >= i.right)
    {
        return {};
    }

    InAccess in_access;
    auto [tminus, succ] =
        vec::read(h.in, in_access(v, tplus - h.delta, u, h.n, h.tau), alloc);
    journey j({u, succ, tminus}, h.delta);

    while(succ != v)
    {
        auto [next_tminus, next_succ] = vec::read(
            h.in, in_access(v, tplus - h.delta, succ, h.n, h.tau), alloc);

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
}

#endif

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::insert_from_out(
    header* h, uint32_t u, uint32_t t1,
    const std::vector<std::pair<uint32_t, uint32_t>>& d, uint32_t succ,
    disk::allocator<PAGE_SIZE>* alloc)
{
    InAccess in_access;
    OutAccess out_access;

    std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> dd;
    for(auto& [v, t2]: d)
    {
        dd.emplace_back(v, t2, t1);
    }

    while(!dd.empty())
    {
        for(auto it = dd.rbegin(); it != dd.rend();)
        {
            auto& [v, t2, tt1] = *it;

            uint32_t idx = out_access(u, tt1, v, h->n, h->tau);
            uint32_t tplus = vec::read(h->out, idx, *alloc).t;

            if(tplus != detail4::null && tplus <= t2)
            {
                dd.erase((++it).base());
                continue;
            }

            vec::write(h->out, idx, {t2, succ}, alloc);

            if(tt1 == 0)
            {
                dd.erase((++it).base());
            }
            else
            {
                --tt1;
                ++it;
            }
        }
    }

    for(auto& [v, t2]: d)
    {
        dd.emplace_back(v, t2, t1);
    }

    while(!dd.empty())
    {
        for(auto it = dd.begin(); it != dd.end();)
        {
            auto& [v, tt2, _] = *it;

            uint32_t idx = in_access(v, tt2 - h->delta, u, h->n, h->tau);
            uint32_t tminus = vec::read(h->in, idx, *alloc).t;

            if(tt2 == h->tau + h->delta
               || (tminus != detail4::null && tminus >= t1))
            {
                dd.erase(it);
                continue;
            }

            vec::write(h->in, idx, {t1, succ}, alloc);
            ++tt2;
            ++it;
        }
    }
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::collect_in(
    const header& h, uint32_t u, uint32_t t, InAccess access,
    std::vector<entry>* row, const disk::allocator<PAGE_SIZE>& alloc)
{
    for(uint32_t wminus = 0; wminus < h.n; ++wminus)
    {
        (*row)[wminus] =
            vec::read(h.in, access(u, t, wminus, h.n, h.tau), alloc);
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc34<InAccess, OutAccess, PAGE_SIZE>::collect_out(
    const header& h, uint32_t u, uint32_t t, OutAccess access,
    std::vector<entry>* row, const disk::allocator<PAGE_SIZE>& alloc)
{
    std::vector<entry> entries;
    for(uint32_t wplus = h.n; wplus > 0; --wplus)
    {
        (*row)[wplus - 1] =
            vec::read(h.out, access(u, t, wplus - 1, h.n, h.tau), alloc);
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::filter_in(
    const header& h, uint32_t v, uint32_t t2, OutAccess access,
    const std::vector<entry>& row, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::vector<std::tuple<uint32_t, uint32_t, uint32_t>>
{
    std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> filtered;
    InAccess access2;
    for(uint32_t u = 0; u < h.n; ++u)
    {
        auto const& [t1, succ] = row[u];
        if(u != v && t1 != detail4::null)
        {
            uint32_t t =
                vec::read(h.out, access(u, t1, v, h.n, h.tau), alloc).t;

            /* uint32_t t = */
            /*     vec::read(h.in, access2(v, t2, u, h.n, h.tau), alloc).t; */
            if(t > t2)
            /* if(t == detail4::null || t < t1) */
            {
                filtered.emplace_back(u, t1, succ);
            }
        }
    }

    return filtered;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc34<InAccess, OutAccess, PAGE_SIZE>::filter_out(
    const header& h, uint32_t u, uint32_t t, OutAccess access,
    const std::vector<entry>& row, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::vector<std::pair<uint32_t, uint32_t>>
{
    std::vector<std::pair<uint32_t, uint32_t>> filtered;
    for(uint32_t v = 0; v < h.n; ++v)
    {
        if(u != v
           && vec::read(h.out, access(u, t, v, h.n, h.tau), alloc).t > row[v].t)
        {
            filtered.emplace_back(v, row[v].t);
        }
    }

    return filtered;
}

} // namespace tgraph_disk_reachability
