#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <stack>
#include <algorithm>
#include <deque>
#include <fastpfor/codecfactory.h>
#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric>
#include <span>
namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class ttc_compressed3
{
    using vec_block = disk::vector<uint32_t, Alloc, PAGE_SIZE>;
    using vec_header =
        disk::vector<typename vec_block::header, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        static constexpr uint32_t MAX_LEVELS =
            (PAGE_SIZE - 32) / sizeof(typename vec_header::header) / 2;

        std::array<typename vec_header::header, MAX_LEVELS> out;
        std::array<typename vec_header::header, MAX_LEVELS> in;
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
        uint32_t z = 1;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       uint32_t z, Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       uint32_t z, It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c,
                            Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v,
                          interval i, const Alloc<PAGE_SIZE>& alloc)
        -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u,
                                    uint32_t v, interval i,
                                    const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto space(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> uint64_t
    {
        uint64_t total = 0;

        {
            for(uint32_t lv = 0; lv < std::log(h.tau) / std::log(h.z);
                ++lv)
            {
                auto it = vec_header::begin(h.in[lv], alloc);
                auto end = vec_header::end(h.in[lv], alloc);

                for(; it != end; ++it)
                {
                    total += sizeof(*it) + it->size * sizeof(uint32_t);
                }
            }
        }

        {
            for(uint32_t lv = 0; lv < std::log(h.tau) / std::log(h.z);
                ++lv)
            {
                auto it = vec_header::begin(h.out[lv], alloc);
                auto end = vec_header::end(h.out[lv], alloc);

                for(; it != end; ++it)
                {
                    total += sizeof(*it) + it->size * sizeof(uint32_t);
                }
            }
        }

        return total;
    }

    static auto print(const header& h, const Alloc<PAGE_SIZE>& alloc)
    {
        // uint32_t nil = std::numeric_limits<uint32_t>::max();

        // std::cout << "\nout   ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << "    ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << '\n' << std::endl;

        // for(uint32_t u = 0; u < h.n; ++u)
        // {
        //     for(uint32_t t = 0; t < h.tau; ++t)
        //     {
        //         std::cout << std::setw(6) << std::left
        //                   << (std::to_string(u) + "-" +
        //                   std::to_string(t));
        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h),
        //                 alloc);
        //             if(tplus == nil)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right <<
        //                 tplus; std::cout << " ";
        //             }
        //         }

        //         std::cout << "    ";

        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h),
        //                 alloc);
        //             if(next == h.n)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right <<
        //                 next; std::cout << " ";
        //             }
        //         }

        //         std::cout << std::endl;
        //     }
        // }

        // // std::cout << "\nin    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << "    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << '\n' << std::endl;
        // // for(uint32_t v = 0; v < h.n; ++v)
        // // {
        // //     for(uint32_t t = 0; t < h.tau; ++t)
        // //     {
        // //         std::cout << std::setw(6) << std::left
        // //                   << (std::to_string(v) + "-" +
        // std::to_string(t));
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h.n,
        // h.tau),
        // //                 alloc);
        // //             if(tminus == nil)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // tminus;
        // //                 std::cout << " ";
        // //             }
        // //         }
        // //         std::cout << "    ";
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h),
        // //                 alloc);
        // //             if(next == h.n)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // next;
        // //                 std::cout << " ";
        // //             }
        // //         }

        // //         std::cout << std::endl;
        // //     }
        // // }
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static auto opt_space() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static uint64_t total_opt_space = 0;
#endif
private:
    struct block_location
    {
        uint64_t block_id = 0;
        uint32_t block_pos = 0;
        uint32_t level = 0;
    };

    struct search_range
    {
        uint32_t current_index = 0;
        uint32_t range_start = 0;
        uint32_t range_size = 0;
    };

    [[nodiscard]] static auto out_leaf_block_location(uint64_t tminus,
                                                      const header& h)
        -> block_location
    {
        return {.block_id = (h.tau - (tminus + 1)) / h.z,
                .block_pos =
                    static_cast<uint32_t>((h.tau - (tminus + 1)) % h.z),
                .level = static_cast<uint32_t>(
                    std::ceil(std::log(h.tau) / std::log(h.z)))};
    }

    [[nodiscard]] static auto in_leaf_block_location(uint64_t tplus,
                                                     const header& h)
        -> block_location
    {
        return {.block_id = (tplus - h.delta) / h.z,
                .block_pos =
                    static_cast<uint32_t>((tplus - h.delta) % h.z),
                .level = static_cast<uint32_t>(
                    std::ceil(std::log(h.tau) / std::log(h.z)))};
    }

    static auto
    compute_parent_locations(block_location initial_location,
                             const header& h)
        -> std::vector<block_location>
    {
        std::vector<block_location> locations;
        locations.push_back(initial_location);

        while(locations.back().level > 1)
        {
            auto [block_id, _, level] = locations.back();
            locations.push_back(
                {.block_id = static_cast<uint64_t>(block_id / h.z),
                 .block_pos = static_cast<uint32_t>(block_id % h.z),
                 .level = level - 1});
        }

        return locations;
    }

    [[nodiscard]] static auto update_in_adjacencies(
        header* h, uint32_t wplus, std::vector<uint32_t> tminuses,
        std::vector<uint32_t> sum,
        std::vector<block_location>& locations, Alloc<PAGE_SIZE>* alloc)
        -> std::vector<uint32_t>
    {
        if(locations.empty())
        {
            std::transform(sum.begin(), sum.begin() + h->n,
                           tminuses.begin(), sum.begin(),
                           [](uint32_t l, uint32_t r)
                           {
                               return std::max(l, r);
                           });
            return sum;
        }

        auto current_location = locations.back();
        locations.pop_back();

        size_t original_size = h->n * (h->z - 1);
        size_t compressed_size = h->n * (h->z - 1);
        std::vector<uint32_t> uncompressed(original_size + 32, 0);
        std::vector<uint32_t> compressed(compressed_size + 1024);

        uint32_t blocks_per_vertice =
            std::pow(h->z, current_location.level - 1);
        uint32_t mapped_block_id =
            wplus * blocks_per_vertice + current_location.block_id;
        auto hb = vec_header::read(h->in[current_location.level],
                                   mapped_block_id, *alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
        original_size = h->n * (h->z - 1);
        decompress_block(compressed.data(), hb.size,
                         uncompressed.data(), original_size);

        std::vector<uint32_t> ret_sum;
        std::vector<uint32_t> next_sum;
        if(current_location.block_pos == 0)
        {
            ret_sum = update_in_adjacencies(h, wplus, tminuses, sum,
                                            locations, alloc);
            next_sum = ret_sum;
            ++current_location.block_pos;
        }
        else
        {
            ret_sum = sum;
            if(current_location.block_pos > 1)
            {
                for(uint32_t wminus = 0; wminus < h->n; ++wminus)
                {
                    sum[wminus] += std::accumulate(
                        uncompressed.begin() + wminus * (h->z - 1),
                        uncompressed.begin() + wminus * (h->z - 1)
                            + current_location.block_pos - 1,
                        0);
                }
            }

            next_sum = sum;
        }

        for(uint32_t i = current_location.block_pos; i < h->z; ++i)
        {
            for(uint32_t wminus = 0; wminus < h->n; ++wminus)
            {
                sum[wminus] +=
                    uncompressed[wminus * (h->z - 1) + i - 1];
            }

            auto child_sum = update_in_adjacencies(
                h, wplus, tminuses, sum, locations, alloc);

            for(uint32_t wminus = 0; wminus < h->n; ++wminus)
            {
                uncompressed[wminus * (h->z - 1) + i - 1] =
                    child_sum[wminus] - next_sum[wminus];

                next_sum[wminus] +=
                    uncompressed[wminus * (h->z - 1) + i - 1];
            }
        }

        compressed_size = h->n * (h->z - 1);
        compress_block(uncompressed.data(), h->n * (h->z - 1),
                       compressed.data(), compressed_size);
        vec_block::resize(&hb, compressed_size, alloc);
        vec_block::write_n(hb, 0, compressed_size, compressed.data(),
                           alloc);

        vec_header::write(h->in[current_location.level],
                          mapped_block_id, hb, alloc);

        ++current_location.block_id;
        current_location.block_pos = 0;
        locations.push_back(current_location);

        return ret_sum;
    }

    static void update_out_adjacencies(header* h, uint32_t wminus,
                                       uint32_t tminus,
                                       std::vector<uint32_t>& wpluses,
                                       std::vector<uint32_t>& tpluses,
                                       Alloc<PAGE_SIZE>* alloc)
    {
        size_t original_size = h->n;
        size_t compressed_size = h->n;
        std::vector<uint32_t> sum(original_size + 32, 0);
        std::vector<uint32_t> compressed(compressed_size + 1024);

        auto hb = vec_header::read(h->out[0], wminus, *alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
        decompress_block(compressed.data(), hb.size, sum.data(),
                         original_size);

        tminus = h->tau - (tminus + 1);

        if(tminus == 0)
        {
            std::transform(sum.begin(), sum.begin() + h->n,
                           tpluses.begin(), sum.begin(),
                           [](uint32_t a, uint32_t b) -> uint32_t
                           {
                               return std::min(a, b);
                           });

            compress_block(sum.data(), h->n, compressed.data(),
                           compressed_size);
            vec_block::resize(&hb, compressed_size, alloc);
            vec_block::write_n(hb, 0, compressed_size,
                               compressed.data(), alloc);

            vec_header::write(h->out[0], wminus, hb, alloc);
        }

        uint32_t child_width = h->tau / h->z;
        uint32_t child_tminus_rest = tminus % child_width;

        block_location child_location = {.block_id = 0,
                                         .block_pos =
                                             tminus / child_width,
                                         .level = 1};

        update_out_adjacencies_rec(h, wminus, tminus, wpluses, tpluses,
                                   sum, child_location, child_width,
                                   child_tminus_rest, alloc);
    }

    static void update_out_adjacencies_rec(
        header* h, uint32_t wminus, uint32_t tminus,
        std::vector<uint32_t>& wpluses, std::vector<uint32_t>& tpluses,
        std::vector<uint32_t> sum, block_location location,
        uint32_t width, uint32_t tminus_rest, Alloc<PAGE_SIZE>* alloc)
    {
        if(width == 0)
        {
            return;
        }

        size_t original_size = h->n * (h->z - 1);
        size_t compressed_size = h->n * (h->z - 1);
        std::vector<uint32_t> uncompressed(original_size + 32, 0);
        std::vector<uint32_t> compressed(compressed_size + 1024);

        uint32_t n_blocks = std::pow(h->z, location.level - 1);
        uint32_t mapped_block_id =
            wminus * n_blocks + location.block_id;
        auto hb = vec_header::read(h->out[location.level],
                                   mapped_block_id, *alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
        original_size = h->n * (h->z - 1);
        decompress_block(compressed.data(), hb.size,
                         uncompressed.data(), original_size);

        if(location.block_pos > 1)
        {
            for(uint32_t wplus: wpluses)
            {
                sum[wplus] -= std::accumulate(
                    uncompressed.begin() + wplus * (h->z - 1),
                    uncompressed.begin() + wplus * (h->z - 1)
                        + location.block_pos - 1,
                    0);
            }
        }

        uint32_t child_width = width / h->z;
        uint32_t child_tminus_rest = tminus_rest;
        block_location child_location = {};

        if(child_width > 0)
        {
            child_location.block_pos = tminus_rest / child_width;
            child_location.level = location.level + 1;
            child_tminus_rest = tminus_rest % child_width;
        }

        for(uint32_t i = location.block_pos; i < h->z;
            ++i, child_location.block_pos = 0)
        {
            child_location.block_id = location.block_id * n_blocks + i;
            bool is_next_sum_equal = i < (h->z - 1);
            bool is_tminus_at_left_of_range =
                tminus <= child_location.block_id * width;
            bool is_all_unchanged = i > 0 && is_tminus_at_left_of_range;

            for(uint32_t wplus: wpluses)
            {
                if(i > 0)
                {
                    uint32_t diff =
                        uncompressed[wplus * (h->z - 1) + i - 1];

                    if(is_tminus_at_left_of_range
                       && tpluses[wplus] < sum[wplus] - diff)
                    {
                        is_all_unchanged = false;
                        uncompressed[wplus * (h->z - 1) + i - 1] =
                            sum[wplus] - tpluses[wplus];
                    }

                    sum[wplus] -= diff;
                }

                if(is_next_sum_equal
                   && uncompressed[wplus * (h->z - 1) + i] != 0)
                {
                    is_next_sum_equal = false;
                }
            }

            if(is_all_unchanged)
            {
                break;
            }

            if(!is_tminus_at_left_of_range && !is_next_sum_equal)
            {
                update_out_adjacencies_rec(h, wminus, tminus, wpluses,
                                           tpluses, sum, child_location,
                                           child_width,
                                           child_tminus_rest, alloc);
            }
        }

        compressed_size = h->n * (h->z - 1);
        compress_block(uncompressed.data(), h->n * (h->z - 1),
                       compressed.data(), compressed_size);
        vec_block::resize(&hb, compressed_size, alloc);
        vec_block::write_n(hb, 0, compressed_size, compressed.data(),
                           alloc);

        vec_header::write(h->out[location.level], mapped_block_id, hb,
                          alloc);
    }

    // [[nodiscard]] static auto update_out_adjacencies(
    //     header* h, uint32_t wminus, std::vector<uint32_t> tpluses,
    //     std::vector<uint32_t> sum, std::vector<block_location>
    //     locations, Alloc<PAGE_SIZE>* alloc) -> std::vector<uint32_t>
    // {
    //     if(locations.empty())
    //     {
    //         std::transform(sum.begin(), sum.begin() + h->n,
    //         tpluses.begin(),
    //                        sum.begin(),
    //                        [](uint32_t l, uint32_t r)
    //                        {
    //                            return std::min(l, r);
    //                        });
    //         return sum;
    //     }

    //     auto current_location = locations.back();
    //     locations.pop_back();

    //     size_t original_size = h->n * (h->z - 1);
    //     size_t compressed_size = h->n * (h->z - 1);
    //     std::vector<uint32_t> uncompressed(original_size + 32, 0);
    //     std::vector<uint32_t> compressed(compressed_size + 1024);

    //     uint32_t blocks_per_vertice =
    //         std::pow(h->z, current_location.level - 1);
    //     uint32_t mapped_block_id =
    //         wminus * blocks_per_vertice + current_location.block_id;
    //     auto hb = vec_header::read(h->out[current_location.level],
    //                                mapped_block_id, *alloc);
    //     vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
    //     original_size = h->n * (h->z - 1);
    //     decompress_block(compressed.data(), hb.size,
    //     uncompressed.data(),
    //                      original_size);

    //     std::vector<uint32_t> ret_sum;
    //     std::vector<uint32_t> next_sum;

    //     if(current_location.block_pos == 0)
    //     {
    //         ret_sum = update_out_adjacencies(h, wminus, tpluses, sum,
    //                                          locations, alloc);
    //         next_sum = ret_sum;
    //         ++current_location.block_pos;
    //     }
    //     else
    //     {
    //         ret_sum = sum;
    //         if(current_location.block_pos > 1)
    //         {
    //             for(uint32_t wplus = 0; wplus < h->n; ++wplus)
    //             {
    //                 sum[wplus] -= std::accumulate(
    //                     uncompressed.begin() + wplus * (h->z - 1),
    //                     uncompressed.begin() + wplus * (h->z - 1)
    //                         + current_location.block_pos - 1,
    //                     0);
    //             }
    //         }

    //         next_sum = sum;
    //     }

    //     for(uint32_t i = current_location.block_pos; i < h->z; ++i)
    //     {
    //         for(uint32_t wplus = 0; wplus < h->n; ++wplus)
    //         {
    //             sum[wplus] -= uncompressed[wplus * (h->z - 1) + i -
    //             1];
    //         }

    //         auto child_sum = update_out_adjacencies(h, wminus,
    //         tpluses, sum,
    //                                                 locations,
    //                                                 alloc);

    //         for(uint32_t wplus = 0; wplus < h->n; ++wplus)
    //         {
    //             uncompressed[wplus * (h->z - 1) + i - 1] =
    //                 next_sum[wplus] - child_sum[wplus];

    //             next_sum[wplus] -= uncompressed[wplus * (h->z - 1) +
    //             i - 1];
    //         }
    //     }

    //     compressed_size = h->n * (h->z - 1);
    //     compress_block(uncompressed.data(), h->n * (h->z - 1),
    //                    compressed.data(), compressed_size);
    //     vec_block::resize(&hb, compressed_size, alloc);
    //     vec_block::write_n(hb, 0, compressed_size, compressed.data(),
    //     alloc);

    //     vec_header::write(h->out[current_location.level],
    //     mapped_block_id, hb,
    //                       alloc);

    //     ++current_location.block_id;
    //     current_location.block_pos = 0;
    //     locations.push_back(current_location);

    //     return ret_sum;
    // }

    // static auto build_out_update_stack(const header& h, uint32_t
    // wminus,
    //                                    uint32_t tminus,
    //                                    const Alloc<PAGE_SIZE>& alloc)
    //     -> std::stack<update_context>
    // {
    //     std::stack<update_context> s;

    //     auto locations = compute_parent_locations(
    //         out_leaf_block_location(wminus, tminus, h), h);

    //     uint32_t level = 0;
    //     uint32_t blocks_per_vertice = 1;
    //     for(auto it = locations.rbegin(); it != locations.rend();
    //         ++it, ++level, blocks_per_vertice *= h->z)
    //     {
    //         auto [initial_block_id, block_position] = *it;
    //         initial_block_id = wminus * blocks_per_vertice +
    //         initial_block_id; uint64_t end_block_id = (wminus + 1) *
    //         blocks_per_vertice; auto hb_it =
    //         vec_header::iterator_at(h->out[level],
    //                                              initial_block_id,
    //                                              *alloc);
    //         auto hb_end =
    //             vec_header::iterator_at(h->out[level], end_block_id,
    //             *alloc);
    //         std::vector<uint32_t> sum(h.n, h.tau);

    //         for(uint64_t current_block_id = initial_block_id; hb_it
    //         != hb_end;
    //             ++hb_it, ++current_block_id, block_position = 0)
    //         {
    //             typename vec_block::header hb = *hb_it;

    //             vec_block::read_n(hb, 0, hb.size, compressed.data(),
    //             *alloc); original_size = h->n * h->z;
    //             decompress_block(compressed.data(), hb.size,
    //                              uncompressed.data(), original_size);

    //             for(uint32_t wplus = 0; wplus < h->n; ++wplus)
    //             {
    //                 std::partial_sum( //
    //                     uncompressed.begin() + wplus * h->z,
    //                     uncompressed.begin() + (wplus + 1) * h->z,
    //                     uncompressed.begin() + wplus * h->z);

    //                 if(current_block_id == initial_block_id)
    //                 {
    //                     initial_sums_before[wplus] -=
    //                         uncompressed[wplus * h->z +
    //                         block_position];
    //                 }
    //             }

    //             uint32_t max_j = 0;
    //             for(uint32_t wplus: wpluses)
    //             {
    //                 uint32_t j = block_position;
    //                 for(; j < h->z
    //                       && out[wplus] < sums_before[wplus]
    //                                           - uncompressed[wplus *
    //                                           h->z + j];
    //                     ++j)
    //                 {
    //                     uncompressed[wplus * h->z + j] =
    //                         sums_after[wplus] - out[wplus];
    //                 }
    //                 max_j = std::max(max_j, j);

    //                 if(current_block_id == initial_block_id)
    //                 {
    //                     initial_sums_after[wplus] -=
    //                         uncompressed[wplus * h->z +
    //                         block_position];
    //                 }
    //             }

    //             if(max_j > block_position)
    //             {
    //                 for(uint32_t wplus = 0; wplus < h->n; ++wplus)
    //                 {
    //                     sums_before[wplus] -=
    //                         uncompressed[(wplus + 1) * h->z - 1];
    //                     sums_after[wplus] -=
    //                         uncompressed[(wplus + 1) * h->z - 1];
    //                     std::adjacent_difference( //
    //                         uncompressed.begin() + wplus * h->z,
    //                         uncompressed.begin() + (wplus + 1) *
    //                         h->z, uncompressed.begin() + wplus *
    //                         h->z);
    //                 }
    //                 compressed_size = h->n * h->z;
    //                 compress_block(uncompressed.data(), h->n * h->z,
    //                                compressed.data(),
    //                                compressed_size);
    //                 vec_block::resize(&hb, compressed_size, alloc);
    //                 vec_block::write_n(hb, 0, compressed_size,
    //                                    compressed.data(), alloc);
    //                 if(!is_same_header(hb, *hb_it))
    //                 {
    //                     updated_headers.push_back({hb,
    //                     current_block_id});
    //                 }
    //             }

    //             if(max_j < h->z)
    //             {
    //                 break;
    //             }
    //         }

    //         if(!updated_headers.empty())
    //         {
    //             uint32_t i = 0;
    //             vec_header::write_lazy(
    //                 h->out[level], updated_headers[i].second,
    //                 [&updated_headers, &i](auto* hb) ->
    //                 std::optional<uint64_t>
    //                 {
    //                     *hb = updated_headers[i].first;
    //                     ++i;
    //                     return (i < updated_headers.size())
    //                                ? std::make_optional(
    //                                    updated_headers[i].second)
    //                                : std::nullopt;
    //                 },
    //                 alloc);
    //         }
    //     }
    // }

    static void compress_block(const uint32_t* in, size_t n_in,
                               uint32_t* out, size_t& n_out)
    {
        using namespace FastPForLib;
        IntegerCODEC& codec = *CODECFactory::getFromName("simple16");
        codec.encodeArray(in, n_in, out, n_out);
    }

    static void decompress_block(const uint32_t* in, size_t n_in,
                                 uint32_t* out, size_t& n_out)
    {
        using namespace FastPForLib;
        IntegerCODEC& codec = *CODECFactory::getFromName("simple16");
        codec.decodeArray(in, n_in, out, n_out);
    }

    [[nodiscard]] static auto
    is_same_header(const typename vec_block::header& h1,
                   const typename vec_block::header& h2) -> bool
    {
        return h1.head == h2.head && h1.size == h2.size
               && h1.capacity == h2.capacity;
    }
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-compressed3.ixx>
