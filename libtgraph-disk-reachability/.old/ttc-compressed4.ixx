#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                               uint32_t delta,
                                               uint32_t z,
                                               Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {
        .n = n,
        .tau = tau,
        .delta = delta,
        .z = z,
    };

    std::vector<uint32_t> uncompressed_zeros(n * (z - 1) + 32, 0);
    std::vector<uint32_t> uncompressed_zeros_lv0(n + 32, 0);
    std::vector<uint32_t> uncompressed_taus_lv0(n + 32, tau);

    size_t compressed_size = n * (z - 1);
    std::vector<uint32_t> compressed_zeros(compressed_size + 1024);
    compress_block(uncompressed_zeros.data(), n * (z - 1),
                   compressed_zeros.data(), compressed_size);
    compressed_zeros.resize(compressed_size);

    compressed_size = n;
    std::vector<uint32_t> compressed_zeros_lv0(compressed_size + 1024);
    compress_block(uncompressed_zeros_lv0.data(), n,
                   compressed_zeros_lv0.data(), compressed_size);
    compressed_zeros_lv0.resize(compressed_size);

    compressed_size = n;
    std::vector<uint32_t> compressed_taus_lv0(compressed_size + 1024);
    compress_block(uncompressed_taus_lv0.data(), n,
                   compressed_taus_lv0.data(), compressed_size);
    compressed_taus_lv0.resize(compressed_size);

    std::vector<typename vec_block::header> headers(n * tau);

    h.out[0] = vec_header::create(n, alloc);
    for(uint32_t i = 0; i < n; ++i)
    {
        headers[i] =
            vec_block::create(compressed_taus_lv0.size(), alloc);
        vec_block::write_n(headers[i], 0, compressed_taus_lv0.size(),
                           compressed_taus_lv0.data(), alloc);
    }
    vec_header::write_n(h.out[0], 0, n, headers.data(), alloc);

    for(uint32_t lv = 1, n_blocks = 1;
        lv < std::ceil(std::log(tau) / std::log(z)) + 1;
        ++lv, n_blocks *= z)
    {
        h.out[lv] = vec_header::create(n_blocks * n, alloc);
        for(uint32_t i = 0; i < n_blocks * n; ++i)
        {
            headers[i] =
                vec_block::create(compressed_zeros.size(), alloc);
            vec_block::write_n(headers[i], 0, compressed_zeros.size(),
                               compressed_zeros.data(), alloc);
        }
        vec_header::write_n(h.out[lv], 0, n_blocks * n, headers.data(),
                            alloc);
    }

    h.in[0] = vec_header::create(n, alloc);
    for(uint32_t i = 0; i < n; ++i)
    {
        headers[i] =
            vec_block::create(compressed_zeros_lv0.size(), alloc);
        vec_block::write_n(headers[i], 0, compressed_zeros_lv0.size(),
                           compressed_zeros_lv0.data(), alloc);
    }
    vec_header::write_n(h.in[0], 0, n, headers.data(), alloc);

    for(uint32_t lv = 1, n_blocks = 1;
        lv < std::ceil(std::log(tau) / std::log(z)) + 1;
        ++lv, n_blocks *= z)
    {
        h.in[lv] = vec_header::create(n_blocks * n, alloc);

        for(uint32_t i = 0; i < n_blocks * n; ++i)
        {
            headers[i] =
                vec_block::create(compressed_zeros.size(), alloc);
            vec_block::write_n(headers[i], 0, compressed_zeros.size(),
                               compressed_zeros.data(), alloc);
        }
        vec_header::write_n(h.in[lv], 0, n_blocks * n, headers.data(),
                            alloc);
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_compressed3<Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, uint32_t z,
    It contacts_begin, It contacts_end, Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed3<Alloc, PAGE_SIZE>::destroy(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    std::vector<typename vec_block::header> headers(h->n * h->tau);

    vec_header::read_n(h->out[0], 0, h->n, headers.data(), *alloc);
    for(auto& hb: headers)
    {
        vec_block::destroy(&hb, alloc);
    }
    vec_header::destroy(&h->out[0], alloc);

    for(uint32_t lv = 1, n_blocks = 1;
        lv < std::ceil(std::log(h->tau) / std::log(h->z)) + 1;
        ++lv, n_blocks *= h->z)
    {
        vec_header::read_n(h->out[lv], 0, n_blocks * h->n,
                           headers.data(), *alloc);
        for(auto& hb: headers)
        {
            vec_block::destroy(&hb, alloc);
        }
        vec_header::destroy(&h->out[lv], alloc);
    }

    vec_header::read_n(h->in[0], 0, h->n, headers.data(), *alloc);
    for(auto& hb: headers)
    {
        vec_block::destroy(&hb, alloc);
    }
    vec_header::destroy(&h->in[0], alloc);

    for(uint32_t lv = 1, n_blocks = 1;
        lv < std::ceil(std::log(h->tau) / std::log(h->z)) + 1;
        ++lv, n_blocks *= h->z)
    {
        vec_header::read_n(h->in[lv], 0, n_blocks * h->n,
                           headers.data(), *alloc);
        for(auto& hb: headers)
        {
            vec_block::destroy(&hb, alloc);
        }
        vec_header::destroy(&h->in[lv], alloc);
    }

    h->n = 0;
    h->tau = 0;
    h->delta = 0;
    h->z = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed3<Alloc, PAGE_SIZE>::clear(header* h,
                                              Alloc<PAGE_SIZE>* alloc)
{
    std::vector<uint32_t> uncompressed_zeros(h->n * (h->z - 1) + 32, 0);
    std::vector<uint32_t> uncompressed_zeros_lv0(h->n + 32, 0);
    std::vector<uint32_t> uncompressed_taus_lv0(h->n + 32, h->tau);

    size_t compressed_size = h->n * (h->z - 1);
    std::vector<uint32_t> compressed_zeros(compressed_size + 1024);
    compress_block(uncompressed_zeros.data(), h->n * (h->z - 1),
                   compressed_zeros.data(), compressed_size);
    compressed_zeros.resize(compressed_size);

    compressed_size = h->n;
    std::vector<uint32_t> compressed_zeros_lv0(compressed_size + 1024);
    compress_block(uncompressed_zeros_lv0.data(), h->n,
                   compressed_zeros_lv0.data(), compressed_size);
    compressed_zeros_lv0.resize(compressed_size);

    compressed_size = h->n;
    std::vector<uint32_t> compressed_taus_lv0(compressed_size + 1024);
    compress_block(uncompressed_taus_lv0.data(), h->n,
                   compressed_taus_lv0.data(), compressed_size);
    compressed_taus_lv0.resize(compressed_size);

    std::vector<typename vec_block::header> headers(h->n * h->tau);

    vec_header::read_n(h->out[0], 0, h->n, headers.data(), *alloc);
    for(auto& hb: headers)
    {
        vec_block::resize(&hb, compressed_taus_lv0.size(), alloc);
        vec_block::write_n(&hb, compressed_taus_lv0.size(),
                           compressed_taus_lv0.data(), alloc);
    }
    vec_header::write_n(&h->out[0], 0, h->n, headers.data(), alloc);

    for(uint32_t lv = 1, n_blocks = 1;
        lv < std::ceil(std::log(h->tau) / std::log(h->z)) + 1;
        ++lv, n_blocks *= h->z)
    {
        vec_header::read_n(h->out[lv], 0, n_blocks * h->n,
                           headers.data(), *alloc);
        for(auto& hb: headers)
        {
            vec_block::resize(&hb, compressed_zeros.size(), alloc);
            vec_block::write_n(&hb, compressed_zeros.size(),
                               compressed_zeros.data(), alloc);
        }
        vec_header::write_n(&h->out[lv], 0, n_blocks * h->n,
                            headers.data(), alloc);
    }

    vec_header::read_n(h->in[0], 0, h->n, headers.data(), *alloc);
    for(auto& hb: headers)
    {
        vec_block::resize(&hb, compressed_zeros_lv0.size(), alloc);
        vec_block::write_n(&hb, compressed_zeros_lv0.size(),
                           compressed_zeros_lv0.data(), alloc);
    }
    vec_header::write_n(&h->in[0], 0, h->n, headers.data(), alloc);

    for(uint32_t lv = 1, n_blocks = 1;
        lv < std::ceil(std::log(h->tau) / std::log(h->z)) + 1;
        ++lv, n_blocks *= h->z)
    {
        vec_header::read_n(h->in[lv], 0, n_blocks * h->n,
                           headers.data(), *alloc);
        for(auto& hb: headers)
        {
            vec_block::resize(&hb, compressed_zeros.size(), alloc);
            vec_block::write_n(&hb, compressed_zeros.size(),
                               compressed_zeros.data(), alloc);
        }
        vec_header::write_n(&h->in[lv], 0, n_blocks * h->n,
                            headers.data(), alloc);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed3<Alloc, PAGE_SIZE>::add_contact(
    header* h, contact c, Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    size_t original_size = h->n * (h->z - 1);
    size_t compressed_size = h->n * (h->z - 1);
    std::vector<uint32_t> uncompressed(original_size + 32);
    std::vector<uint32_t> uncompressed_sums(h->n + 32);
    std::vector<uint32_t> compressed(compressed_size + 1024);

    uint32_t sum = h->tau;
    {
        auto locations = compute_parent_locations(
            out_leaf_block_location(t, *h), *h);

        auto hb = vec_header::read(h->out[0], u, *alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), *alloc);
        original_size = h->n;
        decompress_block(compressed.data(), hb.size,
                         uncompressed_sums.data(), original_size);
        sum = uncompressed_sums[v];

        uint32_t blocks_per_vertice = 1;
        for(auto it = locations.rbegin(); it != locations.rend();
            ++it, blocks_per_vertice *= h->z)
        {
            it->block_id = u * blocks_per_vertice + it->block_id;
            auto hb = vec_header::read(h->out[it->level], it->block_id,
                                       *alloc);
            vec_block::read_n(hb, 0, hb.size, compressed.data(),
                              *alloc);
            original_size = h->n * (h->z - 1);
            decompress_block(compressed.data(), hb.size,
                             uncompressed.data(), original_size);

            sum -= std::accumulate( //
                uncompressed.begin() + v * (h->z - 1),
                uncompressed.begin() + v * (h->z - 1) + it->block_pos,
                0);
        }
    }

    if(sum != t)
    {
        std::vector<uint32_t> out(h->n, h->tau);
        {
            if(t + h->delta < h->tau)
            {
                auto locations = compute_parent_locations(
                    out_leaf_block_location(t + h->delta, *h), *h);

                auto hb = vec_header::read(h->out[0], v, *alloc);
                vec_block::read_n(hb, 0, hb.size, compressed.data(),
                                  *alloc);
                original_size = h->n;
                decompress_block(compressed.data(), hb.size,
                                 uncompressed_sums.data(),
                                 original_size);

                for(uint32_t wplus = 0; wplus < h->n; ++wplus)
                {
                    out[wplus] = uncompressed_sums[wplus];
                }

                uint32_t blocks_per_vertice = 1;
                for(auto it = locations.rbegin();
                    it != locations.rend();
                    ++it, blocks_per_vertice *= h->z)
                {
                    it->block_id =
                        v * blocks_per_vertice + it->block_id;
                    auto hb = vec_header::read(h->out[it->level],
                                               it->block_id, *alloc);
                    vec_block::read_n(hb, 0, hb.size, compressed.data(),
                                      *alloc);
                    original_size = h->n * (h->z - 1);
                    decompress_block(compressed.data(), hb.size,
                                     uncompressed.data(),
                                     original_size);

                    for(uint32_t wplus = 0; wplus < h->n; ++wplus)
                    {
                        out[wplus] -= std::accumulate(
                            uncompressed.begin() + wplus * (h->z - 1),
                            uncompressed.begin() + wplus * (h->z - 1)
                                + it->block_pos + 1,
                            0);
                    }
                }
            }
        }
        out[v] = t;

        std::vector<uint32_t> in(h->n, 0);
        {
            if(t >= h->delta)
            {
                auto locations = compute_parent_locations(
                    in_leaf_block_location(t, *h), *h);

                auto hb = vec_header::read(h->in[0], u, *alloc);
                vec_block::read_n(hb, 0, hb.size, compressed.data(),
                                  *alloc);
                original_size = h->n;
                decompress_block(compressed.data(), hb.size,
                                 uncompressed_sums.data(),
                                 original_size);

                for(uint32_t wminus = 0; wminus < h->n; ++wminus)
                {
                    in[wminus] = uncompressed_sums[wminus];
                }

                uint32_t level = 0;
                uint32_t blocks_per_vertice = 1;
                for(auto it = locations.rbegin();
                    it != locations.rend();
                    ++it, ++level, blocks_per_vertice *= h->z)
                {
                    it->block_id =
                        u * blocks_per_vertice + it->block_id;
                    auto hb = vec_header::read(h->in[it->level],
                                               it->block_id, *alloc);
                    vec_block::read_n(hb, 0, hb.size, compressed.data(),
                                      *alloc);
                    original_size = h->n * (h->z - 1);
                    decompress_block(compressed.data(), hb.size,
                                     uncompressed.data(),
                                     original_size);

                    for(uint32_t wminus = 0; wminus < h->n; ++wminus)
                    {
                        in[wminus] += std::accumulate(
                            uncompressed.begin() + wminus * (h->z - 1),
                            uncompressed.begin() + wminus * (h->z - 1)
                                + it->block_pos,
                            0);
                    }
                }
            }
        }
        in[u] = t + 1;

        std::vector<uint32_t> wpluses;
        for(uint32_t wplus = 0; wplus < h->n; ++wplus)
        {
            if(wplus != u && out[wplus] < h->tau)
            {
                wpluses.push_back(wplus);
            }
        }

        std::vector<uint32_t> wminuses;
        for(uint32_t wminus = 0; wminus < h->n; ++wminus)
        {
            if(wminus != v && in[wminus] > 0)
            {
                wminuses.push_back(wminus);
            }
        }

        for(uint32_t wminus: wminuses)
        {
            update_out_adjacencies(h, wminus, in[wminus] - 1, wpluses,
                                   out, alloc);
        }

        for(uint32_t wplus: wpluses)
        {
            auto locations = compute_parent_locations(
                in_leaf_block_location(out[wplus] + h->delta, *h), *h);

            auto hb = vec_header::read(h->in[0], wplus, *alloc);
            vec_block::read_n(hb, 0, hb.size, compressed.data(),
                              *alloc);
            original_size = h->n;
            decompress_block(compressed.data(), hb.size,
                             uncompressed_sums.data(), original_size);

            uncompressed_sums = update_in_adjacencies(
                h, out[wplus], in, uncompressed_sums, locations, alloc);

            compressed_size = h->n;
            compress_block(uncompressed_sums.data(), h->n,
                           compressed.data(), compressed_size);
            vec_block::resize(&hb, compressed_size, alloc);
            vec_block::write_n(hb, 0, compressed_size,
                               compressed.data(), alloc);

            vec_header::write(h->in[0], wplus, hb, alloc);
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval interv,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    interv.right = std::min(interv.right, h.tau + h.delta);

    if(interv.left >= h.tau || interv.right - interv.left < h.delta)
    {
        return false;
    }

    uint32_t sum = h.tau;
    {
        auto locations = compute_parent_locations(
            out_leaf_block_location(interv.left, h), h);

        size_t original_size = h.n * (h.z - 1);
        size_t compressed_size = h.n * (h.z - 1);
        std::vector<uint32_t> uncompressed(original_size + 32);
        std::vector<uint32_t> uncompressed_sums(h.n + 32);
        std::vector<uint32_t> compressed(compressed_size + 1024);

        auto hb = vec_header::read(h.out[0], u, alloc);
        vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
        original_size = h.n;
        decompress_block(compressed.data(), hb.size,
                         uncompressed_sums.data(), original_size);
        sum = uncompressed_sums[v];

        uint32_t blocks_per_vertice = 1;
        for(auto it = locations.rbegin(); it != locations.rend();
            ++it, blocks_per_vertice *= h.z)
        {
            it->block_id = u * blocks_per_vertice + it->block_id;
            auto hb =
                vec_header::read(h.out[it->level], it->block_id, alloc);
            vec_block::read_n(hb, 0, hb.size, compressed.data(), alloc);
            original_size = h.n * (h.z - 1);
            decompress_block(compressed.data(), hb.size,
                             uncompressed.data(), original_size);

            sum -= std::accumulate( //
                uncompressed.begin() + v * (h.z - 1),
                uncompressed.begin() + v * (h.z - 1) + it->block_pos,
                0);
        }
    }
    return std::min(sum + h.delta, h.tau + h.delta - 1) < interv.right;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    return {};
    // if(u == v)
    // {
    //     return {};
    // }

    // i.right = std::min(i.right, h.tau + h.delta);

    // if(i.left >= h.tau || i.right - i.left < h.delta)
    // {
    //     return {};
    // }

    // auto [tplus, _] = vec::read(h.out_data, out_access(u, i.left, v,
    // h), alloc); tplus += h.delta; if(std::min(tplus, h.tau + h.delta
    // - 1) >= i.right)
    // {
    //     return {};
    // }

    // auto [tminus, succ] = vec::read(h.in_data, in_access(v, tplus, u,
    // h), alloc);
    // --tminus;
    // journey j({u, succ, tminus}, h.delta);

    // while(succ != v)
    // {
    //     auto [next_tminus, next_succ] =
    //         vec::read(h.in_data, in_access(v, tplus, succ, h),
    //         alloc);
    //     next_tminus--;

    //     j.push_back({succ, next_succ, next_tminus});

    //     succ = next_succ;
    //     tminus = next_tminus;
    // }

    // return j;
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::sequential_searches()
    -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_compressed3<Alloc, PAGE_SIZE>::opt_space() -> uint64_t
{
    return total_opt_space;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_compressed3<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
    total_opt_space = 0;
}

#endif

} // namespace tgraph_disk_reachability
