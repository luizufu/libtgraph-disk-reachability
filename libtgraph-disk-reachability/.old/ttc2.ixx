#include <algorithm>
#include <type_traits>

namespace tgraph_disk_reachability
{
template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::rttree_header(header* h, uint32_t u, uint32_t v,
                                    disk::allocator<PAGE_SIZE>* alloc) ->
    typename tree::header
{
    if(auto set_header = graph::label(h->out_graph_header, u, v, *alloc))
    {
        return *set_header;
    }

    auto set_header = tree::create(alloc);
    graph::insert_edge(&h->out_graph_header, u, v, set_header, alloc);
    graph::insert_edge(&h->in_graph_header, v, u, set_header, alloc);
    return set_header;
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::create(uint32_t n, uint32_t tau, uint32_t delta,
                             disk::allocator<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    h.out_graph_header = graph::create(n, alloc);
    h.in_graph_header = graph::create(n, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    return h;
}

template<size_t PAGE_SIZE>
template<typename It>
auto ttc2<PAGE_SIZE>::create(uint32_t n, uint32_t tau, uint32_t delta,
                             It contacts_begin, It contacts_end,
                             disk::allocator<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        auto uvheader = rttree_header(&h, u, v, alloc);
        if(tree::insert(&uvheader, {t, t + h.delta}, v, alloc))
        {
            for(auto& [wminus, wuheader]:
                graph::out_neighbors(h.in_graph_header, u, *alloc))
            {
                if(wminus == v)
                {
                    continue;
                }

                if(auto res = tree::find_prev(wuheader, t, *alloc))
                {
                    auto wvheader = rttree_header(&h, wminus, v, alloc);
                    if(tree::insert(&wvheader, {res->first.left, t + h.delta},
                                    res->second, alloc))
                    {
                        graph::insert_or_update_edge(
                            &h.out_graph_header, wminus, v, wvheader, alloc);
                        graph::insert_or_update_edge(&h.in_graph_header, v,
                                                     wminus, wvheader, alloc);
                    }
                }
            }

            graph::insert_or_update_edge(&h.out_graph_header, u, v, uvheader,
                                         alloc);
            graph::insert_or_update_edge(&h.in_graph_header, v, u, uvheader,
                                         alloc);
        }
    }

    return h;
}

template<size_t PAGE_SIZE>
void ttc2<PAGE_SIZE>::destroy(header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    for(auto& [u, v, label]: graph::edges(h->out_graph_header, *alloc))
    {
        tree::destroy(&label, alloc);
    }

    graph::destroy(&h->out_graph_header, alloc);
    graph::destroy(&h->in_graph_header, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<size_t PAGE_SIZE>
void ttc2<PAGE_SIZE>::clear(header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    for(auto& [u, v, label]: graph::edges(h->out_graph_header, *alloc))
    {
        tree::destroy(&label, alloc);
    }

    graph::clear(&h->out_graph_header, alloc);
    graph::clear(&h->in_graph_header, alloc);
}

template<size_t PAGE_SIZE>
void ttc2<PAGE_SIZE>::add_contact(header* h, contact c,
                                  disk::allocator<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    auto uvheader = rttree_header(h, u, v, alloc);
    if(tree::insert(&uvheader, {t, t + h->delta}, v, alloc))
    {
        std::vector<std::pair<uint32_t, uint32_t>> d;

        for(auto& [wplus, vwheader]:
            graph::out_neighbors(h->out_graph_header, v, *alloc))
        {
            if(wplus == u)
            {
                continue;
            }

            if(auto res = tree::find_next(vwheader, t + h->delta, *alloc))
            {
                uint32_t tplus = res->first.right;
                auto uwheader = rttree_header(h, u, wplus, alloc);
                if(tree::insert(&uwheader, {t, tplus}, v, alloc))
                {
                    d.emplace_back(wplus, tplus);
                    graph::insert_or_update_edge(&h->out_graph_header, u, wplus,
                                                 uwheader, alloc);
                    graph::insert_or_update_edge(&h->in_graph_header, wplus, u,
                                                 uwheader, alloc);
                }
            }
        }

        for(auto& [wminus, wuheader]:
            graph::out_neighbors(h->in_graph_header, u, *alloc))
        {
            if(wminus == v)
            {
                continue;
            }

            if(auto res = tree::find_prev(wuheader, t, *alloc))
            {
                uint32_t tminus = res->first.left;
                uint32_t succ = res->second;
                auto wvheader = rttree_header(h, wminus, v, alloc);
                if(tree::insert(&wvheader, {tminus, t + h->delta}, succ, alloc))
                {
                    for(const auto& [wplus, tplus]: d)
                    {
                        if(wplus == wminus)
                        {
                            continue;
                        }

                        auto wwheader = rttree_header(h, wminus, wplus, alloc);
                        if(tree::insert(&wwheader, {tminus, tplus}, succ,
                                        alloc))
                        {
                            graph::insert_or_update_edge(&h->out_graph_header,
                                                         wminus, wplus,
                                                         wwheader, alloc);
                            graph::insert_or_update_edge(&h->in_graph_header,
                                                         wplus, wminus,
                                                         wwheader, alloc);
                        }
                    }

                    graph::insert_or_update_edge(&h->out_graph_header, wminus,
                                                 v, wvheader, alloc);
                    graph::insert_or_update_edge(&h->in_graph_header, v, wminus,
                                                 wvheader, alloc);
                }
            }
        }

        graph::insert_or_update_edge(&h->out_graph_header, u, v, uvheader,
                                     alloc);
        graph::insert_or_update_edge(&h->in_graph_header, v, u, uvheader,
                                     alloc);
    }
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::can_reach(const header& h, uint32_t u, uint32_t v,
                                interval i,
                                const disk::allocator<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    if(auto uvheader = graph::label(h.out_graph_header, u, v, alloc))
    {
        if(i.left == 0 && i.right == h.tau)
        {
            return true;
        }

        if(auto res = tree::find_next(*uvheader, i.left, alloc))
        {
            return std::min(res->first.right, h.tau + h.delta - 1) < i.right;
        }
    }

    return false;
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const disk::allocator<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    auto uwheader = graph::label(h.out_graph_header, u, v, alloc);
    if(!uwheader.has_value())
    {
        return {};
    }

    auto res = tree::find_next(*uwheader, i.left, alloc);
    if(!res.has_value() || res->first.right >= i.right)
    {
        return {};
    }

    uint32_t succ = res->second;
    uint32_t tminus = res->first.left;

    journey j({u, succ, tminus}, h.delta);

    while(succ != v)
    {
        auto wvheader = graph::label(h.out_graph_header, succ, v, alloc);
        auto res2 = tree::find_next(*wvheader, tminus + h.delta, alloc);
        uint32_t next_succ = res2->second;
        uint32_t next_tminus = res2->first.left;

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::binary_searches() -> uint64_t
{
    return tree::total_binary_searches + graph::total_binary_searches;
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return tree::total_sequential_searches + graph::total_sequential_searches;
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return tree::total_inserts;
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return 0;
}

template<size_t PAGE_SIZE>
auto ttc2<PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return tree::total_erases;
}

template<size_t PAGE_SIZE>
void ttc2<PAGE_SIZE>::reset_benchmark()
{
    tree::total_sequential_searches = 0;
    tree::total_binary_searches = 0;
    tree::total_inserts = 0;
    tree::total_erases = 0;

    graph::total_sequential_searches = 0;
    graph::total_binary_searches = 0;
}

#endif

} // namespace tgraph_disk_reachability
