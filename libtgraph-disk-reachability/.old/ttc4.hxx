#pragma once

#include <libdisk/vector.hxx>
#include <libgraph-disk-structures/adj-list.hxx>
#include <libtgraph-disk-reachability/detail/rttree.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <optional>

namespace tgraph_disk_reachability
{
template<template<typename, size_t> class Graph, size_t PAGE_SIZE = 4096>
class ttc4
{
    using adj = graph_disk_structures::adj_list<uint32_t, PAGE_SIZE>;
    using parents = disk::vector<uint32_t, PAGE_SIZE>;
    using tree = detail::rttree<uint32_t, PAGE_SIZE>;
    using pointers = Graph<typename tree::header, PAGE_SIZE>;

public:
    struct header
    {
        typename adj::header adj_header = {};
        typename parents::header parents_header = {};
        typename pointers::header pointers_header = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       disk::allocator<PAGE_SIZE>* alloc =
                           disk::default_allocator<PAGE_SIZE>()) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       disk::allocator<PAGE_SIZE>* alloc =
                           disk::default_allocator<PAGE_SIZE>()) -> header;
    static void destroy(header* h, disk::allocator<PAGE_SIZE>* alloc =
                                       disk::default_allocator<PAGE_SIZE>());
    static void clear(header* h, disk::allocator<PAGE_SIZE>* alloc =
                                     disk::default_allocator<PAGE_SIZE>());

    static void add_contact(header* h, contact c,
                            disk::allocator<PAGE_SIZE>* alloc =
                                disk::default_allocator<PAGE_SIZE>());

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const disk::allocator<PAGE_SIZE>& alloc =
                              *disk::default_allocator<PAGE_SIZE>()) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i,
                                    const disk::allocator<PAGE_SIZE>& alloc =
                                        *disk::default_allocator<PAGE_SIZE>())
        -> std::optional<journey>;

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    static void meld(header* h, uint32_t wminus, uint32_t tminus,
                     uint32_t wplus, uint32_t v, uint32_t tplus,
                     uint32_t parent, disk::allocator<PAGE_SIZE>* alloc);
    static auto insert(header* h, uint32_t u, uint32_t v, interval interv,
                       uint32_t parent, disk::allocator<PAGE_SIZE>* alloc)
        -> uint32_t;
    static auto find_or_create(header* h, uint32_t v, uint32_t t,
                               disk::allocator<PAGE_SIZE>* alloc) -> uint32_t;

    static auto rttree_header(header* h, uint32_t u, uint32_t v,
                              disk::allocator<PAGE_SIZE>* alloc) ->
        typename tree::header;
    static auto idx(const header& h, uint32_t u, uint32_t v, uint32_t t)
        -> uint32_t;
    static auto vertex(const header& h, uint32_t index) -> uint32_t;
    static auto timestamp(const header& h, uint32_t index) -> uint32_t;
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc4.ixx>
