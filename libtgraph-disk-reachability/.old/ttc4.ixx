#include <libdisk/allocator.hxx>

#include <stack>

namespace tgraph_disk_reachability
{
const uint32_t null_index = std::numeric_limits<uint32_t>::max();

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::create(uint32_t n, uint32_t tau, uint32_t delta,
                                    disk::allocator<PAGE_SIZE>* alloc) -> header
{
    header h = {};

    h.adj_header = adj::create(n * n * tau, alloc);
    h.parents_header = parents::create(n * n * tau, alloc);
    parents::fill(h.parents_header, null_index, alloc);

    if constexpr(!std::is_same_v<Graph<void, 0>,
                                 graph_disk_structures::edge_grid<void, 0>>)
    {
        h.pointers_header = pointers::create(n, alloc);
    }
    else
    {
        h.pointers_header = pointers::create(n, 32U, alloc);
    }

    h.n = n;
    h.tau = tau;
    h.delta = delta;

    for(uint32_t v = 0; v < n; ++v)
    {
        auto tree_header = rttree_header(&h, v, v, alloc);
        tree::insert_(&tree_header, {0, 0}, idx(h, v, v, 0), alloc);
        pointers::insert_or_update_edge(&h.pointers_header, v, v, tree_header,
                                        alloc);
    }

    return h;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
template<typename It>
auto ttc4<Graph, PAGE_SIZE>::create(uint32_t n, uint32_t tau, uint32_t delta,
                                    It contacts_begin, It contacts_end,
                                    disk::allocator<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        contact c = *it;
        if(c.u == c.v || c.t >= h.tau)
        {
            continue;
        }

        auto uu = find_or_create(&h, c.u, c.t, alloc);
        if(auto uv = insert(&h, c.u, c.v, {c.t, c.t + h.delta}, uu, alloc);
           uv != null_index)
        {
            for(auto& [wminus, _]:
                pointers::in_neighbors(h.pointers_header, c.u, *alloc))
            {
                if(wminus == c.u || wminus == c.v)
                {
                    continue;
                }

                if(auto wu = tree::find_prev(
                       rttree_header(&h, wminus, c.u, alloc), c.t, *alloc))
                {
                    insert(&h, wminus, c.v, {wu->first.left, c.t + h.delta},
                           wu->second, alloc);
                }
            }
        }
    }

    return h;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
void ttc4<Graph, PAGE_SIZE>::destroy(header* h,
                                     disk::allocator<PAGE_SIZE>* alloc)
{
    adj::destroy(&h->adj_header, alloc);
    parents::destroy(&h->parents_header, alloc);
    for(auto& [u, v, tree_header]: pointers::edges(h->pointers_header, *alloc))
    {
        tree::destroy(&tree_header, alloc);
    }
    pointers::destroy(&h->pointers_header, alloc);

    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
void ttc4<Graph, PAGE_SIZE>::clear(header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    adj::clear(&h->adj_header, alloc);
    parents::fill(h->parents_header, null_index, alloc);
    for(auto& [u, v, tree_header]: pointers::edges(h->pointers_header, *alloc))
    {
        tree::destroy(&tree_header, alloc);
    }
    pointers::clear(&h->pointers_header, alloc);

    for(uint32_t v = 0; v < h->n; ++v)
    {
        auto tree_header = rttree_header(&h, v, v, alloc);
        tree::insert_(&tree_header, {0, 0}, idx(*h, v, v, 0), alloc);
        pointers::insert_or_update_edge(&h->pointers_header, v, v, tree_header,
                                        alloc);
    }
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
void ttc4<Graph, PAGE_SIZE>::add_contact(header* h, contact c,
                                         disk::allocator<PAGE_SIZE>* alloc)
{
    if(c.u == c.v || c.t >= h->tau)
    {
        return;
    }

    auto uu = find_or_create(h, c.u, c.t, alloc);
    if(auto uv = insert(h, c.u, c.v, {c.t, c.t + h->delta}, uu, alloc);
       uv != uu)
    {
        if(auto vv = tree::find_next(rttree_header(h, c.v, c.v, alloc),
                                     c.t + h->delta, *alloc))
        {
            meld(h, c.u, c.t, c.v, c.v, timestamp(*h, vv->second), uv, alloc);
        }

        for(auto& [wminus, tree_header]:
            pointers::in_neighbors(h->pointers_header, c.u, *alloc))
        {
            if(wminus == c.u || wminus == c.v)
            {
                continue;
            }

            if(auto wu = tree::find_prev(tree_header, c.t, *alloc))
            {
                meld(h, wminus, wu->first.left, c.u, c.v, c.t + h->delta,
                     wu->second, alloc);
            }
        }
    }
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::can_reach(const header& h, uint32_t u, uint32_t v,
                                       interval i,
                                       const disk::allocator<PAGE_SIZE>& alloc)
    -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    if(auto tree_header = pointers::label(h.pointers_header, u, v, alloc))
    {
        if(auto n = tree::find_prev(*tree_header, i.right - 1, alloc))
        {
            return n->first.left >= i.left;
        }
    }

    return false;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const disk::allocator<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    if(auto tree_header = pointers::label(h.pointers_header, u, v, alloc))
    {
        if(auto n = tree::find_prev(*tree_header, i.right - 1, alloc))
        {
            if(n->first.left >= i.left)
            {
                std::stack<contact> contacts;
                uint32_t cur = n->second - h.delta;

                while(vertex(h, cur) != u)
                {
                    uint32_t parent = parents::read(
                        h.parents_header,
                        cur - (vertex(h, cur) == v ? 0 : h.delta), alloc);
                    contacts.push({vertex(h, parent), vertex(h, cur),
                                   timestamp(h, parent)});
                    cur = parent;
                }

                journey j(h.delta);
                while(!contacts.empty())
                {
                    j.push_back(contacts.top());
                    contacts.pop();
                }

                return j;
            }
        }
    }

    return {};
}

#ifdef BENCHMARK_ON

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return adj::total_binary_searches + tree::total_binary_searches
           + pointers::total_binary_searches;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return adj::total_sequential_searches + tree::total_sequential_searches
           + pointers::total_sequential_searches;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return tree::total_inserts;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return 0;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return tree::total_erases;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
void ttc4<Graph, PAGE_SIZE>::reset_benchmark()
{
    adj::total_sequential_searches = 0;
    adj::total_binary_searches = 0;

    tree::total_sequential_searches = 0;
    tree::total_binary_searches = 0;
    tree::total_inserts = 0;
    tree::total_erases = 0;

    pointers::total_sequential_searches = 0;
    pointers::total_binary_searches = 0;
}

#endif

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::find_or_create(header* h, uint32_t v, uint32_t t,
                                            disk::allocator<PAGE_SIZE>* alloc)
    -> uint32_t
{
    auto tree_header = rttree_header(h, v, v, alloc);
    auto vv = tree::find_prev(tree_header, t, *alloc);

    if(timestamp(*h, vv->second) == t)
    {
        return vv->second;
    }

    uint32_t ci_before = null_index;
    uint32_t ci_after = idx(*h, v, v, t);

    if(auto tplus = adj::label(h->adj_header, vv->second, v, *alloc))
    {
        ci_before = idx(*h, v, v, *tplus);
    }

    adj::insert_or_update_edge(&h->adj_header, vv->second, v, t, alloc);
    parents::write(h->parents_header, ci_after, vv->second, alloc);
    tree::insert_(&tree_header, {t, t}, ci_after, alloc);
    pointers::insert_or_update_edge(&h->pointers_header, v, v, tree_header,
                                    alloc);

    if(ci_before != null_index)
    {
        adj::insert_or_update_edge(&h->adj_header, ci_after, v,
                                   timestamp(*h, ci_before), alloc);
        parents::write(h->parents_header, ci_before, ci_after, alloc);
    }

    return ci_after;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::insert(header* h, uint32_t u, uint32_t v,
                                    interval interv, uint32_t parent,
                                    disk::allocator<PAGE_SIZE>* alloc)
    -> uint32_t
{
    auto tree_header = rttree_header(h, u, v, alloc);
    if(auto range = tree::none_contained_by_(tree_header, interv, *alloc))
    {
        std::vector<std::pair<interval, uint32_t>> collecteds;
        for(auto it = range->first; it != range->second; ++it)
        {
            collecteds.push_back(*it);
        }

        uint32_t ci = idx(*h, u, v, interv.right);
        for(const auto& [key, collected]: collecteds)
        {
            tree::remove_(&tree_header, key, alloc);
            adj::delete_edge(
                &h->adj_header,
                parents::read(h->parents_header, collected - h->delta, *alloc),
                v, alloc);
            parents::write(h->parents_header, collected - h->delta, null_index,
                           alloc);
            if(key.right < h->tau)
            {
                std::vector<std::pair<uint32_t, uint32_t>> children;
                for(auto& child:
                    adj::out_neighbors(h->adj_header, collected, *alloc))
                {
                    children.emplace_back(child);
                }

                for(auto& [w, t]: children)
                {
                    adj::delete_edge(&h->adj_header, collected, w, alloc);
                    adj::insert_or_update_edge(&h->adj_header, ci, w, t, alloc);
                    parents::write(h->parents_header,
                                   idx(*h, u, w, t) - h->delta, ci, alloc);
                }
            }
        }

        adj::insert_or_update_edge(&h->adj_header, parent, v, interv.right,
                                   alloc);
        parents::write(h->parents_header, ci - h->delta, parent, alloc);
        tree::insert_(&tree_header, interv, ci, alloc);
        pointers::insert_or_update_edge(&h->pointers_header, u, v, tree_header,
                                        alloc);

        return ci;
    }

    return parent;
}

/* template<template<typename> class Graph> */
/* void ttc7<Graph>::meld(uint32_t wminus, uint32_t tminus, uint32_t wplus, */
/*                        uint32_t v, uint32_t tplus, uint32_t parent) */
/* { */
/* struct ctx */
/* { */
/*     uint32_t parent = std::numeric_limits<uint32_t>::max(); */
/*     uint32_t v = 0; */
/*     uint32_t tplus = 0; */
/* }; */
/*     std::stack<ctx> q; */
/*     std::vector<uint8_t> visited(_n, 0U); */
/*     q.push({.parent = parent, .v = v, .tplus = tplus}); */
/*  */
/*     while(!q.empty()) */
/*     { */
/*         auto p = q.top(); */
/*         q.pop(); */
/*  */
/*         if(p.v != wplus && !visited[p.v]) */
/*         { */
/*             p.parent = insert(wminus, p.v, {tminus, p.tplus}, p.parent); */
/*             visited[p.v] = true; */
/*         } */
/*  */
/*         if(p.tplus < _tau) */
/*         { */
/*             for(auto& [w, t]: _adj.out_neighbors(idx(wplus, p.v, p.tplus)))
 */
/*             { */
/*                 q.push({.parent = p.parent, .v = w, .tplus = t}); */
/*             } */
/*         } */
/*     } */
/* } */

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
void ttc4<Graph, PAGE_SIZE>::meld(header* h, uint32_t wminus, uint32_t tminus,
                                  uint32_t wplus, uint32_t v, uint32_t tplus,
                                  uint32_t parent,
                                  disk::allocator<PAGE_SIZE>* alloc)
{
    auto next_parent =
        v == wplus ? parent
                   : insert(h, wminus, v, {tminus, tplus}, parent, alloc);

    if((next_parent != parent || v == wplus) && tplus < h->tau)
    {
        for(auto& [w, t]:
            adj::out_neighbors(h->adj_header, idx(*h, wplus, v, tplus), *alloc))
        {
            meld(h, wminus, tminus, wplus, w, t, parent, alloc);
        }
    }
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::rttree_header(header* h, uint32_t u, uint32_t v,
                                           disk::allocator<PAGE_SIZE>* alloc) ->
    typename tree::header
{
    if(auto tree_header = pointers::label(h->pointers_header, u, v, *alloc))
    {
        return *tree_header;
    }

    auto tree_header = tree::create(alloc);
    pointers::insert_edge(&h->pointers_header, u, v, tree_header, alloc);
    return tree_header;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::idx(const header& h, uint32_t u, uint32_t v,
                                 uint32_t t) -> uint32_t
{
    return (u * h.n * h.tau) + (v * h.tau + t);
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::vertex(const header& h, uint32_t index) -> uint32_t
{
    return (index / h.tau) % h.n;
}

template<template<typename, size_t> class Graph, size_t PAGE_SIZE>
auto ttc4<Graph, PAGE_SIZE>::timestamp(const header& h, uint32_t index)
    -> uint32_t
{
    return index % h.tau;
}

} // namespace tgraph_disk_reachability
