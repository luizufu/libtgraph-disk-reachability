#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    uint64_t size = static_cast<uint64_t>(n)
                    * static_cast<uint64_t>(tau)
                    * static_cast<uint64_t>(n);
    h.in = vec::create(size, alloc);
    vec::fill(h.in, {0, n}, alloc);
    h.out = vec::create(size, alloc);
    vec::fill(h.out, {tau, n}, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

#ifdef BENCHMARK_ON
    total_opt_space = n * n * tau * sizeof(uint32_t) * 2;
#endif

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, It contacts_begin,
    It contacts_end, Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        if(vec::read(h.out, out_access(u, t, v, h), *alloc).t != t)
        {
            std::vector<entry> out(h.n, {h.tau, h.n});
            out[v] = {t, v};

            std::vector<uint32_t> wpluses;
            for(uint32_t wplus = 0; wplus < h.n; ++wplus)
            {
                if(wplus != u && out[wplus].t < h.tau)
                {
                    wpluses.push_back(wplus);
                }
            }

            std::vector<entry> in(h.n, {0, h.n});
            if(t >= h.delta)
            {
                vec::read_n(h.in, in_access(u, t, 0, h), h.n, in.data(),
                            *alloc);
            }
            in[u] = {t + 1, v};

            std::vector<uint32_t> wminuses;
            for(uint32_t wminus = 0; wminus < h.n; ++wminus)
            {
                if(wminus != v && in[wminus].t > 0)
                {
                    wminuses.push_back(wminus);
                }
            }

            std::vector<entry> tmp(h.n, {h.tau, h.n});
            for(uint32_t wminus: wminuses)
            {
                bool replaced = true;
                uint32_t tminus = in[wminus].t;
                while(replaced && tminus > 0)
                {
                    vec::read_n(h.out,
                                out_access(wminus, tminus - 1, 0, h),
                                h.n, tmp.data(), *alloc);

                    replaced = false;
                    for(uint32_t wplus: wpluses)
                    {
                        if(out[wplus].t < tmp[wplus].t)
                        {
                            tmp[wplus] = out[wplus];
                            replaced = true;
                        }
                    }

                    if(replaced)
                    {
                        vec::write_n(
                            h.out, out_access(wminus, tminus - 1, 0, h),
                            h.n, tmp.data(), alloc);
                    }

                    --tminus;
                }
            }

            std::fill(tmp.begin(), tmp.end(), entry {0, h.n});
            for(uint32_t wplus: wpluses)
            {
                bool replaced = true;
                uint32_t tplus = out[wplus].t + h.delta;
                while(replaced && tplus < h.tau + h.delta)
                {
                    vec::read_n(h.in, in_access(wplus, tplus, 0, h),
                                h.n, tmp.data(), *alloc);

                    replaced = false;
                    for(uint32_t wminus: wminuses)
                    {
                        if(in[wminus].t > tmp[wminus].t)
                        {
                            tmp[wminus] = in[wminus];
                            replaced = true;
                        }
                    }

                    if(replaced)
                    {
                        vec::write_n(h.in,
                                     in_access(wplus, tplus, 0, h), h.n,
                                     tmp.data(), alloc);
                    }

                    ++tplus;
                }
            }
        }
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_filtered<Alloc, PAGE_SIZE>::destroy(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->in, alloc);
    vec::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_filtered<Alloc, PAGE_SIZE>::clear(
    header* h, Alloc<PAGE_SIZE>* alloc)
{
    vec::fill(*h->in, {0, h->n}, alloc);
    vec::fill(*h->out, {h->tau, h->n}, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_filtered<Alloc, PAGE_SIZE>::add_contact(
    header* h, contact c, Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    if(vec::read(h->out, out_access(u, t, v, *h), *alloc).t != t)
    {
        std::vector<entry> out(h->n, {h->tau, h->n});
        if(t + h->delta < h->tau)
        {
            vec::read_n(h->out, out_access(v, t + h->delta, 0, *h),
                        h->n, out.data(), *alloc);
        }
        out[v] = {t, v};

        std::vector<uint32_t> wpluses;
        for(uint32_t wplus = 0; wplus < h->n; ++wplus)
        {
            if(wplus != u && out[wplus].t < h->tau)
            {
                wpluses.push_back(wplus);
            }
        }

        std::vector<entry> in(h->n, {0, h->n});
        if(t >= h->delta)
        {
            vec::read_n(h->in, in_access(u, t, 0, *h), h->n, in.data(),
                        *alloc);
        }
        in[u] = {t + 1, v};

        std::vector<uint32_t> wminuses;
        for(uint32_t wminus = 0; wminus < h->n; ++wminus)
        {
            if(wminus != v && in[wminus].t > 0)
            {
                wminuses.push_back(wminus);
            }
        }

        std::vector<entry> tmp(h->n, {h->tau, h->n});
        for(uint32_t wminus: wminuses)
        {
            auto wpluses2 = wpluses;
            uint32_t tminus = in[wminus].t;
            while(!wpluses2.empty() && tminus > 0)
            {
                uint64_t initial_disk_pos =
                    out_access(wminus, tminus - 1, 0, *h);

                vec::read_is(h->out, wpluses2.begin(), wpluses2.end(),
                             tmp.data(), true, *alloc,
                             initial_disk_pos);

                for(auto it = wpluses2.begin(); it != wpluses2.end();)
                {
                    uint32_t wplus = *it;
                    if(out[wplus].t < tmp[wplus].t)
                    {
                        tmp[wplus] = out[wplus];
                        ++it;
                    }
                    else
                    {
                        it = wpluses2.erase(it);
                    }
                }

                if(!wpluses2.empty())
                {
                    vec::write_is(h->out, wpluses2.begin(),
                                  wpluses2.end(), tmp.data(), true,
                                  alloc, initial_disk_pos);
                }

                --tminus;
            }
        }

        std::fill(tmp.begin(), tmp.end(), entry {0, h->n});
        for(uint32_t wplus: wpluses)
        {
            auto wminuses2 = wminuses;
            uint32_t tplus = out[wplus].t + h->delta;
            while(!wminuses2.empty() && tplus < h->tau + h->delta)
            {
                uint64_t initial_disk_pos =
                    in_access(wplus, tplus, 0, *h);
                vec::read_is(h->in, wminuses2.begin(), wminuses2.end(),
                             tmp.data(), true, *alloc,
                             initial_disk_pos);

                for(auto it = wminuses2.begin(); it != wminuses2.end();)
                {
                    uint32_t wminus = *it;
                    if(in[wminus].t > tmp[wminus].t)
                    {
                        tmp[wminus] = in[wminus];
                        ++it;
                    }
                    else
                    {
                        it = wminuses2.erase(it);
                    }
                }

                if(!wminuses2.empty())
                {
                    vec::write_is(h->in, wminuses2.begin(),
                                  wminuses2.end(), tmp.data(), true,
                                  alloc, initial_disk_pos);
                }

                ++tplus;
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return std::min(
               vec::read(h.out, out_access(u, i.left, v, h), alloc).t
                   + h.delta,
               h.tau + h.delta - 1)
           < i.right;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    auto [tplus, _] =
        vec::read(h.out, out_access(u, i.left, v, h), alloc);
    tplus += h.delta;
    if(std::min(tplus, h.tau + h.delta - 1) >= i.right)
    {
        return {};
    }

    auto [tminus, succ] =
        vec::read(h.in, in_access(v, tplus, u, h), alloc);
    --tminus;
    journey j({u, succ, tminus}, h.delta);

    while(succ != v)
    {
        auto [next_tminus, next_succ] =
            vec::read(h.in, in_access(v, tplus, succ, h), alloc);
        next_tminus--;

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::binary_searches()
    -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::sequential_searches()
    -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::tuple_inserts()
    -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::tuple_updates()
    -> uint64_t
{
    return total_updates;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_expanded_filtered<Alloc, PAGE_SIZE>::opt_space() -> uint64_t
{
    return total_opt_space;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_expanded_filtered<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
    total_opt_space = 0;
}

#endif

} // namespace tgraph_disk_reachability
