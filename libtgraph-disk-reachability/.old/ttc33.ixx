#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_disk_reachability::detail3
{
const static uint32_t null = std::numeric_limits<uint32_t>::max();
} // namespace tgraph_disk_reachability::detail3

namespace tgraph_disk_reachability
{
auto by_in_access2::operator()(uint32_t u, uint32_t t, uint32_t v, uint32_t n,
                               uint32_t tau) const -> uint32_t
{
    return (u * tau + t) * n + v;
}

auto by_out_access2::operator()(uint32_t u, uint32_t t, uint32_t v, uint32_t n,
                                uint32_t tau) const -> uint32_t
{
    return (u * tau + t) * n + v;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, disk::allocator<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    h.in = vec::create(n * tau * n, alloc);
    vec::fill(h.in, {detail3::null, n}, alloc);
    h.out = vec::create(n * tau * n, alloc);
    vec::fill(h.out, {detail3::null, n}, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    return h;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
template<typename It>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::create(
    uint32_t n, uint32_t tau, uint32_t delta, It contacts_begin,
    It contacts_end, disk::allocator<PAGE_SIZE>* alloc) -> header
{
    header h = create(n, tau, delta, alloc);
    InAccess in_access;

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        if(insert(&h, u, v, t, t + h.delta, v, alloc))
        {
            if(t >= h.delta)
            {
                for(uint32_t wminus = 0; wminus < h.n; ++wminus)
                {
                    if(wminus == v)
                    {
                        continue;
                    }

                    if(const auto& [tminus, succ] = vec::read(
                           h.in, in_access(u, t - h.delta, wminus, h.n, h.tau),
                           *alloc);
                       tminus != detail3::null)
                    {
                        insert(&h, wminus, v, tminus, t + h.delta, succ, alloc);
                    }
                }
#ifdef BENCHMARK_ON
                ++total_sequential_searches;
#endif
            }
        }
    }

    return h;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::destroy(
    header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->in, alloc);
    vec::destroy(&h->out, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::clear(
    header* h, disk::allocator<PAGE_SIZE>* alloc)
{
    vec::fill(*h->in, {detail3::null, h->n}, alloc);
    vec::fill(*h->out, {detail3::null, h->n}, alloc);
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::add_contact(
    header* h, contact c, disk::allocator<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    if(insert(h, u, v, t, t + h->delta, v, alloc))
    {
        OutAccess out_access;
        InAccess in_access;
        std::vector<entry> row(h->n);
        std::vector<std::pair<uint32_t, uint32_t>> dout;
        std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> din;

        if(t + h->delta < h->tau)
        {
            collect_out(*h, v, t + h->delta, out_access, &row, *alloc);
            dout = insert_from_out(h, u, t, &row, v, alloc);
        }

        if(t >= h->delta)
        {
            collect_in(*h, u, t - h->delta, in_access, &row, *alloc);
            din = insert_from_in(h, v, t + h->delta, &row, alloc);
        }

        insert_from_in_out(h, &din, &dout, alloc);
    }
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval i,
    const disk::allocator<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    return std::min(
               vec::read(h.out, OutAccess {}(u, i.left, v, h.n, h.tau), alloc)
                   .t,
               h.tau + h.delta - 1)
           < i.right;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const disk::allocator<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return {};
    }

    OutAccess out_access;
    auto [tplus, _] =
        vec::read(h.out, out_access(u, i.left, v, h.n, h.tau), alloc);
    if(std::min(tplus, h.tau + h.delta - 1) >= i.right)
    {
        return {};
    }

    InAccess in_access;
    auto [tminus, succ] =
        vec::read(h.in, in_access(v, tplus - h.delta, u, h.n, h.tau), alloc);
    journey j({u, succ, tminus}, h.delta);

    while(succ != v)
    {
        auto [next_tminus, next_succ] = vec::read(
            h.in, in_access(v, tplus - h.delta, succ, h.n, h.tau), alloc);

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
}

#endif

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::insert(
    header* h, uint32_t u, uint32_t v, uint32_t t1, uint32_t t2, uint32_t succ,
    disk::allocator<PAGE_SIZE>* alloc) -> bool
{
    OutAccess out_access;
    if(vec::read(h->out, out_access(u, t1, v, h->n, h->tau), *alloc).t < t2)
    {
        return false;
    }

    uint32_t tt = t1 + 1;
    while(tt > 0)
    {
        uint32_t tplus =
            vec::read(h->out, out_access(u, tt - 1, v, h->n, h->tau), *alloc).t;
        if(tplus != detail3::null && tplus <= t2)
        {
            break;
        }

#ifdef BENCHMARK_ON
        if(tplus == detail3::null)
        {
            ++total_inserts;
        }
        else
        {
            ++total_updates;
        }
#endif

        vec::write(h->out, out_access(u, tt - 1, v, h->n, h->tau), {t2, succ},
                   alloc);
        --tt;
    }
#ifdef BENCHMARK_ON
    if(tt < t1 + 1)
    {
        ++total_sequential_searches;
    }
#endif

    InAccess in_access;
    tt = t2;
    while(tt < h->tau + h->delta)
    {
        uint32_t tminus =
            vec::read(h->in, in_access(v, tt - h->delta, u, h->n, h->tau),
                      *alloc)
                .t;
        if(tminus != detail3::null && tminus >= t1)
        {
            break;
        }

#ifdef BENCHMARK_ON
        if(tminus == detail3::null)
        {
            ++total_inserts;
        }
        else
        {
            ++total_updates;
        }
#endif

        vec::write(h->in, in_access(v, tt - h->delta, u, h->n, h->tau),
                   {t1, succ}, alloc);
        ++tt;
    }
#ifdef BENCHMARK_ON
    if(tt > t2)
    {
        ++total_sequential_searches;
    }
#endif

    return true;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::insert_from_in(
    header* h, uint32_t v, uint32_t t2, std::vector<entry>* row,
    disk::allocator<PAGE_SIZE>* alloc)
    -> std::vector<std::tuple<uint32_t, uint32_t, uint32_t>>
{
    InAccess in_access;
    OutAccess out_access;

    auto d = filter_in(*h, v, t2, out_access, *row, *alloc);
    auto dd = d;
    while(!dd.empty())
    {
        for(auto it = dd.rbegin(); it != dd.rend();)
        {
            auto& [u, tt1, succ] = *it;

            uint32_t idx = out_access(u, tt1, v, h->n, h->tau);
            uint32_t tplus = vec::read(h->out, idx, *alloc).t;

            if(tplus != detail3::null && tplus <= t2)
            {
                dd.erase((++it).base());
                continue;
            }

            vec::write(h->out, idx, {t2, succ}, alloc);

            if(tt1 == 0)
            {
                dd.erase((++it).base());
            }
            else
            {
                --tt1;
                ++it;
            }
        }
    }

    std::vector<std::tuple<uint32_t, uint32_t, uint32_t, uint32_t>> ddd;
    for(auto& [u, t1, succ]: d)
    {
        ddd.emplace_back(u, t1, succ, t2);
    }
    while(!ddd.empty())
    {
        for(auto it = ddd.begin(); it != ddd.end();)
        {
            auto& [u, t1, succ, tt2] = *it;

            uint32_t idx = in_access(v, tt2 - h->delta, u, h->n, h->tau);
            uint32_t tminus = vec::read(h->in, idx, *alloc).t;

            if(tt2 == h->tau + h->delta
               || (tminus != detail3::null && tminus >= t1))
            {
                ddd.erase(it);
                continue;
            }

            vec::write(h->in, idx, {t1, succ}, alloc);
            ++tt2;
            ++it;
        }
    }

    return d;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::insert_from_out(
    header* h, uint32_t u, uint32_t t1, std::vector<entry>* row, uint32_t succ,
    disk::allocator<PAGE_SIZE>* alloc)
    -> std::vector<std::pair<uint32_t, uint32_t>>
{
    InAccess in_access;
    OutAccess out_access;
    auto d = filter_out(*h, u, t1, out_access, *row, *alloc);

    std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> dd;
    for(auto& [v, t2]: d)
    {
        dd.emplace_back(v, t2, t1);
    }

    while(!dd.empty())
    {
        for(auto it = dd.rbegin(); it != dd.rend();)
        {
            auto& [v, t2, tt1] = *it;

            uint32_t idx = out_access(u, tt1, v, h->n, h->tau);
            uint32_t tplus = vec::read(h->out, idx, *alloc).t;

            if(tplus != detail3::null && tplus <= t2)
            {
                dd.erase((++it).base());
                continue;
            }

            vec::write(h->out, idx, {t2, succ}, alloc);

            if(tt1 == 0)
            {
                dd.erase((++it).base());
            }
            else
            {
                --tt1;
                ++it;
            }
        }
    }

    for(auto& [v, t2]: d)
    {
        dd.emplace_back(v, t2, t1);
    }

    while(!dd.empty())
    {
        for(auto it = dd.begin(); it != dd.end();)
        {
            auto& [v, tt2, _] = *it;

            uint32_t idx = in_access(v, tt2 - h->delta, u, h->n, h->tau);
            uint32_t tminus = vec::read(h->in, idx, *alloc).t;

            if(tt2 == h->tau + h->delta
               || (tminus != detail3::null && tminus >= t1))
            {
                dd.erase(it);
                continue;
            }

            vec::write(h->in, idx, {t1, succ}, alloc);
            ++tt2;
            ++it;
        }
    }

    return d;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::insert_from_in_out(
    header* h, std::vector<std::tuple<uint32_t, uint32_t, uint32_t>>* din,
    std::vector<std::pair<uint32_t, uint32_t>>* dout,
    disk::allocator<PAGE_SIZE>* alloc)
{
    InAccess in_access;
    OutAccess out_access;

    for(uint32_t i = 0; i < din->size(); ++i)
    {
        auto const& [u, t1, succ] = (*din)[i];

        std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> dd;
        for(auto& [v, t2]: *dout)
        {
            dd.emplace_back(v, t2, t1);
        }

        while(!dd.empty())
        {
            for(auto it = dd.rbegin(); it != dd.rend();)
            {
                auto& [v, t2, tt1] = *it;

                uint32_t idx = out_access(u, tt1, v, h->n, h->tau);
                uint32_t tplus = vec::read(h->out, idx, *alloc).t;

                if(tplus != detail3::null && tplus <= t2)
                {
                    dd.erase((++it).base());
                    continue;
                }

                vec::write(h->out, idx, {t2, succ}, alloc);

                if(tt1 == 0)
                {
                    dd.erase((++it).base());
                }
                else
                {
                    --tt1;
                    ++it;
                }
            }
        }

        for(auto& [v, t2]: *dout)
        {
            dd.emplace_back(v, t2, t1);
        }

        while(!dd.empty())
        {
            for(auto it = dd.begin(); it != dd.end();)
            {
                auto& [v, tt2, _] = *it;

                uint32_t idx = in_access(v, tt2 - h->delta, u, h->n, h->tau);
                uint32_t tminus = vec::read(h->in, idx, *alloc).t;

                if(tt2 == h->tau + h->delta
                   || (tminus != detail3::null && tminus >= t1))
                {
                    dd.erase(it);
                    continue;
                }

                vec::write(h->in, idx, {t1, succ}, alloc);
                ++tt2;
                ++it;
            }
        }
    }
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::collect_in(
    const header& h, uint32_t u, uint32_t t, InAccess access,
    std::vector<entry>* row, const disk::allocator<PAGE_SIZE>& alloc)
{
    for(uint32_t wminus = 0; wminus < h.n; ++wminus)
    {
        (*row)[wminus] =
            vec::read(h.in, access(u, t, wminus, h.n, h.tau), alloc);
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
void ttc33<InAccess, OutAccess, PAGE_SIZE>::collect_out(
    const header& h, uint32_t u, uint32_t t, OutAccess access,
    std::vector<entry>* row, const disk::allocator<PAGE_SIZE>& alloc)
{
    std::vector<entry> entries;
    for(uint32_t wplus = h.n; wplus > 0; --wplus)
    {
        (*row)[wplus - 1] =
            vec::read(h.out, access(u, t, wplus - 1, h.n, h.tau), alloc);
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::filter_in(
    const header& h, uint32_t v, uint32_t t2, OutAccess access,
    const std::vector<entry>& row, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::vector<std::tuple<uint32_t, uint32_t, uint32_t>>
{
    if(t2 == detail3::null)
    {
        return {};
    }

    std::vector<std::tuple<uint32_t, uint32_t, uint32_t>> filtered;
    for(uint32_t u = 0; u < h.n; ++u)
    {
        auto const& [t1, succ] = row[u];
        if(u != v && t1 != detail3::null
           && vec::read(h.out, access(u, t1, v, h.n, h.tau), alloc).t > t2)
        {
            filtered.emplace_back(u, t1, succ);
        }
    }

    return filtered;
}

template<typename InAccess, typename OutAccess, size_t PAGE_SIZE>
auto ttc33<InAccess, OutAccess, PAGE_SIZE>::filter_out(
    const header& h, uint32_t u, uint32_t t, OutAccess access,
    const std::vector<entry>& row, const disk::allocator<PAGE_SIZE>& alloc)
    -> std::vector<std::pair<uint32_t, uint32_t>>
{
    if(t == detail3::null)
    {
        return {};
    }

    std::vector<std::pair<uint32_t, uint32_t>> filtered;
    for(uint32_t v = 0; v < h.n; ++v)
    {
        if(u != v
           && vec::read(h.out, access(u, t, v, h.n, h.tau), alloc).t > row[v].t)
        {
            filtered.emplace_back(v, row[v].t);
        }
    }

    return filtered;
}

} // namespace tgraph_disk_reachability
