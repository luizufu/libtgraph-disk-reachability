#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

namespace tgraph_disk_reachability
{
struct by_in_access
{
    auto operator()(uint32_t u, uint32_t t, uint32_t v, uint32_t n,
                    uint32_t tau) const -> uint32_t;
};

struct by_out_access
{
    auto operator()(uint32_t u, uint32_t t, uint32_t v, uint32_t n,
                    uint32_t tau) const -> uint32_t;
};

template<typename InAccess = by_in_access, typename OutAccess = by_out_access,
         size_t PAGE_SIZE = 4096>
class ttc32
{
    struct entry
    {
        uint32_t t = 0;
        uint32_t succ = 0;
    };
    using vec = disk::vector<entry, PAGE_SIZE>;

public:
    struct header
    {
        typename vec::header in = {};
        typename vec::header out = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       disk::allocator<PAGE_SIZE>* alloc =
                           disk::default_allocator<PAGE_SIZE>()) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       disk::allocator<PAGE_SIZE>* alloc =
                           disk::default_allocator<PAGE_SIZE>()) -> header;
    static void destroy(header* h, disk::allocator<PAGE_SIZE>* alloc =
                                       disk::default_allocator<PAGE_SIZE>());
    static void clear(header* h, disk::allocator<PAGE_SIZE>* alloc =
                                     disk::default_allocator<PAGE_SIZE>());

    static void add_contact(header* h, contact c,
                            disk::allocator<PAGE_SIZE>* alloc =
                                disk::default_allocator<PAGE_SIZE>());

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const disk::allocator<PAGE_SIZE>& alloc =
                              *disk::default_allocator<PAGE_SIZE>()) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i,
                                    const disk::allocator<PAGE_SIZE>& alloc =
                                        *disk::default_allocator<PAGE_SIZE>())
        -> std::optional<journey>;

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif

private:
    static auto insert(header* h, uint32_t u, uint32_t v, uint32_t t1,
                       uint32_t t2, uint32_t succ,
                       disk::allocator<PAGE_SIZE>* alloc) -> bool;
    static void collect_in(const header& h, uint32_t u, uint32_t t,
                           InAccess access, std::vector<entry>* row,
                           const disk::allocator<PAGE_SIZE>& alloc);
    static void collect_out(const header& h, uint32_t u, uint32_t t,
                            OutAccess access, std::vector<entry>* row,
                            const disk::allocator<PAGE_SIZE>& alloc);
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc32.ixx>
