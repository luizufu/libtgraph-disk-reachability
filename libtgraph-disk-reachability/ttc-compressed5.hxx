#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/detail/compressor-map.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <algorithm>
#include <deque>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <span>

namespace tgraph_disk_reachability
{

template<const char* Compressor, template<size_t> typename Alloc,
         size_t PAGE_SIZE = 4096>
class ttc_compressed5
{
    using vec_block = disk::vector<uint32_t, Alloc, PAGE_SIZE>;
    using vec_header =
        disk::vector<typename vec_block::header, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        typename vec_header::header in = {};
        typename vec_header::header out = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
        uint32_t z = 1;
        uint32_t h.z = 1;
        uint32_t h.z = 1;
        uint32_t levels = 1;
        uint32_t n_nodes = 1;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c,
                            Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v,
                          interval i, const Alloc<PAGE_SIZE>& alloc)
        -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u,
                                    uint32_t v, interval i,
                                    const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto space(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> uint64_t
    {
        uint64_t total = 0;

        {
            auto it = vec_header::begin(h.in, alloc);
            auto end = vec_header::end(h.in, alloc);

            for(; it != end; ++it)
            {
                total += sizeof(*it) + it->size * sizeof(uint32_t);
            }
        }

        {
            auto it = vec_header::begin(h.out, alloc);
            auto end = vec_header::end(h.out, alloc);

            for(; it != end; ++it)
            {
                total += sizeof(*it) + it->size * sizeof(uint32_t);
            }
        }

        return total;
    }

    static auto print(const header& h, const Alloc<PAGE_SIZE>& alloc)
    {
        // uint32_t nil = std::numeric_limits<uint32_t>::max();

        // std::cout << "\nout   ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << "    ";
        // for(uint32_t k = 0; k < h.n; ++k)
        // {
        //     std::cout << std::setw(3) << std::right << k;
        //     std::cout << " ";
        // }
        // std::cout << '\n' << std::endl;

        // for(uint32_t u = 0; u < h.n; ++u)
        // {
        //     for(uint32_t t = 0; t < h.tau; ++t)
        //     {
        //         std::cout << std::setw(6) << std::left
        //                   << (std::to_string(u) + "-" +
        //                   std::to_string(t));
        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h),
        //                 alloc);
        //             if(tplus == nil)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right <<
        //                 tplus; std::cout << " ";
        //             }
        //         }

        //         std::cout << "    ";

        //         for(uint32_t v = 0; v < h.n; ++v)
        //         {
        //             auto [tplus, next] =
        //                 vec::read(h.out, out_access(u, t, v, h),
        //                 alloc);
        //             if(next == h.n)
        //             {
        //                 std::cout << "  X ";
        //             }
        //             else
        //             {
        //                 std::cout << std::setw(3) << std::right <<
        //                 next; std::cout << " ";
        //             }
        //         }

        //         std::cout << std::endl;
        //     }
        // }

        // // std::cout << "\nin    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << "    ";
        // // for(uint32_t k = 0; k < h.n; ++k)
        // // {
        // //     std::cout << std::setw(3) << std::right << k;
        // //     std::cout << " ";
        // // }
        // // std::cout << '\n' << std::endl;
        // // for(uint32_t v = 0; v < h.n; ++v)
        // // {
        // //     for(uint32_t t = 0; t < h.tau; ++t)
        // //     {
        // //         std::cout << std::setw(6) << std::left
        // //                   << (std::to_string(v) + "-" +
        // std::to_string(t));
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h.n,
        // h.tau),
        // //                 alloc);
        // //             if(tminus == nil)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // tminus;
        // //                 std::cout << " ";
        // //             }
        // //         }
        // //         std::cout << "    ";
        // //         for(uint32_t u = 0; u < h.n; ++u)
        // //         {
        // //             auto [tminus, next] =
        // //                 vec::read(h.in, in_access(v, t, u, h),
        // //                 alloc);
        // //             if(next == h.n)
        // //             {
        // //                 std::cout << "  X ";
        // //             }
        // //             else
        // //             {
        // //                 std::cout << std::setw(3) << std::right <<
        // next;
        // //                 std::cout << " ";
        // //             }
        // //         }

        // //         std::cout << std::endl;
        // //     }
        // // }
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static auto opt_space() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static uint64_t total_opt_space = 0;
#endif
private:
    struct location
    {
        uint32_t _z;
        uint32_t _div;
        uint64_t base;
        uint32_t _id;
        uint32_t pos;
        uint32_t _i;

        location() = default;
        location(uint32_t w, uint32_t t, const header& h)
            : _z(h.z)
            , _div(std::pow(_z, h.levels - 1))
            , base(w * h.n_nodes)
            , _id(0)
            , pos(t / _div)
            , _i(t % _div)
        {
            _div /= _z;
        }

        [[nodiscard]] auto id() const -> uint64_t
        {
            return base + _id + 1;
        }

        auto next(uint32_t p) const -> location
        {
            location other;
            other._z = _z;
            other.base = base;
            other._id = _id * _z + p + 1;
            other.pos = p > pos ? 0 : _i / _div;
            other._i = p > pos ? 0 : _i % _div;
            other._div = _div / _z;

            return other;
        }

        [[nodiscard]] auto has_next() const -> bool
        {
            return _div > 0;
        }
    };

    static auto buffer1(uint32_t size) -> uint32_t*
    {
        static std::vector<uint32_t> buffer;
        buffer.resize(size + 32);
        return buffer.data();
    }

    static auto buffer2(uint32_t size) -> uint32_t*
    {
        static std::vector<uint32_t> buffer;
        buffer.resize(size + 1024);
        return buffer.data();
    }

    static void read_block(const header& h,
                           const typename vec_block::header& hb,
                           uint32_t* compressed, uint32_t* original,
                           uint32_t m, const Alloc<PAGE_SIZE>& alloc)
    {
        vec_block::read_n(hb, 0, hb.size, compressed, alloc);

        size_t size = h.n * m;
        codec_map(Compressor)
            .decodeArray(compressed, hb.size, original, size);

        for(uint32_t i = 0; i < h.n; ++i)
        {
            std::partial_sum(&original[i * m], &original[i * m + m],
                             &original[i * m]);
        }
    }

    static auto write_block(const header& h,
                            const typename vec_block::header& hb,
                            uint32_t* compressed, uint32_t* original,
                            uint32_t m, Alloc<PAGE_SIZE>* alloc) ->
        typename vec_block::header
    {
        for(uint32_t i = 0; i < h.n; ++i)
        {
            std::adjacent_difference(&original[i * m],
                                     &original[i * m + m],
                                     &original[i * m]);
        }

        size_t size = h.n * m;
        codec_map(Compressor)
            .encodeArray(original, h.n * m, compressed, size);

        auto tmp = hb;
        vec_block::resize(&tmp, size, alloc);
        vec_block::write_n(tmp, 0, size, compressed, alloc);

        return tmp;
    }

    static void get_adjacency(const header& h, uint32_t w1,
                              const uint32_t* w2s, uint32_t t1,
                              uint32_t* t2s, uint32_t n, bool out,
                              const Alloc<PAGE_SIZE>& alloc)
    {
        location loc(w1, t1, h);
        auto header = out ? h.out : h.in;
        auto original = buffer1(h.n * h.z);
        auto compressed = buffer2(h.n * h.z);
        std::vector<uint32_t> copy(w2s, w2s + n);

        auto hb = vec_header::read(header, loc.base, alloc);
        read_block(h, hb, compressed, original, 2, alloc);

        for(uint32_t w2: copy)
        {
            t2s[w2] += original[w2 * 2];
        }

        copy.erase(std::remove_if(copy.begin(), copy.end(),
                                  [&](uint32_t w2)
                                  {
                                      return original[w2 * 2]
                                             == original[w2 * 2 + 1];
                                  }),
                   copy.end());

        do
        {
            loc = loc.next(loc.pos);
            if(loc.pos > 0)
            {
                read_block(h, vec_header::read(header, loc.id(), alloc),
                           original, compressed, h.z, alloc);

                for(uint32_t w2: copy)
                {
                    t2s[w2] += original[w2 * h.z + loc.pos - 1];
                }

                if(loc.has_next())
                {
                    copy.erase(
                        std::remove_if(
                            copy.begin(), copy.end(),
                            [&](uint32_t w2)
                            {
                                return original[w2 * h.z + loc.pos - 1]
                                       == original[w2 * h.z + loc.pos];
                            }),
                        copy.end());
                }
            }

        } while(loc.has_next() && !copy.empty());
    }

    struct child_info
    {
        uint32_t i;
        uint32_t old_parent;
        bool need_cleaning;
    };

    static auto
    clean_block(const header& h, const typename vec_header::header& hb,
                const location& loc,
                const std::vector<child_info>& info, uint32_t* original)
        -> std::map<uint32_t, std::vector<child_info>>
    {
        for(auto& [w2, _1, _2]: info)
        {
            std::fill(&original[w2 * h.h.z],
                      &original[(w2 + 1) * h.h.z], 0);
        }

        return {};
    }

    static void add_to_block(const header& h, const location& loc,
                             uint32_t* t2s, uint32_t* original,
                             const std::vector<child_info>& info)
        ->std::map<uint32_t, std::vector<child_info>>
    {
        uint32_t max_j = 0;
        bool updated_i1 = false;
        std::vector<uint32_t> old_values(h.h.z);

        for(auto& [w2, old_parent, need_cleaning]: info)
        {
            if(need_cleaning)
            {
                std::fill(&original[w2 * h.h.z],
                          &original[(w2 + 1) * h.h.z], 0);
            }

            if(pos == loc.pos && loc2.pos > 0)
            {
                for(uint32_t j = loc2.pos - 1;
                    j < h.h.z
                    && t2s[i]
                           > old_parent + original[w2 * h.h.z + j];
                    ++j)
                {
                    original[w2 * h.h.z + j] = t2s[i] - old_parent;
                }
            }
        }

        for(uint32_t i = 0; i < n; ++i)
        {
            std::copy(&original[w2s[i] * h.h.z],
                      &original[(w2s[i] + 1) * h.h.z], &old_values[0]);

            uint32_t j = loc.pos;
            if(t2s[i] > original[w2s[i] * h.h.z + j])
            {
                bool updated_i1_local = false;
                if(loc.has_next() && loc.next(loc.pos).pos == 0)
                {
                    updated_i1_local = true;
                    original[w2s[i] * h.h.z + j] = t2s[i];
                }

                ++j;

                for(;
                    j < h.h.z && t2s[i] > original[w2s[i] * h.h.z + j];
                    ++j)
                {
                    original[w2s[i] * h.h.z + j] = t2s[i];
                }

                if(updated_i1_local && loc.pos > 0
                   && old_values[loc.pos - 1] == old_values[loc.pos])
                {
                    children_to_update[loc.pos - 1].push_back({
                        .i = i,
                        .need_cleaning = true,
                    });
                }

                if(original[w2s[i] * h.h.z + loc.pos]
                   < original[w2s[i] * h.h.z + loc.pos + 1])
                {
                    children_to_update[loc.pos].push_back(
                        {.i = i,
                         .old_parent = old_values[loc.pos],
                         .need_cleaning = old_values[loc.pos]
                                          == old_values[loc.pos + 1]});
                }

                if(j - 1 > loc.pos && j < h.h.z
                   && original[w2s[i] * h.h.z + j - 1]
                          < original[w2s[i] * h.h.z + j])
                {
                    children_to_update[j - 1].push_back(
                        {.i = i,
                         .old_parent = old_values[j - 1],
                         .need_cleaning =
                             old_values[j - 1] == old_values[j]});
                }

                max_j = std::max(j, max_j);
                updated_i1 |= updated_i1_local;
            }
        }

        if(updated_i1 || max_j > loc.pos + 1)
        {
            auto tmp = write_block(h, hb, compressed.data(),
                                   original.data(), h.h.z, alloc);
            if(!is_same_header(tmp, hb))
            {
                updated_headers.push_back({tmp, loc.id()});
            }
        }
    }

    static void fix_block(
        const header& h, const location& loc,
        std::vector<uint32_t>& w2s, uint32_t* t2s, uint32_t* original,
        std::map<uint32_t, std::vector<child_info>>& update_info,
        std::vector<std::pair<typename vec_block::header, uint64_t>>&
            updated_headers)
    {
        for(auto [pos, info]: children_to_update)
        {
            auto loc2 = loc.next(pos);
            hb = vec_header::read(header, loc2.id(), *alloc);
            read_block(h, hb, compressed.data(), original.data(), h.h.z,
                       *alloc);

            for(auto& [i, old_parent, need_cleaning]: info)
            {
                if(need_cleaning)
                {
                    std::fill(&original[w2s[i] * h.h.z],
                              &original[(w2s[i] + 1) * h.h.z], 0);
                }

                if(pos == loc.pos && loc2.pos > 0)
                {
                    for(uint32_t j = loc2.pos - 1;
                        j < h.h.z
                        && t2s[i] > old_parent
                                        + original[w2s[i] * h.h.z + j];
                        ++j)
                    {
                        original[w2s[i] * h.h.z + j] =
                            t2s[i] - old_parent;
                    }
                }
                else if(pos >= loc.pos)
                {
                    uint32_t diff = t2s[i] - old_parent;
                    for(uint32_t j = 0; j < h.h.z; ++j)
                    {
                        original[w2s[i] * h.h.z + j] =
                            original[w2s[i] * h.h.z + j] > diff
                                ? original[w2s[i] * h.h.z + j] - diff
                                : 0;
                    }
                }
            }

            auto tmp = write_block(h, hb, compressed.data(),
                                   original.data(), h.h.z, alloc);
            if(!is_same_header(tmp, hb))
            {
                updated_headers.push_back({tmp, loc2.id()});
            }
        }
    }

    static void
    update_adjacency_rec(const header& h, location loc,
                         std::vector<uint32_t>& w2s,
                         const uint32_t* t2s, bool out,
                         uint32_t parent_pos,
                         std::vector<child_info>& update_info,
                         update_type type, Alloc<PAGE_SIZE>* alloc)
    {
        auto original = buffer1(h.n * h.z);
        auto compressed = buffer2(h.n * h.z);
        auto header = out ? h.out : h.in;

        auto hb = vec_header::read(header, loc.id(), *alloc);
        read_block(h, hb, compressed, original, h.h.z, *alloc);

        auto child_to_update = type == update_type::clean
                                   ? clean(h, hb, w2s, t2s, original)
                               : pos == loc.pos && loc2.pos > 0
                                   ? add(h, hb, w2s, t2s, original)
                                   : fix(h, hb, w2s, t2s, original);

            if()

        auto tmp = write_block(h, hb, compressed.data(),
                               original.data(), h.h.z, alloc);
        if(!is_same_header(tmp, hb))
        {
            vec_header::write(header, loc.id(), tmp, *alloc);
        }

        for(auto [pos, info]: children_to_update)
        {
            update_adjacency_rec(h, loc.next(pos), t2s, info, out,
                                 alloc);
        }
    }
}

static void
update_adjacency(const header& h, uint32_t w1, const uint32_t* w2s,
                 uint32_t t1, const uint32_t* t2s, uint32_t n, bool out,
                 Alloc<PAGE_SIZE>* alloc)
{
    location loc(w1, t1, h);
    auto original = buffer1(h.n * h.z);
    auto compressed = buffer2(h.n * h.z);
    auto header = out ? h.out : h.in;
    std::vector<uint32_t> copy(w2s, w2s + n);

    auto hb = vec_header::read(header, loc.id(), *alloc);
    read_block(h, hb, compressed.data(), original.data(), h.h.z,
               *alloc);
}

[[nodiscard]] static auto
is_same_header(const typename vec_block::header& h1,
               const typename vec_block::header& h2) -> bool
{
    return h1.head == h2.head && h1.size == h2.size
           && h1.capacity == h2.capacity;
}

[[nodiscard]] static auto
is_header_empty(const typename vec_block::header& h) -> bool
{
    return h.head == 0;
}
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-compressed5.ixx>
