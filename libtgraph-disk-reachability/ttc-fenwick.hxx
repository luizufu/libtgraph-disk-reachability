#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/vector.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <iomanip>
#include <limits>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class ttc_fenwick
{
    using vec = disk::vector<uint32_t, Alloc, PAGE_SIZE>;

public:
    struct header
    {
        typename vec::header header = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c, Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i, const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

    static auto add_(const header& h, uint32_t u, uint32_t v, uint32_t t1,
                     uint32_t t2, Alloc<PAGE_SIZE>* alloc) -> bool
    {
        return add(h, u, v, t1, t2, alloc);
    }

    static auto sum_(const header& h, uint32_t u, uint32_t v, uint32_t t1,
                     const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        return sum(h, u, v, t1, alloc);
    }

    static auto find_(const header& h, uint32_t u, uint32_t v, uint32_t t2,
                      const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        return find(h, u, v, t2, alloc);
    }

    static void print_(const header& h, uint32_t u, uint32_t v,
                       const Alloc<PAGE_SIZE>& alloc)
    {
        for(uint32_t i = 0; i < h.tau + 1; ++i)
            std::cout << std::setw(3) << i << ", ";
        std::cout << '\n' << std::endl;

        // for(uint32_t i = 0; i < h.tau; ++i)
        //     std::cout << std::setw(3) << inverse(i, h.tau) << ", ";
        // std::cout << '\n' << std::endl;

        std::vector<uint32_t> tplus(h.tau);
        vec::read_n(h.header, access(u, v, 0, h), h.tau, tplus.data(), alloc);
        for(auto& t: tplus)
        {
            // if(t == h.tau)
            // {
            //     --t;
            // }
            std::cout << std::setw(3) << (t) << ", ";
        }
        std::cout << std::endl;

        for(uint32_t i = 0; i < h.tau; ++i)
        {
            auto opt = sum(h, u, v, i, alloc);
            std::string str = opt ? std::to_string(*opt) : "-1";
            std::cout << std::setw(3) << str << ", ";
        }
        std::cout << std::endl;

        // std::cout << std::setw(5) << " ";
        for(uint32_t i = 0; i < h.tau + 1; ++i)
        {
            auto opt = find(h, u, v, i, alloc);
            std::string str = opt ? std::to_string(*opt) : "-1";
            std::cout << std::setw(3) << str << ", ";
        }
        std::cout << std::endl;
    }

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_updates = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif
private:
    static auto access(uint64_t u, uint64_t v, uint64_t t, const header& h)
        -> uint64_t
    {
        return (u * h.n + v) * h.tau + t;
    }

    static auto inverse(uint32_t i, uint32_t bit_size) -> uint32_t
    {
        return (bit_size - i - 1);
    }

    static auto sum(const header& h, uint32_t u, uint32_t v, uint32_t tminus,
                    const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        if(tminus >= h.tau)
        {
            return std::nullopt;
        }

        std::vector<uint64_t> indexes;
        {
            uint64_t first = access(u, v, 0, h);
            uint32_t i = inverse(tminus, h.tau);
            for(++i; i > 0; i -= i & -i)
            {
                indexes.push_back(first + i - 1);
            }
        }

        uint32_t tplus = h.tau;
        if(!indexes.empty())
        {
            uint32_t cur = indexes.size();
            vec::read_lazy(
                h.header, indexes[cur - 1],
                [&tplus, &indexes,
                 &cur](const uint32_t& val) -> std::optional<uint64_t>
                {
                    tplus = std::min(tplus, val);
                    --cur;

                    return (cur > 0) ? std::make_optional(indexes[cur - 1])
                                     : std::nullopt;
                },
                alloc);
        }
        tplus += h.delta;
        return tplus < h.tau + h.delta ? std::make_optional(tplus)
                                       : std::nullopt;
    }

    static auto add(const header& h, uint32_t u, uint32_t v, uint32_t tminus,
                    uint32_t tplus, Alloc<PAGE_SIZE>* alloc) -> bool
    {
        if(tminus >= h.tau || tplus >= h.tau + h.delta)
        {
            return false;
        }

        std::vector<uint64_t> indexes;
        {
            uint64_t first = access(u, v, 0, h);
            uint32_t i = inverse(tminus, h.tau);
            for(++i; i <= h.tau; i += i & -i)
            {
                indexes.push_back(first + i - 1);
            }
        }

        bool ret = false;
        if(!indexes.empty())
        {
            tplus -= h.delta;
            uint32_t cur = 0;
            vec::write_lazy(
                h.header, indexes[0],
                [&indexes, &cur, &ret,
                 tplus](uint32_t* val) -> std::optional<uint64_t>
                {
                    if(tplus < *val)
                    {
                        *val = tplus;
                        ret = true;
                    }
                    ++cur;

                    return (cur < indexes.size())
                               ? std::make_optional(indexes[cur])
                               : std::nullopt;
                },
                alloc);
        }

        return ret;
    }

    static auto find(const header& h, uint32_t u, uint32_t v, uint32_t tplus,
                     const Alloc<PAGE_SIZE>& alloc) -> std::optional<uint32_t>
    {
        if(tplus >= h.tau + h.delta)
        {
            return std::nullopt;
        }

        uint64_t first = first = access(u, v, 0, h);
        uint32_t tminus = 0;
        uint32_t j = 1U << static_cast<uint32_t>(std::log2(h.tau));
        vec::read_lazy(
            h.header, first + tminus + j - 1,
            [first, &tminus, &j, tplus,
             h](const uint32_t& val) -> std::optional<uint64_t>
            {
                if(val + h.delta > tplus)
                {
                    tminus += j;
                }

                do
                {
                    j >>= 1U;
                } while(tminus + j > h.tau);

                return (j > 0) ? std::make_optional(first + tminus + j - 1)
                               : std::nullopt;
            },
            alloc);
        // return tminus;

        // return std::make_optional(inverse(tminus, h.tau));
        return tminus < h.tau ? std::make_optional(inverse(tminus, h.tau))
                              : std::nullopt;
    }
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/ttc-fenwick.ixx>
