#pragma once

#include <libdisk/allocator.hxx>
#include <libdisk/set.hxx>
#include <libtgraph-disk-reachability/journey.hxx>

#include <unordered_map>
#include <vector>
#include <optional>

namespace tgraph_disk_reachability
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE = 4096>
class baseline
{
    using set = disk::set<contact, Alloc, std::less<contact>, PAGE_SIZE>;
    using journey_map = std::unordered_map<uint32_t, std::vector<journey>>;

public:
    struct header
    {
        typename set::header set_header = {};
        uint32_t n = 0;
        uint32_t tau = 0;
        uint32_t delta = 0;
    };

    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    template<typename It>
    static auto create(uint32_t n, uint32_t tau, uint32_t delta,
                       It contacts_begin, It contacts_end,
                       Alloc<PAGE_SIZE>* alloc) -> header;

    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void add_contact(header* h, contact c, Alloc<PAGE_SIZE>* alloc);

    static auto can_reach(const header& h, uint32_t u, uint32_t v, interval i,
                          const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto reconstruct_journey(const header& h, uint32_t u, uint32_t v,
                                    interval i, const Alloc<PAGE_SIZE>& alloc)
        -> std::optional<journey>;

#ifdef BENCHMARK_ON
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tuple_inserts() -> uint64_t;
    static auto tuple_updates() -> uint64_t;
    static auto tuple_erases() -> uint64_t;
    static void reset_benchmark();

    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static uint64_t total_inserts = 0;
#endif

private:
    static auto construct_trivial(const baseline::header& h, uint32_t source,
                                  uint32_t target, interval i,
                                  const Alloc<PAGE_SIZE>& alloc)
        -> journey_map;
};

} // namespace tgraph_disk_reachability

#include <libtgraph-disk-reachability/baseline.ixx>
