#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>
#include <cmath>

namespace tgraph_disk_reachability
{
// TODO remove fill operations if it does not change
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                                 uint32_t delta,
                                                 Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    uint64_t size = static_cast<uint64_t>(n) * static_cast<uint64_t>(n)
                    * static_cast<uint64_t>(tau);
    h.header = vec::create(size, alloc);
    vec::fill(h.header, tau, alloc);
    h.n = n;
    h.tau = tau;
    h.delta = delta;

    h.levels[0] = 0;
    for(uint32_t i = 1; i < lambda(tau) + 1; ++i)
    {
        h.levels[i] = (tau + (1U << (i - 1))) / (1U << i) + h.levels[i - 1];
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
template<typename It>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t tau,
                                                 uint32_t delta,
                                                 It contacts_begin,
                                                 It contacts_end,
                                                 Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = create(n, tau, delta, alloc);

    for(auto it = contacts_begin; it != contacts_end; ++it)
    {
        const auto& [u, v, t] = *it;

        if(u == v || t >= h.tau)
        {
            continue;
        }

        // put here code for inserting ordered (u, v, t)
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_level_fenwick<Alloc, PAGE_SIZE>::destroy(header* h,
                                                  Alloc<PAGE_SIZE>* alloc)
{
    vec::destroy(&h->header, alloc);
    h->n = 0;
    h->tau = 0;
    h->delta = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_level_fenwick<Alloc, PAGE_SIZE>::clear(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    vec::fill(*h->header, h->tau, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_level_fenwick<Alloc, PAGE_SIZE>::add_contact(header* h, contact c,
                                                      Alloc<PAGE_SIZE>* alloc)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= h->tau)
    {
        return;
    }

    if(auto uv = sum(*h, u, v, t, *alloc); !uv || *uv != t + h->delta)
    {
        add(*h, u, v, t, t + h->delta, alloc);

        std::vector<std::pair<uint32_t, uint32_t>> d;

        for(uint32_t wplus = 0; wplus < h->n; ++wplus)
        {
            if(wplus == u || wplus == v)
            {
                continue;
            }

            if(auto tplus = sum(*h, v, wplus, t + h->delta, *alloc))
            {
                if(add(*h, u, wplus, t, *tplus, alloc))
                {
                    d.emplace_back(wplus, *tplus);
                }
            }
        }

        for(uint32_t wminus = 0; wminus < h->n; ++wminus)
        {
            if(wminus == u || wminus == v)
            {
                continue;
            }

            if(auto tminus = find(*h, wminus, u, t, *alloc))
            {
                if(add(*h, wminus, v, *tminus, t + h->delta, alloc))
                {
                    for(const auto& [wplus, tplus]: d)
                    {
                        add(*h, wminus, wplus, *tminus, tplus, alloc);
                    }
                }
            }
        }

        // std::vector<std::pair<uint32_t, uint32_t>> in;
        // for(uint32_t wminus = 0; wminus < h->n; ++wminus)
        // {
        //     if(wminus == v)
        //     {
        //         continue;
        //     }
        //     else if(wminus == u)
        //     {
        //         in.emplace_back(u, t);
        //     }
        //     else if(auto tminus = find(*h, wminus, u, t, *alloc))
        //     {
        //         in.emplace_back(wminus, *tminus);
        //     }
        // }

        // std::vector<std::pair<uint32_t, uint32_t>> out;
        // for(uint32_t wplus = 0; wplus < h->n; ++wplus)
        // {
        //     if(wplus == u)
        //     {
        //         continue;
        //     }
        //     else if(wplus == v)
        //     {
        //         out.emplace_back(v, t + h->delta);
        //     }
        //     else if(auto tplus = sum(*h, v, wplus, t + h->delta, *alloc))
        //     {
        //         out.emplace_back(wplus, *tplus);
        //     }
        // }

        // for(const auto& [wminus, tminus]: in)
        // {
        //     for(const auto& [wplus, tplus]: out)
        //     {
        //         add(*h, wminus, wplus, tminus, tplus, alloc);
        //     }
        // }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::can_reach(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, h.tau + h.delta);

    if(i.left >= h.tau || i.right - i.left < h.delta)
    {
        return false;
    }

    auto opt = sum(h, u, v, i.left, alloc);
    return opt ? std::min(*opt, h.tau + h.delta - 1) < i.right : false;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::reconstruct_journey(
    const header& h, uint32_t u, uint32_t v, interval i,
    const Alloc<PAGE_SIZE>& alloc) -> std::optional<journey>
{
    return {};
}

#ifdef BENCHMARK_ON

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::tuple_inserts() -> uint64_t
{
    return total_inserts;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::tuple_updates() -> uint64_t
{
    return total_updates;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto ttc_level_fenwick<Alloc, PAGE_SIZE>::tuple_erases() -> uint64_t
{
    return 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void ttc_level_fenwick<Alloc, PAGE_SIZE>::reset_benchmark()
{
    total_sequential_searches = 0;
    total_binary_searches = 0;
    total_inserts = 0;
    total_updates = 0;
}

#endif

} // namespace tgraph_disk_reachability
