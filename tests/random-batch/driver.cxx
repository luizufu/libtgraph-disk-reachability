#include <libdisk/allocator.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libtgraph-disk-reachability/baseline.hxx>
#include <libtgraph-disk-reachability/ttc-expanded.hxx>
#include <libtgraph-disk-reachability/ttc-trees.hxx>
#include <libtgraph-disk-reachability/version.hxx>

#include <unordered_set>
#include <vector>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <cassert>
#include <cmath>

using edge = std::pair<uint32_t, uint32_t>;

template<typename Index, template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_timestamp_insertion(uint32_t n, uint32_t tau);

auto main() -> int
{
    using namespace graph_disk_structures;
    using namespace tgraph_disk_reachability;

    // test_timestamp_insertion<ttc_trees<adj_matrix, 256>, 256>(32, 32);
    test_timestamp_insertion<ttc_expanded<disk::allocator, 256>,
                             disk::allocator, 256>(32, 32);

    return 0;
}

static unsigned int SEED = {};

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static bool first = true;

    if(first)
    {
        SEED = r();
        /* SEED = 1430173019; */
        first = false;
    }
    static std::mt19937 gen(SEED);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto generate_interval(uint32_t tau) -> tgraph_disk_reachability::interval
{
    std::uniform_int_distribution<uint32_t> tdist(0, tau - 1);

    tgraph_disk_reachability::interval i = {tdist(generator()),
                                            tdist(generator())};

    if(i.right < i.left)
    {
        std::swap(i.left, i.right);
    }
    else if(i.left == i.right)
    {
        i.right += 1;
    }

    return i;
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<std::pair<edge, tgraph_disk_reachability::interval>>
{
    std::vector<std::pair<edge, tgraph_disk_reachability::interval>> contacts;

    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(uint32_t t1 = 1; t1 < t; ++t1)
    {
        for(uint32_t u = 0; u < n; ++u)
        {
            for(uint32_t v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        contacts.push_back({{u, v}, {g[u * n + v], t1}});
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                contacts.push_back({{u, v}, {g[u * n + v], t}});
            }
        }
    }

    return contacts;
}

template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}

template<typename Index, template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_timestamp_insertion(uint32_t n, uint32_t tau)
{
    /* std::cerr << "Testing n = " << n << "; tau = " << tau << "; " */
    /*           << type_name<tgraph_reachability::ttc_trees<Graph>>() <<
     * std::endl;
     */

    /* std::cout << "initiating test " << n << ", " << tau << std::endl; */

    std::filesystem::remove("random_test_base");
    std::filesystem::remove("random_test_ttc_trees");
    Alloc<PAGE_SIZE> alloc1("random_test_base", 1);
    Alloc<PAGE_SIZE> alloc2("random_test_ttc_trees", 1);

    using Base = tgraph_disk_reachability::baseline<Alloc, PAGE_SIZE>;

    std::uniform_int_distribution<uint32_t> vdist(0, n - 1);
    auto icontacts = make_emeg(n, tau, 0.1, 0.3);
    std::vector<tgraph_disk_reachability::contact> contacts;
    for(const auto& [e, interv]: icontacts)
    {
        for(uint32_t t = interv.left; t < interv.right; ++t)
        {
            contacts.emplace_back(
                tgraph_disk_reachability::contact {e.first, e.second, t});
        }
    }
    std::sort(contacts.begin(), contacts.end(),
              [](const auto& lhs, const auto& rhs)
              {
                  auto tlhs = std::make_tuple(lhs.t, lhs.u, lhs.v);
                  auto trhs = std::make_tuple(rhs.t, rhs.u, rhs.v);
                  return tlhs < trhs;
              });

    auto h1 =
        Base::create(n, tau, 1, contacts.begin(), contacts.end(), &alloc1);
    auto h2 =
        Index::create(n, tau, 1, contacts.begin(), contacts.end(), &alloc2);

    for(uint32_t i = 0; i < 200; ++i)
    {
        uint32_t u = vdist(generator());
        uint32_t v = vdist(generator());
        auto interv = generate_interval(tau);

        bool res1 = Base::can_reach(h1, u, v, interv, alloc1);
        bool res2 = Index::can_reach(h2, u, v, interv, alloc2);

        if(res1 != res2)
        {
            std::cout << "t2 error: " << res1 << std::endl;
            std::cout << "Query: " << u << ", " << v << ", " << interv.left
                      << ", " << interv.right << std::endl;
        }
        assert(res1 == res2);
    }

    std::filesystem::remove("random_base");
    std::filesystem::remove("random_test");
}
