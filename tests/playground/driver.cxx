#include <libdisk/allocator.hxx>
#include <libtgraph-disk-reachability/ttc-compressed.hxx>
#include <libtgraph-disk-reachability/ttc-compressed2.hxx>
#include <libtgraph-disk-reachability/ttc-compressed3.hxx>
#include <libtgraph-disk-reachability/ttc-compressed6.hxx>
#include <libtgraph-disk-reachability/ttc-compressed7.hxx>
#include <libtgraph-disk-reachability/version.hxx>

#include <vector>
#include <filesystem>
#include <iostream>

const char SIMPLE8B[] = "simple8b";

auto main() -> int
{
    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;
    const uint32_t d = 3;
    const uint32_t e = 4;
    const uint32_t f = 5;
    const uint32_t g = 6;

    using namespace tgraph_disk_reachability;
    using Index = ttc_compressed7<SIMPLE8B, disk::allocator, 256>;

    std::filesystem::remove("256_allocator");
    disk::allocator<256> alloc("256_allocator", 0);
    auto h = Index::create(2, 4, 1, &alloc);


    Index::add_contact(&h, {0, 1, 3}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 2}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);

    assert(Index::can_reach(h, 0, 1, {0, 4}, alloc));


    std::filesystem::remove("256_allocator");
}
