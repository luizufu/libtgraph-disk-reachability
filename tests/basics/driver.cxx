#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libdisk/set.hxx>
// #include <libgraph-disk-structures/adj-list.hxx>
// #include <libgraph-disk-structures/adj-matrix.hxx>
// #include <libgraph-disk-structures/edge-grid.hxx>
#include <libtgraph-disk-reachability/baseline.hxx>
// #include <libtgraph-disk-reachability/ttc-compressed.hxx>
// #include <libtgraph-disk-reachability/ttc-compressed2.hxx>
// #include <libtgraph-disk-reachability/ttc-compressed7.hxx>
#include <libtgraph-disk-reachability/ttc-compressed8.hxx>
// #include <libtgraph-disk-reachability/ttc-expanded-bycol.hxx>
// #include <libtgraph-disk-reachability/ttc-expanded-filtered.hxx>
// #include <libtgraph-disk-reachability/ttc-expanded.hxx>
// #include <libtgraph-disk-reachability/ttc-fenwick.hxx>
// #include <libtgraph-disk-reachability/ttc-level-fenwick.hxx>
// #include <libtgraph-disk-reachability/ttc-trees.hxx>
#include <libtgraph-disk-reachability/version.hxx>

#include <set>
#include <vector>
#include <filesystem>
#include <iostream>
#include <cassert>

const char SIMPLE8B[] = "simple8b";

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_basic();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_two_jumps();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_find_better_journey();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_baseline_error();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_delete_multiple_labels();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error2();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error3();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error4();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error5();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc2_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc3_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc5_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error2();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error3();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error4();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc7_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc7_error2();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_fen_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_compressed3_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_compressed3_error2();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_compressed7_error1();
template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_manager1();

auto main() -> int
{
    using namespace tgraph_disk_reachability;
    // using namespace graph_disk_structures;

    // test_basic<baseline<disk::buffered_allocator, 256>,
    //            disk::buffered_allocator, 256>();
    // test_basic<baseline<disk::allocator, 256>, disk::allocator,
    // 256>(); test_basic<ttc_trees<adj_matrix, disk::allocator, 256>,
    //            disk::allocator, 256>();
    // test_basic<ttc_expanded<disk::allocator, 256>, disk::allocator,
    //            256>();
    // test_basic<ttc_expanded_bycol<disk::allocator, 256>,
    //            disk::allocator, 256>();
    // test_basic<ttc_expanded_filtered<disk::allocator, 256>,
    //            disk::allocator, 256>();
    // test_basic<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //            disk::allocator, 256>();
    // test_basic<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //            disk::allocator, 256>();
    test_basic<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
               disk::allocator, 256>();
    // test_basic<ttc_fenwick<disk::allocator, 256>, disk::allocator,
    //            256>();
    // test_basic<ttc_level_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //            256>();

    // test_two_jumps<baseline<disk::allocator, 256>, disk::allocator,
    //                256>();
    // test_two_jumps<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_two_jumps<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_two_jumps<ttc_expanded_bycol<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_two_jumps<ttc_expanded_filtered<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_two_jumps<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_two_jumps<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    test_two_jumps<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                   disk::allocator, 256>();
    // test_two_jumps<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_two_jumps<ttc_level_fenwick<disk::allocator, 256>,
    //                disk::allocator, 256>();

    // test_find_better_journey<baseline<disk::allocator, 256>,
    //                          disk::allocator, 256>();
    // test_find_better_journey<
    //     ttc_trees<adj_matrix, disk::allocator, 256>, disk::allocator,
    //     256>();
    // test_find_better_journey<ttc_expanded<disk::allocator, 256>,
    //                          disk::allocator, 256>();
    // test_find_better_journey<ttc_expanded_bycol<disk::allocator,
    // 256>,
    //                          disk::allocator, 256>();
    // test_find_better_journey<
    //     ttc_expanded_filtered<disk::allocator, 256>, disk::allocator,
    //     256>();
    // test_find_better_journey<
    //     ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //     disk::allocator, 256>();
    // test_find_better_journey<
    //     ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //     disk::allocator, 256>();
    test_find_better_journey<
        ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
        disk::allocator, 256>();
    // test_find_better_journey<ttc_fenwick<disk::allocator, 256>,
    //                          disk::allocator, 256>();
    // test_find_better_journey<ttc_level_fenwick<disk::allocator, 256>,
    //                          disk::allocator, 256>();

    // test_baseline_error<baseline<disk::allocator, 256>,
    // disk::allocator,
    //                     256>();
    // test_baseline_error<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                     disk::allocator, 256>();
    // test_baseline_error<ttc_expanded<disk::allocator, 256>,
    //                     disk::allocator, 256>();
    // test_baseline_error<ttc_expanded_bycol<disk::allocator, 256>,
    //                     disk::allocator, 256>();
    // test_baseline_error<ttc_expanded_filtered<disk::allocator, 256>,
    //                     disk::allocator, 256>();
    // test_baseline_error<ttc_compressed<SIMPLE8B, disk::allocator,
    // 256>,
    //                     disk::allocator, 256>();
    // test_baseline_error<ttc_compressed2<SIMPLE8B, disk::allocator,
    // 256>,
    //                     disk::allocator, 256>();
    test_baseline_error<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                        disk::allocator, 256>();
    // test_baseline_error<ttc_fenwick<disk::allocator, 256>,
    //                     disk::allocator, 256>();
    // test_baseline_error<ttc_level_fenwick<disk::allocator, 256>,
    //                     disk::allocator, 256>();

    // test_delete_multiple_labels<baseline<disk::allocator, 256>,
    //                             disk::allocator, 256>();
    // test_delete_multiple_labels<
    //     ttc_trees<adj_matrix, disk::allocator, 256>, disk::allocator,
    //     256>();
    // test_delete_multiple_labels<ttc_expanded<disk::allocator, 256>,
    //                             disk::allocator, 256>();
    // test_delete_multiple_labels<
    //     ttc_expanded_bycol<disk::allocator, 256>, disk::allocator,
    //     256>();
    // test_delete_multiple_labels<
    //     ttc_expanded_filtered<disk::allocator, 256>, disk::allocator,
    //     256>();
    // test_delete_multiple_labels<
    //     ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //     disk::allocator, 256>();
    // test_delete_multiple_labels<
    //     ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //     disk::allocator, 256>();
    test_delete_multiple_labels<
        ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
        disk::allocator, 256>();
    // test_delete_multiple_labels<ttc_fenwick<disk::allocator, 256>,
    //                             disk::allocator, 256>();
    // test_delete_multiple_labels<ttc_level_fenwick<disk::allocator,
    // 256>,
    //                             disk::allocator, 256>();

    // test_tc_error<baseline<disk::allocator, 256>, disk::allocator,
    //               256>();
    // test_tc_error<ttc_trees<adj_matrix, disk::allocator, 256>,
    //               disk::allocator, 256>();
    // test_tc_error<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //               256>();
    // test_tc_error<ttc_expanded_bycol<disk::allocator, 256>,
    //               disk::allocator, 256>();
    // test_tc_error<ttc_expanded_filtered<disk::allocator, 256>,
    //               disk::allocator, 256>();
    // test_tc_error<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //               disk::allocator, 256>();
    // test_tc_error<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //               disk::allocator, 256>();
    test_tc_error<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                  disk::allocator, 256>();
    // test_tc_error<ttc_fenwick<disk::allocator, 256>, disk::allocator,
    //               256>();
    // test_tc_error<ttc_level_fenwick<disk::allocator, 256>,
    //               disk::allocator, 256>();

    // test_tc_error2<baseline<disk::allocator, 256>, disk::allocator,
    //                256>();
    // test_tc_error2<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error2<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error2<ttc_expanded_bycol<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error2<ttc_expanded_filtered<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error2<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error2<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    test_tc_error2<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                   disk::allocator, 256>();
    // test_tc_error2<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error2<ttc_level_fenwick<disk::allocator, 256>,
    //                disk::allocator, 256>();

    // test_tc_error3<baseline<disk::allocator, 256>, disk::allocator,
    //                256>();
    // test_tc_error3<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error3<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error3<ttc_expanded_bycol<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error3<ttc_expanded_filtered<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error3<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error3<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error3<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error3<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error3<ttc_level_fenwick<disk::allocator, 256>,
    //                disk::allocator, 256>();

    // test_tc_error4<baseline<disk::allocator, 256>, disk::allocator,
    //                256>();
    // test_tc_error4<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error4<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error4<ttc_expanded_bycol<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error4<ttc_expanded_filtered<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error4<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error4<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    test_tc_error4<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                   disk::allocator, 256>();
    // test_tc_error4<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error4<ttc_level_fenwick<disk::allocator, 256>,
    //                disk::allocator, 256>();

    // test_tc_error5<baseline<disk::allocator, 256>, disk::allocator,
    //                256>();
    // test_tc_error5<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error5<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error5<ttc_expanded_bycol<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error5<ttc_expanded_filtered<disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error5<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    // test_tc_error5<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                disk::allocator, 256>();
    test_tc_error5<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                   disk::allocator, 256>();
    // test_tc_error5<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                256>();
    // test_tc_error5<ttc_level_fenwick<disk::allocator, 256>,
    //                disk::allocator, 256>();

    // test_tc2_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc2_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc2_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc2_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc2_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc2_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc2_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc2_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc2_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc2_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc3_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc3_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc3_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc3_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc3_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc3_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc3_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc3_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc3_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc3_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc5_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc5_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc5_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc5_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc5_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc5_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc5_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc5_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc5_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc5_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc6_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc6_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc6_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc6_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc6_error2<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc6_error2<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error2<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error2<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error2<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error2<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error2<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc6_error2<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc6_error2<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error2<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc6_error3<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc6_error3<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error3<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error3<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error3<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error3<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error3<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc6_error3<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc6_error3<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error3<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc6_error4<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc6_error4<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error4<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error4<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error4<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error4<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc6_error4<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc6_error4<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc6_error4<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc6_error4<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc7_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc7_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc7_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc7_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc7_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc7_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_tc7_error2<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_tc7_error2<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error2<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc7_error2<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error2<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error2<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_tc7_error2<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_tc7_error2<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_tc7_error2<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_tc7_error2<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_fen_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_fen_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_fen_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_fen_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_fen_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_fen_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_fen_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_fen_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_fen_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_fen_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    //

    // test_compressed3_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_compressed3_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_compressed3_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_compressed3_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_compressed3_error1<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_compressed3_error1<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_compressed3_error2<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_compressed3_error2<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error2<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_compressed3_error2<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error2<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error2<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed3_error2<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_compressed3_error2<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_compressed3_error2<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_compressed3_error2<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_compressed7_error1<baseline<disk::allocator, 256>, disk::allocator,
    //                 256>();
    // test_compressed7_error1<ttc_trees<adj_matrix, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed7_error1<ttc_expanded<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_compressed7_error1<ttc_expanded_bycol<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed7_error1<ttc_expanded_filtered<disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed7_error1<ttc_compressed<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    // test_compressed7_error1<ttc_compressed2<SIMPLE8B, disk::allocator, 256>,
    //                 disk::allocator, 256>();
    test_compressed7_error1<ttc_compressed8<SIMPLE8B, disk::allocator, 256>,
                    disk::allocator, 256>();
    // test_compressed3_error2<ttc_fenwick<disk::allocator, 256>,
    // disk::allocator,
    //                 256>();
    // test_compressed3_error2<ttc_level_fenwick<disk::allocator, 256>,
    //                 disk::allocator, 256>();

    // test_manager1<baseline<disk::allocator, 256>, disk::allocator,
    // 256 > ();
    // test_manager1<ttc_trees<adj_matrix, disk::allocator, 256>,
    // disk::allocator, 256>(); test_manager1<ttc_expanded<4096>,
    // 4096>();
}

template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_true_journey(const typename Index::header& h, uint32_t u,
                       uint32_t v, uint32_t t1, uint32_t t2,
                       const Alloc<PAGE_SIZE>& alloc)
{
    auto j = Index::reconstruct_journey(h, u, v, {t1, t2}, alloc);

    assert(j.has_value());
    assert(j->from() == u);
    assert(j->to() == v);
    assert(t1 <= j->departure());
    assert(j->arrival() < t2);

    const auto& cs = j->contacts();
    for(size_t i = 1; i < cs.size(); ++i)
    {
        assert(cs[i - 1].t < cs[i].t);
    }
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_false_journey(const typename Index::header& h, uint32_t u,
                        uint32_t v, uint32_t t1, uint32_t t2,
                        const Alloc<PAGE_SIZE>& alloc)
{
    auto j = Index::reconstruct_journey(h, u, v, {t1, t2}, alloc);
    assert(!j.has_value());
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto create_index(uint32_t n, uint32_t tau, uint32_t delta, uint32_t z,
                  Alloc<PAGE_SIZE>* alloc)
{
    // using namespace graph_disk_structures;
    using namespace tgraph_disk_reachability;
    if constexpr( //
                  // std::is_same_v<Index,
                  //                ttc_trees<adj_matrix, Alloc,
                  //                PAGE_SIZE>> || //
        std::is_same_v<Index, baseline<Alloc,
                                       PAGE_SIZE>> || //
        std::is_same_v<Index, ttc_compressed8<SIMPLE8B, Alloc,
                                       PAGE_SIZE>> //
        // std::is_same_v<Index,
        //                ttc_expanded_bycol<Alloc, PAGE_SIZE>> ||  //
        // std::is_same_v<Index, ttc_expanded<Alloc, PAGE_SIZE>> || //
        // std::is_same_v<Index,
        //                ttc_expanded_filtered<Alloc, PAGE_SIZE>> || //
        // std::is_same_v<Index,
        //                ttc_fenwick<Alloc, PAGE_SIZE>> || //
        // std::is_same_v<Index,
        //                ttc_level_fenwick<Alloc, PAGE_SIZE>> //
    )
    {
        return Index::create(n, tau, delta, alloc);
    }
    else
    {
        return Index::create(n, tau, delta, z, alloc);
    }
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_basic()
{
    std::cout << "Testing basic; " << type_name<Index>() << std::endl;

    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;
    const uint32_t d = 3;
    const uint32_t e = 4;
    const uint32_t f = 5;
    const uint32_t g = 6;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(7, 12, 1, 2, &alloc);

    Index::add_contact(&h, {a, b, 1}, &alloc);
    Index::add_contact(&h, {b, c, 3}, &alloc);
    Index::add_contact(&h, {c, d, 5}, &alloc);
    Index::add_contact(&h, {a, e, 4}, &alloc);
    Index::add_contact(&h, {e, d, 9}, &alloc);
    Index::add_contact(&h, {a, f, 6}, &alloc);
    Index::add_contact(&h, {f, g, 6}, &alloc);
    Index::add_contact(&h, {g, d, 6}, &alloc);
    Index::add_contact(&h, {a, b, 2}, &alloc);
    Index::add_contact(&h, {b, c, 4}, &alloc);
    Index::add_contact(&h, {c, d, 6}, &alloc);
    Index::add_contact(&h, {a, e, 5}, &alloc);
    Index::add_contact(&h, {e, d, 10}, &alloc);
    Index::add_contact(&h, {a, f, 7}, &alloc);
    Index::add_contact(&h, {f, g, 7}, &alloc);
    Index::add_contact(&h, {g, d, 7}, &alloc);
    Index::add_contact(&h, {a, f, 8}, &alloc);
    Index::add_contact(&h, {f, g, 8}, &alloc);
    Index::add_contact(&h, {g, d, 8}, &alloc);
    Index::add_contact(&h, {g, d, 9}, &alloc);
    // Index::print(h, alloc);

    assert(Index::can_reach(h, a, b, {0, 12}, alloc));
    assert(Index::can_reach(h, a, c, {0, 12}, alloc));
    assert(Index::can_reach(h, a, d, {0, 12}, alloc));
    assert(Index::can_reach(h, a, f, {0, 12}, alloc));
    assert(Index::can_reach(h, a, g, {0, 12}, alloc));
    assert(Index::can_reach(h, b, c, {0, 12}, alloc));
    assert(Index::can_reach(h, b, d, {0, 12}, alloc));
    assert(Index::can_reach(h, e, d, {0, 12}, alloc));
    assert(Index::can_reach(h, f, g, {0, 12}, alloc));
    assert(Index::can_reach(h, f, d, {0, 12}, alloc));
    assert(Index::can_reach(h, g, d, {0, 12}, alloc));
    assert(!Index::can_reach(h, a, b, {4, 5}, alloc));
    assert(!Index::can_reach(h, a, c, {5, 6}, alloc));
    assert(!Index::can_reach(h, a, d, {20, 40}, alloc));
    assert(!Index::can_reach(h, a, f, {1, 5}, alloc));
    assert(!Index::can_reach(h, a, g, {1, 5}, alloc));
    assert(!Index::can_reach(h, b, c, {0, 2}, alloc));
    assert(!Index::can_reach(h, b, d, {0, 4}, alloc));
    assert(!Index::can_reach(h, e, d, {5, 8}, alloc));
    assert(!Index::can_reach(h, f, g, {1, 5}, alloc));
    assert(!Index::can_reach(h, f, d, {1, 5}, alloc));
    assert(!Index::can_reach(h, g, d, {1, 5}, alloc));

    // test_true_journey<Index>(h, a, d, 0, 12, alloc);
    // test_true_journey<Index>(h, a, d, 4, 12, alloc);
    // test_true_journey<Index>(h, a, d, 6, 12, alloc);
    // test_true_journey<Index>(h, a, d, 7, 12, alloc);
    // test_false_journey<Index>(h, a, d, 8, 12, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_two_jumps()
{
    std::cout << "Testing two_jumps; " << type_name<Index>()
              << std::endl;

    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(3, 4, 1, 2, &alloc);

    Index::add_contact(&h, {a, b, 0}, &alloc);
    Index::add_contact(&h, {a, b, 1}, &alloc);
    Index::add_contact(&h, {a, b, 2}, &alloc);
    Index::add_contact(&h, {a, b, 3}, &alloc);
    Index::add_contact(&h, {c, b, 0}, &alloc);
    Index::add_contact(&h, {c, b, 1}, &alloc);
    Index::add_contact(&h, {c, a, 2}, &alloc);

    assert(Index::can_reach(h, a, b, {0, 2}, alloc));
    assert(Index::can_reach(h, a, b, {1, 3}, alloc));
    assert(Index::can_reach(h, a, b, {2, 4}, alloc));
    assert(Index::can_reach(h, a, b, {3, 5}, alloc));
    assert(Index::can_reach(h, c, b, {0, 2}, alloc));
    assert(Index::can_reach(h, c, b, {1, 3}, alloc));
    assert(Index::can_reach(h, c, a, {2, 4}, alloc));
    assert(Index::can_reach(h, c, b, {2, 5}, alloc));

    // test_true_journey<Index>(h, c, b, 2, 5, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_find_better_journey()
{
    std::cout << "Testing find_better_journey; " << type_name<Index>()
              << std::endl;

    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;
    const uint32_t d = 3;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 20, 1, 2, &alloc);

    Index::add_contact(&h, {a, b, 0}, &alloc);
    Index::add_contact(&h, {a, b, 1}, &alloc);
    Index::add_contact(&h, {a, b, 2}, &alloc);
    Index::add_contact(&h, {a, b, 3}, &alloc);
    Index::add_contact(&h, {b, c, 5}, &alloc);
    Index::add_contact(&h, {b, c, 6}, &alloc);
    Index::add_contact(&h, {b, c, 7}, &alloc);
    Index::add_contact(&h, {b, c, 8}, &alloc);
    Index::add_contact(&h, {c, d, 10}, &alloc);
    Index::add_contact(&h, {c, d, 11}, &alloc);
    Index::add_contact(&h, {c, d, 12}, &alloc);
    Index::add_contact(&h, {c, d, 13}, &alloc);
    Index::add_contact(&h, {a, d, 5}, &alloc);

    assert(Index::can_reach(h, a, b, {0, 2}, alloc));
    assert(Index::can_reach(h, a, b, {1, 3}, alloc));
    assert(Index::can_reach(h, a, b, {2, 4}, alloc));
    assert(Index::can_reach(h, a, b, {3, 5}, alloc));
    assert(Index::can_reach(h, a, d, {5, 7}, alloc));
    // test_true_journey<Index>(h, a, d, 5, 7, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_baseline_error()
{
    std::cout << "Testing baseline_error; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h =
        create_index<Index, Alloc, PAGE_SIZE>(16, 32, 1, 2, &alloc);

    insert(&h, 0, 3, 0, 32, &alloc);
    insert(&h, 9, 1, 0, 15, &alloc);
    insert(&h, 4, 1, 0, 32, &alloc);
    insert(&h, 9, 8, 0, 32, &alloc);
    insert(&h, 3, 2, 0, 14, &alloc);
    insert(&h, 0, 2, 0, 32, &alloc);
    insert(&h, 8, 3, 0, 7, &alloc);
    insert(&h, 4, 0, 0, 32, &alloc);
    insert(&h, 5, 1, 0, 32, &alloc);
    insert(&h, 12, 6, 0, 32, &alloc);
    insert(&h, 11, 1, 0, 32, &alloc);
    insert(&h, 12, 1, 0, 28, &alloc);
    insert(&h, 1, 0, 0, 32, &alloc);

    assert(Index::can_reach(h, 9, 8, {13, 28}, alloc));
    // test_true_journey<Index>(h, 9, 8, 13, 28, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_delete_multiple_labels()
{
    std::cout << "Testing delete_multiple_labels; "
              << type_name<Index>() << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 10, 1, 2, &alloc);

    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 2}, &alloc);
    Index::add_contact(&h, {0, 1, 3}, &alloc);
    Index::add_contact(&h, {1, 2, 2}, &alloc);
    Index::add_contact(&h, {1, 2, 3}, &alloc);
    Index::add_contact(&h, {1, 2, 4}, &alloc);
    Index::add_contact(&h, {2, 3, 3}, &alloc);
    Index::add_contact(&h, {2, 3, 4}, &alloc);
    Index::add_contact(&h, {2, 3, 5}, &alloc);
    Index::add_contact(&h, {0, 3, 3}, &alloc);

    assert(Index::can_reach(h, 0, 3, {3, 5}, alloc));
    // test_true_journey<Index>(h, 0, 3, 3, 5, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error()
{
    std::cout << "Testing tc_error1; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(3, 4, 1, 2, &alloc);

    insert(&h, 0, 2, 0, 4, &alloc);
    insert(&h, 1, 0, 0, 4, &alloc);
    insert(&h, 1, 2, 0, 4, &alloc);

    assert(Index::can_reach(h, 1, 2, {2, 4}, alloc));
    // test_true_journey<Index>(h, 1, 2, 2, 4, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error2()
{
    std::cout << "Testing tc_error2; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h =
        create_index<Index, Alloc, PAGE_SIZE>(32, 32, 1, 2, &alloc);

    insert(&h, 14, 0, 0, 32, &alloc);
    insert(&h, 14, 1, 0, 26, &alloc);
    insert(&h, 1, 2, 0, 32, &alloc);
    insert(&h, 18, 2, 0, 32, &alloc);
    insert(&h, 9, 10, 0, 32, &alloc);
    insert(&h, 7, 3, 0, 32, &alloc);
    insert(&h, 2, 0, 0, 5, &alloc);
    insert(&h, 2, 3, 0, 32, &alloc);
    insert(&h, 3, 5, 0, 32, &alloc);
    insert(&h, 0, 4, 0, 32, &alloc);
    insert(&h, 9, 7, 0, 32, &alloc);
    insert(&h, 27, 2, 0, 32, &alloc);
    insert(&h, 27, 1, 0, 32, &alloc);
    insert(&h, 8, 2, 0, 32, &alloc);
    insert(&h, 5, 0, 0, 32, &alloc);
    insert(&h, 4, 5, 0, 32, &alloc);
    insert(&h, 24, 1, 0, 21, &alloc);
    insert(&h, 21, 5, 0, 32, &alloc);
    insert(&h, 25, 4, 17, 32, &alloc);
    insert(&h, 1, 3, 0, 32, &alloc);
    insert(&h, 6, 4, 0, 32, &alloc);
    insert(&h, 23, 0, 0, 32, &alloc);
    insert(&h, 8, 1, 0, 23, &alloc);
    insert(&h, 16, 4, 0, 32, &alloc);
    insert(&h, 25, 11, 0, 32, &alloc);
    insert(&h, 11, 2, 0, 13, &alloc);
    insert(&h, 25, 2, 0, 16, &alloc);
    insert(&h, 6, 3, 0, 32, &alloc);
    insert(&h, 8, 4, 0, 32, &alloc);
    insert(&h, 26, 3, 0, 32, &alloc);
    insert(&h, 12, 10, 0, 32, &alloc);
    insert(&h, 12, 8, 0, 32, &alloc);
    insert(&h, 5, 2, 0, 32, &alloc);
    insert(&h, 12, 11, 0, 32, &alloc);
    insert(&h, 10, 4, 0, 32, &alloc);
    insert(&h, 7, 9, 0, 32, &alloc);
    insert(&h, 13, 5, 0, 32, &alloc);
    insert(&h, 15, 16, 0, 32, &alloc);
    insert(&h, 15, 4, 0, 32, &alloc);
    insert(&h, 19, 0, 0, 32, &alloc);
    insert(&h, 16, 2, 0, 30, &alloc);
    insert(&h, 15, 1, 0, 32, &alloc);
    insert(&h, 16, 3, 0, 32, &alloc);
    insert(&h, 10, 0, 0, 32, &alloc);
    insert(&h, 9, 4, 0, 32, &alloc);
    insert(&h, 0, 1, 0, 28, &alloc);

    assert(Index::can_reach(h, 0, 2, {19, 22}, alloc));
    // test_true_journey<Index>(h, 0, 2, 19, 22, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error3()
{
    std::cout << "Testing tc_error3; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h =
        create_index<Index, Alloc, PAGE_SIZE>(16, 64, 1, 2, &alloc);

    insert(&h, 3, 1, 31, 64, &alloc);
    insert(&h, 0, 3, 0, 64, &alloc);
    insert(&h, 9, 2, 0, 55, &alloc);
    insert(&h, 1, 2, 0, 38, &alloc);
    insert(&h, 5, 2, 0, 41, &alloc);
    insert(&h, 3, 2, 0, 13, &alloc);
    insert(&h, 7, 0, 14, 33, &alloc);
    insert(&h, 12, 10, 0, 46, &alloc);
    insert(&h, 5, 11, 47, 64, &alloc);
    insert(&h, 0, 2, 39, 59, &alloc);
    insert(&h, 11, 3, 0, 42, &alloc);
    insert(&h, 6, 1, 0, 58, &alloc);
    insert(&h, 9, 1, 26, 54, &alloc);
    insert(&h, 10, 2, 0, 64, &alloc);
    insert(&h, 8, 0, 0, 64, &alloc);
    insert(&h, 6, 3, 0, 4, &alloc);
    insert(&h, 6, 7, 0, 49, &alloc);
    insert(&h, 11, 0, 0, 25, &alloc);
    insert(&h, 11, 2, 0, 22, &alloc);
    insert(&h, 3, 1, 10, 64, &alloc);
    insert(&h, 3, 1, 0, 9, &alloc);
    insert(&h, 9, 8, 23, 64, &alloc);
    insert(&h, 6, 0, 0, 7, &alloc);
    insert(&h, 2, 1, 0, 64, &alloc);
    insert(&h, 0, 1, 0, 29, &alloc);
    insert(&h, 8, 1, 0, 64, &alloc);
    insert(&h, 11, 10, 52, 64, &alloc);
    insert(&h, 10, 2, 57, 64, &alloc);
    insert(&h, 10, 1, 0, 5, &alloc);
    insert(&h, 10, 0, 0, 1, &alloc);
    insert(&h, 9, 3, 0, 2, &alloc);
    insert(&h, 11, 1, 0, 60, &alloc);
    insert(&h, 1, 3, 0, 64, &alloc);
    insert(&h, 7, 1, 0, 64, &alloc);
    insert(&h, 4, 0, 0, 45, &alloc);
    insert(&h, 8, 9, 0, 63, &alloc);
    insert(&h, 12, 4, 0, 40, &alloc);
    insert(&h, 9, 1, 0, 6, &alloc);
    insert(&h, 12, 9, 0, 12, &alloc);

    assert(!Index::can_reach(h, 12, 0, {13, 15}, alloc));
    // test_false_journey<Index>(h, 12, 0, 13, 15, alloc);

    assert(Index::can_reach(h, 12, 0, {13, 16}, alloc));
    // test_true_journey<Index>(h, 12, 0, 13, 16, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error4()
{
    std::cout << "Testing tc_error4; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h =
        create_index<Index, Alloc, PAGE_SIZE>(5, 512, 1, 2, &alloc);

    insert(&h, 3, 0, 263, 512, &alloc);
    insert(&h, 3, 2, 0, 203, &alloc); // esquisito
    insert(&h, 2, 0, 0, 255, &alloc);
    insert(&h, 0, 1, 250, 326, &alloc);
    insert(&h, 1, 4, 89, 381, &alloc);

    assert(Index::can_reach(h, 3, 4, {234, 463}, alloc));
    // test_true_journey<Index>(h, 3, 4, 234, 463, alloc);

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc_error5()
{
    std::cout << "Testing tc_error5; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(8, 32, 1, 2, &alloc);

    insert(&h, 6, 7, 19, 30, &alloc);
    insert(&h, 5, 7, 21, 26, &alloc);
    insert(&h, 5, 6, 9, 11, &alloc);
    insert(&h, 5, 7, 28, 32, &alloc);
    insert(&h, 5, 7, 0, 11, &alloc);

    assert(Index::can_reach(h, 5, 7, {10, 19}, alloc));

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc2_error1()
{
    std::cout << "Testing tc2_error1; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(6, 16, 1, 2, &alloc);

    insert(&h, 0, 1, 0, 1, &alloc);
    insert(&h, 2, 3, 2, 6, &alloc);
    insert(&h, 4, 5, 11, 14, &alloc);
    insert(&h, 3, 4, 5, 13, &alloc);
    insert(&h, 1, 2, 0, 2, &alloc);

    assert(Index::can_reach(h, 0, 5, {0, 13}, alloc));

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc3_error1()
{
    std::cout << "Testing tc3_error1; " << type_name<Index>()
              << std::endl;

    auto insert = [](auto* header, uint32_t u, uint32_t v,
                     uint32_t start, uint32_t end,
                     Alloc<PAGE_SIZE>* alloc)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            Index::add_contact(header, {u, v, t}, alloc);
        }
    };

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 4, 1, 2, &alloc);

    insert(&h, 0, 2, 1, 4, &alloc);
    insert(&h, 1, 2, 3, 4, &alloc);
    insert(&h, 0, 1, 0, 1, &alloc);

    assert(Index::can_reach(h, 0, 2, {0, 3}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc5_error1()
{
    std::cout << "Testing tc5_error1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 4, 1, 2, &alloc);

    Index::add_contact(&h, {2, 3, 3}, &alloc);
    Index::add_contact(&h, {1, 2, 2}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 2}, &alloc);

    assert(!Index::can_reach(h, 0, 3, {2, 3}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error1()
{
    std::cout << "Testing tc6_error1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(5, 5, 1, 2, &alloc);

    Index::add_contact(&h, {2, 3, 2}, &alloc);
    Index::add_contact(&h, {2, 3, 3}, &alloc);
    Index::add_contact(&h, {3, 4, 4}, &alloc);
    Index::add_contact(&h, {1, 2, 1}, &alloc);
    Index::add_contact(&h, {1, 2, 2}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {0, 2, 1}, &alloc);
    Index::add_contact(&h, {0, 2, 2}, &alloc);

    assert(Index::can_reach(h, 0, 4, {2, 6}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error2()
{
    std::cout << "Testing tc6_error2; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 4, 1, 2, &alloc);

    Index::add_contact(&h, {1, 2, 1}, &alloc);
    Index::add_contact(&h, {1, 2, 2}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {2, 3, 1}, &alloc);
    Index::add_contact(&h, {2, 3, 2}, &alloc);
    Index::add_contact(&h, {2, 3, 3}, &alloc);
    Index::add_contact(&h, {0, 2, 1}, &alloc);
    Index::add_contact(&h, {0, 2, 0}, &alloc);

    assert(Index::can_reach(h, 0, 3, {1, 5}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error3()
{
    std::cout << "Testing tc6_error3; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(5, 7, 1, 2, &alloc);

    Index::add_contact(&h, {0, 2, 0}, &alloc);
    Index::add_contact(&h, {1, 3, 4}, &alloc);
    Index::add_contact(&h, {3, 4, 2}, &alloc);
    Index::add_contact(&h, {3, 4, 5}, &alloc);
    Index::add_contact(&h, {1, 4, 6}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);
    Index::add_contact(&h, {0, 1, 4}, &alloc);
    Index::add_contact(&h, {0, 3, 4}, &alloc);
    Index::add_contact(&h, {2, 3, 1}, &alloc);

    assert(Index::can_reach(h, 0, 4, {3, 7}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc6_error4()
{
    std::cout << "Testing tc6_error4; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(5, 5, 1, 2, &alloc);

    Index::add_contact(&h, {3, 4, 4}, &alloc);
    Index::add_contact(&h, {1, 2, 1}, &alloc);
    Index::add_contact(&h, {2, 3, 3}, &alloc);
    Index::add_contact(&h, {2, 3, 2}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);

    assert(Index::can_reach(h, 0, 4, {0, 6}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc7_error1()
{
    std::cout << "Testing tc7_error1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(5, 4, 1, 2, &alloc);

    Index::add_contact(&h, {2, 3, 2}, &alloc);
    Index::add_contact(&h, {1, 2, 1}, &alloc);
    Index::add_contact(&h, {3, 4, 3}, &alloc);
    Index::add_contact(&h, {1, 3, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);

    assert(Index::can_reach(h, 0, 4, {0, 5}, alloc));
    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_tc7_error2()
{
    std::cout << "Testing tc7_error2; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(6, 6, 1, 2, &alloc);

    Index::add_contact(&h, {4, 5, 5}, &alloc);
    Index::add_contact(&h, {3, 4, 3}, &alloc);
    Index::add_contact(&h, {3, 4, 4}, &alloc);
    Index::add_contact(&h, {2, 3, 2}, &alloc);
    Index::add_contact(&h, {2, 3, 3}, &alloc);
    Index::add_contact(&h, {1, 2, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);

    assert(Index::can_reach(h, 0, 5, {0, 7}, alloc));
    assert(Index::can_reach(h, 4, 5, {5, 7}, alloc));
    assert(Index::can_reach(h, 3, 4, {3, 5}, alloc));
    assert(Index::can_reach(h, 3, 4, {4, 6}, alloc));
    assert(Index::can_reach(h, 2, 3, {2, 4}, alloc));
    assert(Index::can_reach(h, 2, 3, {3, 5}, alloc));
    assert(Index::can_reach(h, 1, 2, {1, 3}, alloc));
    assert(Index::can_reach(h, 0, 1, {0, 2}, alloc));

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_fen_error1()
{
    std::cout << "Testing fen_error1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(3, 8, 1, 2, &alloc);

    Index::add_contact(&h, {0, 2, 4}, &alloc);
    Index::add_contact(&h, {1, 2, 7}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);

    assert(Index::can_reach(h, 0, 2, {1, 6}, alloc));
    assert(Index::can_reach(h, 1, 2, {2, 9}, alloc));
    assert(Index::can_reach(h, 0, 1, {1, 3}, alloc));
    assert(Index::can_reach(h, 0, 2, {1, 9}, alloc));

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_compressed3_error1()
{
    std::cout << "Testing compressed3_error1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 4, 1, 2, &alloc);

    Index::add_contact(&h, {0, 2, 0}, &alloc);
    Index::add_contact(&h, {0, 2, 1}, &alloc);
    Index::add_contact(&h, {0, 2, 2}, &alloc);
    Index::add_contact(&h, {1, 3, 2}, &alloc);
    Index::add_contact(&h, {1, 3, 3}, &alloc);
    Index::add_contact(&h, {0, 1, 0}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 2}, &alloc);
    Index::add_contact(&h, {2, 3, 1}, &alloc);


    assert(!Index::can_reach(h, 0, 3, {0, 2}, alloc));

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_compressed3_error2()
{
    std::cout << "Testing compressed3_error2; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(4, 8, 1, 2, &alloc);

    Index::add_contact(&h, {2, 3, 5}, &alloc);
    Index::add_contact(&h, {2, 3, 6}, &alloc);
    Index::add_contact(&h, {2, 3, 7}, &alloc);
    Index::add_contact(&h, {0, 2, 3}, &alloc);
    Index::add_contact(&h, {0, 2, 4}, &alloc);
    Index::add_contact(&h, {0, 2, 5}, &alloc);
    Index::add_contact(&h, {0, 2, 6}, &alloc);
    Index::add_contact(&h, {0, 2, 7}, &alloc);
    Index::add_contact(&h, {0, 3, 1}, &alloc);
    Index::add_contact(&h, {0, 3, 2}, &alloc);
    Index::add_contact(&h, {0, 3, 3}, &alloc);
    Index::add_contact(&h, {0, 3, 4}, &alloc);
    Index::add_contact(&h, {0, 3, 5}, &alloc);

    assert(!Index::can_reach(h, 0, 3, {3, 4}, alloc));

    std::filesystem::remove("256_allocator");
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_compressed7_error1()
{
    std::cout << "Testing compressed7_error1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("256_allocator");
    Alloc<PAGE_SIZE> alloc("256_allocator", 0);

    auto h = create_index<Index, Alloc, PAGE_SIZE>(3, 8, 1, 2, &alloc);

    Index::add_contact(&h, {0, 1, 0}, &alloc);
    Index::add_contact(&h, {0, 1, 1}, &alloc);
    Index::add_contact(&h, {0, 1, 2}, &alloc);
    Index::add_contact(&h, {1, 2, 0}, &alloc);
    Index::add_contact(&h, {1, 2, 1}, &alloc);
    Index::add_contact(&h, {1, 2, 2}, &alloc);
    Index::add_contact(&h, {1, 2, 3}, &alloc);

    assert(Index::can_reach(h, 0, 2, {1, 5}, alloc));

    std::filesystem::remove("256_allocator");
}


template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void test_manager1()
{
    std::cout << "Testing manager1; " << type_name<Index>()
              << std::endl;

    std::filesystem::remove("allocator");

    union alignas(PAGE_SIZE) header
    {
        typename Index::header h = {};
        std::byte bytes[PAGE_SIZE];
    };

    {
        Alloc<PAGE_SIZE> alloc("allocator", 0);
        alloc.pnew();

        header root {.h = create_index<Index, Alloc, PAGE_SIZE>(
                         4, 4, 1, 2, &alloc)};

        Index::add_contact(&root.h, {0, 2, 0}, &alloc);
        Index::add_contact(&root.h, {0, 2, 1}, &alloc);
        Index::add_contact(&root.h, {1, 1, 1}, &alloc);
        Index::print(root.h, alloc);

        alloc.pwrite(1, root.bytes);
    }

    {
        Alloc<PAGE_SIZE> alloc("allocator", 0);
        header root;
        alloc.pread(1, root.bytes);
        Index::print(root.h, alloc);
        Index::reconstruct_journey(root.h, 2, 0, {1, 3}, alloc);
    }

    std::filesystem::remove("allocator");
}
