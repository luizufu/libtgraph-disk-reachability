#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libtgraph-disk-reachability/ttc-compressed.hxx>
#include <libtgraph-disk-reachability/ttc-compressed2.hxx>
#include <libtgraph-disk-reachability/ttc-compressed3.hxx>
#include <libtgraph-disk-reachability/ttc-expanded-bycol.hxx>
#include <libtgraph-disk-reachability/ttc-expanded-filtered.hxx>
#include <libtgraph-disk-reachability/ttc-expanded.hxx>
#include <libtgraph-disk-reachability/ttc-fenwick.hxx>
#include <libtgraph-disk-reachability/ttc-trees.hxx>
#include <libtgraph-disk-reachability/version.hxx>

#include <unordered_set>
#include <vector>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <cassert>
#include <cmath>

const char simple16[] = "simple16";

using edge = std::pair<uint32_t, uint32_t>;

template<typename Index1, typename Index2,
         template<size_t> typename Alloc1,
         template<size_t> typename Alloc2, size_t PAGE_SIZE>
void compare(uint32_t n, uint32_t tau);

auto main() -> int
{
    using namespace tgraph_disk_reachability;

    // 73 259181 150126
    std::cout << std::endl;
    compare<ttc_expanded_filtered<disk::allocator, 4096>, // 1
            ttc_expanded<disk::allocator, 4096>,          // 2
            disk::allocator, disk::allocator, 4096>(1024, 8);
    return 0;
}

static unsigned int SEED = {};

auto generator() -> std::mt19937&
{
    static std::random_device r;
    /* static std::seed_seq seed {  }; */
    static bool first = true;

    if(first)
    {
        SEED = r();
        // SEED = 1348416780;
        /* std::cout << SEED << std::endl; */
        first = false;
    }
    /* 8 32, 142 2784234245 */

    static std::mt19937 gen(SEED);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto generate_interval(uint32_t tau)
    -> tgraph_disk_reachability::interval
{
    std::uniform_int_distribution<uint32_t> tdist(0, tau - 1);

    tgraph_disk_reachability::interval i = {tdist(generator()),
                                            tdist(generator())};

    if(i.right < i.left)
    {
        std::swap(i.left, i.right);
    }
    else if(i.left == i.right)
    {
        i.right += 1;
    }

    return i;
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<std::pair<edge, tgraph_disk_reachability::interval>>
{
    std::vector<std::pair<edge, tgraph_disk_reachability::interval>>
        contacts;

    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(uint32_t t1 = 1; t1 < t; ++t1)
    {
        for(uint32_t u = 0; u < n; ++u)
        {
            for(uint32_t v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        contacts.push_back(
                            {{u, v}, {g[u * n + v], t1}});
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                contacts.push_back({{u, v}, {g[u * n + v], t}});
            }
        }
    }

    return contacts;
}

auto make_contact(uint32_t n, uint32_t tau)
    -> tgraph_disk_reachability::contact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }

    uint32_t t = next_probability() * tau;
    if(t == tau)
    {
        --t;
    }

    return {u, v, t};
}

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto create_index(uint32_t n, uint32_t tau, uint32_t delta, uint32_t z,
                  Alloc<PAGE_SIZE>* alloc)
{
    if constexpr(
        std::is_same_v<
            Index,
            tgraph_disk_reachability::ttc_compressed<
                simple16, Alloc,
                PAGE_SIZE>> || std::is_same_v<Index, tgraph_disk_reachability::ttc_compressed2<Alloc, PAGE_SIZE>> || std::is_same_v<Index, tgraph_disk_reachability::ttc_compressed3<Alloc, PAGE_SIZE>>)
    {
        return Index::create(n, tau, delta, z, alloc);
    }
    else
    {
        return Index::create(n, tau, delta, alloc);
    }
}

template<typename Index1, typename Index2,
         template<size_t> typename Alloc1,
         template<size_t> typename Alloc2, size_t PAGE_SIZE>
void compare(uint32_t n, uint32_t tau)
{
    std::uniform_int_distribution<uint32_t> vdist(0, n - 1);
    auto icontacts = make_emeg(n, tau, 0.01, 0.3);
    std::vector<tgraph_disk_reachability::contact> contacts;
    for(const auto& [e, interv]: icontacts)
    {
        for(uint32_t t = interv.left; t < interv.right; ++t)
        {
            contacts.push_back({e.first, e.second, t});
        }
    }
    std::cout << n << " " << tau << " " << contacts.size() << std::endl;

    std::shuffle(contacts.begin(), contacts.end(), generator());

    auto now = []()
    {
        return std::chrono::high_resolution_clock::now();
    };

    auto diffnow = [&now](const auto& start)
    {
        std::chrono::duration<double> diff = now() - start;
        return diff.count();
    };

    std::filesystem::remove("index1");
    std::filesystem::remove("index2");

    Alloc1<PAGE_SIZE> alloc1("index1", 1);

    std::cout << std::fixed;
    std::cout << std::setprecision(2);
    auto h1 = create_index<Index1, Alloc1, PAGE_SIZE>(
        n, tau, 1, std::sqrt(tau), &alloc1);
    auto start = now();
    for(uint32_t i = 0; i < contacts.size(); ++i)
    {
        Index1::add_contact(&h1, contacts[i], &alloc1);
        std::cout << '\r' << std::setw(6)
                  << (i / static_cast<double>(contacts.size()) * 100)
                  << "%" << std::flush;
    }
    auto tend1 = diffnow(start);

    std::cout << "\rtime = " << tend1 << ", "
              << "seq_acc = " << alloc1.sequential_accesses() << ", "
              << "seq_wri = " << alloc1.sequential_writes() << ", "
              << "ran_acc = " << alloc1.random_accesses() << ", "
              << "ran_wri = " << alloc1.random_writes() << ", "
              << "tot_acc = "
              << alloc1.sequential_accesses() + alloc1.random_accesses()
              << ", "
              << "tot_wri = "
              << alloc1.sequential_writes() + alloc1.random_writes()
              << ", "
              // << "opt_size = " << Index1::space(h1, alloc1)
              << std::endl;

    Alloc2<PAGE_SIZE> alloc2("index2", 1);

    auto h2 = create_index<Index2, Alloc2, PAGE_SIZE>(
        n, tau, 1, std::sqrt(tau), &alloc2);
    start = now();
    for(uint32_t i = 0; i < contacts.size(); ++i)
    {
        Index2::add_contact(&h2, contacts[i], &alloc2);
        std::cout << '\r' << std::setw(6)
                  << (i / static_cast<double>(contacts.size()) * 100)
                  << "%" << std::flush;
    }
    auto tend2 = diffnow(start);

    std::cout << "\rtime = " << tend2 << ", "
              << "seq_acc = " << alloc2.sequential_accesses() << ", "
              << "seq_wri = " << alloc2.sequential_writes() << ", "
              << "ran_acc = " << alloc2.random_accesses() << ", "
              << "ran_wri = " << alloc2.random_writes() << ", "
              << "tot_acc = "
              << alloc2.sequential_accesses() + alloc2.random_accesses()
              << ", "
              << "tot_wri = "
              << alloc2.sequential_writes() + alloc2.random_writes()
              << ", "
              // << "opt_size = " << Index2::space(h2, alloc2)
              << std::endl;

    std::vector<tgraph_disk_reachability::contact> rcontacts;
    for(uint32_t i = 0; i < 1000; ++i)
    {
        rcontacts.push_back(make_contact(n, tau));
    }

    start = now();
    for(const auto& c: rcontacts)
    {
        Index1::add_contact(&h1, c, &alloc1);
    }
    tend1 = diffnow(start);

    start = now();
    for(const auto& c: rcontacts)
    {
        Index2::add_contact(&h2, c, &alloc2);
    }
    tend2 = diffnow(start);

    std::cout << "time = " << (tend1 / 10) << std::endl;
    std::cout << "time = " << (tend2 / 10) << std::endl;

    std::filesystem::remove("index1");
    std::filesystem::remove("index2");
}
