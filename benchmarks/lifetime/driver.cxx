#include <array>

#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libtgraph-disk-reachability/ttc-expanded.hxx>
#include <libtgraph-disk-reachability/ttc-compressed.hxx>
#include <libtgraph-disk-reachability/ttc-trees.hxx>

#include <vector>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <chrono>

// const std::string INDEX_DIR = "/hdd/luiz/experiments/lifetime/";
const std::string INDEX_DIR = "/home/luiz/";

using graph_disk_structures::adj_matrix;
using tgraph_disk_reachability::contact;
using tgraph_disk_reachability::interval;
using tgraph_disk_reachability::ttc_expanded;
using tgraph_disk_reachability::ttc_trees;

using icontact = std::tuple<uint32_t, uint32_t, interval>;
using tclock =
    std::chrono::time_point<std::chrono::high_resolution_clock>;

struct configuration
{
    std::string prefix;
    uint32_t n = 0;
    uint32_t tau = 0;
    bool sort = false;
    std::vector<uint32_t> deltas = {};
};

struct state
{
    double time = 0;
    uint64_t seq_acc = 0;
    uint64_t ran_acc = 0;
    uint64_t hits = 0;
    tclock clock;

    auto operator+=(const state& other) -> state&
    {
        time += other.time;
        seq_acc += other.seq_acc;
        ran_acc += other.ran_acc;
        hits += other.hits;
        return *this;
    }
};

auto make_complete(uint32_t n, uint32_t t) -> std::vector<contact>;
auto make_query(uint32_t n, uint32_t tau) -> icontact;
auto generator() -> std::mt19937&;

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
void run(const configuration& config)
{
    std::string str =
        std::is_same_v<Index, ttc_trees<adj_matrix, Alloc, 4096>>
            ? "trees"
        : std::is_same_v<Index, ttc_expanded<Alloc, 4096>> ? "expanded"
                                                           : "unknown";

    std::string index_file = INDEX_DIR + str;
    std::filesystem::remove(index_file);
    Alloc<PAGE_SIZE> alloc(index_file.c_str(), 0);

    auto now = [&alloc]() -> state
    {
        return {
            .time = 0,
            .seq_acc = alloc.sequential_accesses(),
            .ran_acc = alloc.random_accesses(),
            .hits = alloc.cache().hits(),
            .clock = std::chrono::high_resolution_clock::now(),
        };
    };

    auto diffnow = [&now](const auto& start) -> state
    {
        auto end = now();
        std::chrono::duration<double> diff = end.clock - start.clock;

        return {
            .time = diff.count(),
            .seq_acc = end.seq_acc - start.seq_acc,
            .ran_acc = end.ran_acc - start.ran_acc,
            .hits = end.hits - start.hits,
        };
    };

    auto contacts = make_complete(config.n, config.tau);

    if(config.sort)
    {
        std::sort(contacts.begin(), contacts.end(),
                  [](const auto& c1, const auto& c2)
                  {
                      return std::make_tuple(c1.t, c1.u, c1.v)
                             < std::make_tuple(c2.t, c2.u, c2.v);
                  });
    }
    else
    {
        std::shuffle(contacts.begin(), contacts.end(), generator());
    }

    uint64_t print_after = config.n * config.n * config.tau * 0.001;

    for(uint32_t delta: config.deltas)
    {
        alloc.clear();
        auto h = Index::create(config.n, config.tau, delta, &alloc);

        uint64_t c = 0;
        state op1 = {};
        state op2 = {};
        state op3 = {};

        for(auto contact: contacts)
        {
            {
                auto start = now();
                Index::add_contact(&h, contact, &alloc);
                op1 += diffnow(start);
            }

            // {
            //     auto [u, v, interv] = make_query(config.n,
            //     config.tau); auto start = now();
            //     dont_optimize(Index::can_reach(h, u, v, interv,
            //     alloc)); op2 += diffnow(start);
            // }

            // {
            //     auto [u, v, interv] = make_query(config.n,
            //     config.tau); auto start = now(); dont_optimize(
            //         Index::reconstruct_journey(h, u, v, interv,
            //         alloc));
            //     op3 += diffnow(start);
            // }

            ++c;

            if(c % print_after == 0)
            {
                uint64_t seq_wri = alloc.sequential_writes();
                uint64_t ran_wri = alloc.random_writes();

                std::cout
                    << config.prefix << ";" // prefix
                    << str << ";"           // structure
                    << config.n << ";"      // n
                    << config.tau << ";"    // tau
                    << delta << ";"         // delta
                    << c << ";"             // contacts
                    << op1.time << ";"      // add_contact time (s)
                    << op1.seq_acc
                    << ";" // add_contact sequential reads
                    << op1.ran_acc << ";" // add_contact random reads
                    << seq_wri << ";"     // sequetial disk writes
                    << ran_wri << ";"     // random disk writes
                    << op1.hits
                    // << op2.time << ";"    // can_reach time (s)
                    // << op2.seq_acc << ";" // can_reach sequential
                    // reads
                    // << op2.ran_acc << ";" // can_reach random reads
                    // << op2.hits << ";"    // can_reach cache hits
                    // << op3.time << ";" // reconstruct_journey time
                    // (s)
                    // << op3.seq_acc
                    // << ";" // reconstruct_journey seq reads
                    // << op3.ran_acc
                    // << ";"      // reconstruct_journey ran reads
                    // << op3.hits // reconstruct_journey cache hits
                    << std::endl;
            }
        }

        Index::destroy(&h, &alloc);
    }

    std::filesystem::remove(index_file);
}

auto main() -> int
{
    std::vector<configuration> configs = {
        {.n = 100, .tau = 10, .sort = true, .deltas = {1}},
        {.n = 100, .tau = 100, .sort = true, .deltas = {1}},
        {.n = 100, .tau = 1000, .sort = true, .deltas = {1}},
        {.n = 100, .tau = 10000, .sort = true, .deltas = {1}},
    };

    run<ttc_expanded<disk::allocator, 4096>, disk::allocator, 4096>(
        {.n = 100, .tau = 10000, .sort = true, .deltas = {1}});

    // for(uint32_t i = 1; i <= 10; ++i)
    // {
    //     for(auto config: configs)
    //     {
    //         config.prefix = std::to_string(i) + "-sorted";
    //         run<ttc_expanded<disk::allocator, 4096>, disk::allocator,
    //             4096>(config);
    //     }
    // }

    // for(uint32_t i = 1; i <= 10; ++i)
    // {
    //     for(auto config: configs)
    //     {
    //         config.prefix = std::to_string(i) + "-sorted";
    //         run<ttc_trees<adj_matrix, disk::allocator, 4096>,
    //             disk::allocator, 4096>(config);
    //     }
    // }
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto make_complete(uint32_t n, uint32_t tau) -> std::vector<contact>
{
    std::vector<contact> contacts;

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            for(uint32_t t = 0; t < tau; ++t)
            {
                contacts.push_back({u, v, t});
            }
        }
    }

    return contacts;
}

inline auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}
