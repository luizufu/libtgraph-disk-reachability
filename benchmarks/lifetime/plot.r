#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(scales)
library(grid)
library(matrixStats)
library(gridExtra)
library(tikzDevice)


theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1.2)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(size = rel(1)),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            # panel.spacing.y = unit(-1, "lines"),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.text = element_text(margin = margin(r = 10)),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(0, 3, 0, 2), "mm"),
            strip.background = element_blank(),
            strip.text = element_text(face = "bold", hjust = 0, vjust = 0.3)
        ))
}

scientific_10 <- function(x) {
    parse(
        text = gsub(
            "e\\+*", " %*% 10^",
            scales::scientific_format()(x)
        )
    )
}

input_file <- "benchmarks/lifetime/lifetime-results.csv"
output_dir <- "benchmarks/lifetime/output/"

d <- read.csv(input_file, sep = ";", header = FALSE)
colnames(d) <- c(
    "structure",
    "n",
    "tau",
    "delta",
    "contacts",
    "op1_time",
    "op1_seq_acc",
    "op1_ran_acc",
    "op1_seq_wri",
    "op1_ran_wri",
    "op1_hits",
    "op2_time",
    "op2_seq_acc",
    "op2_ran_acc",
    "op2_hits",
    "op3_time",
    "op3_seq_acc",
    "op3_ran_acc",
    "op3_hits"
)
d <- do.call(
    data.frame,
    aggregate(
        . ~ structure + n + tau + delta + contacts,
        d,
        function(x) c(mean = mean(x), sd = sd(x))
    )
)

d[is.na(d)] <- 0
d$tau <- factor(d$tau, labels = paste0(
    "n == 100 ~~~ tau == ", unique(d$tau)
))
d$structure <- factor(d$structure,
    labels = c("Array-Based", "Tree-Based")
)

dir.create(output_dir, showWarnings = FALSE)
tikz(file = paste0(output_dir, "/lifetime-time.tex"), width = 11, height = 7)

ggplot(
    d,
    aes(x = contacts, y = op1_time.mean, color = factor(structure))
) +
    scale_x_continuous(labels = scientific_10) +
    scale_y_continuous(labels = scientific_10) +
    geom_errorbar(aes(
        ymin = op1_time.mean - op1_time.sd,
        ymax = op1_time.mean + op1_time.sd
    ), width = 10, position = position_dodge(10), linetype = "solid") +
    geom_line(size = 1) +
    labs(x = "Number of contacts", y = "Wall-Clock Time (s)") +
    facet_wrap(
        vars(tau),
        ncol = 2,
        scales = "free",
        labeller = label_parsed
    ) +
    guides(color = guide_legend(title = "Data Structure:")) +
    theme_Publication()

# dev.off()


# ran_acc <- ggplot(
#     d,
#     aes(x = contacts, y = op1_ran_acc.mean, color = factor(structure))
# ) +
#     scale_x_continuous(labels = scientific_10) +
#     scale_y_continuous(labels = scientific_10) +
#     geom_errorbar(aes(
#         ymin = op1_ran_acc.mean - op1_seq_acc.sd,
#         ymax = op1_ran_acc.mean + op1_seq_acc.sd
#     ), width = 10, position = position_dodge(10), linetype = "solid") +
#     geom_line(size = 1) +
#     labs(x = "Number of contacts", y = "Number of random accesses") +
#     facet_wrap(
#         vars(tau),
#         ncol = 1,
#         scales = "free",
#         labeller = label_parsed
#     ) +
#     guides(color = guide_legend(title = "Data Structure:")) +
#     theme_Publication()

# dir.create(output_dir, showWarnings = FALSE)
# ggsave(paste0(output_dir, "/lifetime-time.pdf"), time, width = 11)
# ggsave(paste0(output_dir, "/lifetime-ran-acc.pdf"), ran_acc, height = 10)
