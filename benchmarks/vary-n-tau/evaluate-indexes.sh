#!/bin/bash

outdir='/mnt/luiz/experiments/vary-parameters'
exec='./benchmarks/vary-n-tau/driver'
outfile='results2.csv'
rm -f $outfile
rm -f $outdir/*-tmp

i=1

# for i in `seq 2 100`; do
    for file in `ls $outdir | sort -t'-' -n -k{2,3,4}`; do
        orig="$outdir/$file"
        copy="$outdir/$file-tmp"
        index='trees'
        if [[ $file == *"expanded"* ]]; then
            index='expanded'
        fi

        cp $orig $copy
        echo "$i: $copy"
        res=`$exec -o evaluate -i $index $copy`
        echo "$index,$i,$res" >> $outfile
        rm $copy
    done
# done
