#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)

d <- read.csv("results2.csv", header = FALSE)
colnames(d) <- c(
    "index",
    "iteration",
    "n",
    "tau",
    "delta",
    "op1_time",
    "op1_seq_acc",
    "op1_ran_acc",
    "op1_seq_wri",
    "op1_ran_wri",
    "op1_hits",
    "op2_time",
    "op2_seq_acc",
    "op2_ran_acc",
    "op2_hits",
    "op3_time",
    "op3_seq_acc",
    "op3_ran_acc",
    "op3_hits"
)

d$op1_total_acc <- d$op1_seq_acc + d$op1_ran_acc

op <- "time"
# for (op in c("time", "seq_acc", "ran_acc", "seq_wri", "ran_wri")) {
ids <- c("index", "n", "tau")
rest <- colnames(d)[grepl(op, colnames(d))]
tmp <- do.call(
    data.frame,
    aggregate(value ~ .,
        melt(d[, c(ids, rest)], id.vars = ids, na.rm = TRUE),
        FUN = function(x) c(mean = mean(x), sd = sd(x))
    )
)

tmp2 <- tmp[tmp$variable == "op1_time", ]
# tmp2 <- tmp2[tmp2$index == "expanded", ]

ggplot(tmp2, aes(x = tau, y = value.mean, color = index)) +
    geom_line() +
    # geom_errorbar(aes(
    #     ymin = value.mean - value.sd,
    #     ymax = value.mean + value.sd
    # ), width = 10, position = position_dodge(10), linetype = "solid") +
    facet_grid(rows = vars(n), scales = "free")



# }
