#!/bin/bash

outdir='/mnt/luiz/experiments/vary-n-tau'
outfile="$outdir/creation-results.csv"
exec='/home/luiz/Documents/codes/cpp/libtgraph-disk-reachability/benchmarks/vary-n-tau/driver'
indexes=('trees' 'expanded')

mkdir -p $outdir
cd $outdir

for i in `seq 1 10`; do
    for pn in `seq 3 9`; do
        for ptau in `seq 3 9`; do
            for index in ${indexes[@]}; do
                n=$((2**$pn))
                tau=$((2**$ptau))
                file="$index-$n-$tau-$i"

                res=`$exec -o create -i $index -n $n -t $tau $file`
                echo "$index,$res" >> $outfile
            done
        done
    done
done
