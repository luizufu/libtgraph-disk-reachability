#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)

d <- read.csv("benchmarks/vary-n-tau/creation-results.csv", header = FALSE)
colnames(d) <- c(
    "structure",
    "n",
    "tau",
    "delta",
    "contacts",
    "time",
    "seq_acc",
    "ran_acc",
    "seq_wri",
    "ran_wri",
    "hits"
)

# d$total_acc <- d$seq_acc + d$ran_acc
# d$total_wri <- d$seq_wri + d$ran_wri

op <- "time"
# for (op in c("time", "seq_acc", "ran_acc", "seq_wri", "ran_wri")) {
ids <- c("structure", "n", "tau")
rest <- colnames(d)[grepl(op, colnames(d))]
tmp <- do.call(
    data.frame,
    aggregate(value ~ .,
        melt(d[, c(ids, rest)], id.vars = ids, na.rm = TRUE),
        FUN = function(x) c(mean = mean(x), sd = sd(x))
    )
)
tmp[is.na(tmp)] <- 0
tmp <- tmp[tmp$structure != "expanded", ]
tmp <- tmp[tmp$n >= 32 & tmp$n <= 256, ]

tmp$n <- factor(tmp$n,
    labels = paste0(expression(n), " = ", unique(tmp$n))
)
tmp$structure <- factor(tmp$structure,
    labels = c("Array-Based", "Tree-Based")
)


g <- ggplot(tmp, aes(x = tau, y = value.mean, color = structure)) +
    geom_line() +
    geom_errorbar(aes(
        ymin = value.mean - value.sd,
        ymax = value.mean + value.sd
    ), width = 10, position = position_dodge(10), linetype = "solid") +
    facet_grid(rows = vars(n), scales = "free") +
    xlab(expression(tau)) +
    ylab("Wall-Clock Time (s)") +
    guides(color = guide_legend(title = "Data Structure:"))
# }

ggsave("vary-n-tau-time.pdf", g, height = 5)
