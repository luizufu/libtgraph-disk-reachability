#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libtgraph-disk-reachability/ttc-expanded.hxx>
#include <libtgraph-disk-reachability/ttc-trees.hxx>

#include <benchmarks/vary-n-tau/cxxopts.hxx>
#include <filesystem>
#include <iostream>
#include <random>
#include <thread>

using namespace cxxopts;
using namespace disk;
using namespace tgraph_disk_reachability;
using namespace graph_disk_structures;

const uint32_t PAGE_SIZE = 4096;
const uint32_t N_TESTS = 10;
const float BIRTH_RATE = .05F;
const float DEATH_RATE = .3F;

template<typename Index, template<size_t> typename Alloc>
void create(const std::string& filename, uint32_t n, uint32_t tau,
            uint32_t delta);
template<typename Index, template<size_t> typename Alloc>
void evaluate(const std::string& filename);

auto main(int argc, char* argv[]) -> int
{
    Options options("tgraph-disk-manager", "Temporal Graph Disk Manager");
    options.positional_help("filename").show_positional_help();
    // clang-format off
    options.add_options()
        ("o", "create or evaluate", value<std::string>())
        ("i", "trees or expanded", value<std::string>())
        ("a", "non-buffered or buffered", value<std::string>()->default_value("non-buffered"))
        ("n", "number of vertices", value<uint32_t>())
        ("t", "number of timestamps", value<uint32_t>())
        ("d", "global edge latency", value<uint32_t>()->default_value("1"))
        ("filename", "index filename", value<std::string>())
        ("h,help", "Print usage");
    // clang-format on
    options.parse_positional("filename");
    auto result = options.parse(argc, argv);

    if(result.count("help") > 0)
    {
        std::cout << options.help() << std::endl;
        return 0;
    }

    auto op = result["o"].as<std::string>();
    auto i = result["filename"].as<std::string>();
    auto index = result["i"].as<std::string>();
    auto alloc = result["a"].as<std::string>();

    if(op == "create")
    {
        auto n = result["n"].as<uint32_t>();
        auto tau = result["t"].as<uint32_t>();
        auto delta = result["d"].as<uint32_t>();

        if(index == "expanded")
        {
            if(alloc == "non-buffered")
            {
                create<ttc_expanded<disk::allocator, PAGE_SIZE>,
                       disk::allocator>(i, n, tau, delta);
            }
            else
            {
                create<ttc_expanded<disk::buffered_allocator, PAGE_SIZE>,
                       disk::buffered_allocator>(i, n, tau, delta);
            }
        }
        else if(index == "trees")
        {
            if(alloc == "non-buffered")
            {
                create<ttc_trees<adj_matrix, disk::allocator, PAGE_SIZE>,
                       disk::allocator>(i, n, tau, delta);
            }
            else
            {
                create<
                    ttc_trees<adj_matrix, disk::buffered_allocator, PAGE_SIZE>,
                    disk::buffered_allocator>(i, n, tau, delta);
            }
        }
        else
        {
            std::cerr << "invalid -i option" << std::endl;
        }
    }
    else if(op == "evaluate")
    {
        if(index == "expanded")
        {
            if(alloc == "non-buffered")
            {
                evaluate<ttc_expanded<disk::allocator, PAGE_SIZE>,
                         disk::allocator>(i);
            }
            else
            {
                evaluate<ttc_expanded<disk::buffered_allocator, PAGE_SIZE>,
                         disk::buffered_allocator>(i);
            }
        }
        else if(index == "trees")
        {
            if(alloc == "non-buffered")
            {
                evaluate<ttc_trees<adj_matrix, disk::allocator, PAGE_SIZE>,
                         disk::allocator>(i);
            }
            else
            {
                evaluate<
                    ttc_trees<adj_matrix, disk::buffered_allocator, PAGE_SIZE>,
                    disk::buffered_allocator>(i);
            }
        }
        else
        {
            std::cerr << "invalid -i option" << std::endl;
        }
    }
    else
    {
        std::cerr << "invalid -o option" << std::endl;
    }
}

template<typename Index>
union alignas(PAGE_SIZE) root_page
{
    typename Index::header h = {};
    std::byte bytes[PAGE_SIZE];
};

using tclock = std::chrono::time_point<std::chrono::high_resolution_clock>;

struct state
{
    double time = 0;
    uint64_t seq_acc = 0;
    uint64_t ran_acc = 0;
    uint64_t hits = 0;
    tclock clock;

    auto operator+=(const state& other) -> state&
    {
        time += other.time;
        seq_acc += other.seq_acc;
        ran_acc += other.ran_acc;
        hits += other.hits;
        return *this;
    }

    auto operator/=(uint64_t d) -> state&
    {
        time /= static_cast<double>(d);
        seq_acc /= d;
        ran_acc /= d;
        hits /= d;
        return *this;
    }
};

using icontact = std::tuple<uint32_t, uint32_t, interval>;
auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<icontact>;
auto make_query(uint32_t n, uint32_t tau) -> icontact;
auto make_contact(uint32_t n, uint32_t tau) -> contact;
auto generator() -> std::mt19937&;
void dont_optimize(auto&& x);

template<typename Index, template<size_t> typename Alloc>
void create(const std::string& filename, uint32_t n, uint32_t tau,
            uint32_t delta)
{
    std::filesystem::remove(filename);
    Alloc<PAGE_SIZE> alloc(filename.c_str(), 0);

    auto now = [&alloc]() -> state
    {
        return {
            .time = 0,
            .seq_acc = alloc.sequential_accesses(),
            .ran_acc = alloc.random_accesses(),
            .hits = alloc.cache().hits(),
            .clock = std::chrono::high_resolution_clock::now(),
        };
    };

    auto diffnow = [&now](const auto& start) -> state
    {
        auto end = now();
        std::chrono::duration<double> diff = end.clock - start.clock;

        return {
            .time = diff.count(),
            .seq_acc = end.seq_acc - start.seq_acc,
            .ran_acc = end.ran_acc - start.ran_acc,
            .hits = end.hits - start.hits,
        };
    };
    auto icontacts = make_emeg(n, tau, BIRTH_RATE, DEATH_RATE);
    std::vector<contact> contacts;
    for(const auto& [u, v, interv]: icontacts)
    {
        for(uint32_t t = interv.left; t < interv.right; ++t)
        {
            contacts.push_back(contact {u, v, t});
        }
    }

    alloc.pnew();
    root_page<Index> root = {.h = Index::create(n, tau, delta, &alloc)};

    uint64_t seq_wri_local = alloc.sequential_writes();
    uint64_t ran_wri_local = alloc.random_writes();
    auto start = now();
    for(const auto& contact: contacts)
    {
        Index::add_contact(&root.h, contact, &alloc);
    }
    state op1 = diffnow(start);
    uint64_t seq_wri = alloc.sequential_writes() - seq_wri_local;
    uint32_t ran_wri = alloc.random_writes() - ran_wri_local;

    alloc.pwrite(1, root.bytes);

    std::cout << root.h.n << ','        // number of vertices
              << root.h.tau << ','      // number of timestamps
              << root.h.delta << ','    // duration to traverse a contact
              << contacts.size() << ',' // number of contacts
              << op1.time << ','        // add_contact time (s)
              << op1.seq_acc << ','     // add_contact sequential reads
              << op1.ran_acc << ','     // add _contact random reads
              << seq_wri << ','         // sequetial disk writes
              << ran_wri << ','         // random disk writes
              << op1.hits               // add_contact cache hits
              << std::endl;
}

template<typename Index, template<size_t> typename Alloc>
void evaluate(const std::string& filename)
{
    Alloc<PAGE_SIZE> alloc(filename.c_str(), 0);

    auto now = [&alloc]() -> state
    {
        return {
            .time = 0,
            .seq_acc = alloc.sequential_accesses(),
            .ran_acc = alloc.random_accesses(),
            .hits = alloc.cache().hits(),
            .clock = std::chrono::high_resolution_clock::now(),
        };
    };

    auto diffnow = [&now](const auto& start) -> state
    {
        auto end = now();
        std::chrono::duration<double> diff = end.clock - start.clock;

        return {
            .time = diff.count(),
            .seq_acc = end.seq_acc - start.seq_acc,
            .ran_acc = end.ran_acc - start.ran_acc,
            .hits = end.hits - start.hits,
        };
    };

    root_page<Index> root = {};
    alloc.pread(1, root.bytes);

    state op1 = {};
    state op2 = {};
    state op3 = {};
    uint64_t seq_wri = 0;
    uint64_t ran_wri = 0;

    for(uint32_t t = 0; t < N_TESTS; ++t)
    {
        {
            auto contact = make_contact(root.h.n, root.h.tau);
            uint64_t seq_wri_local = alloc.sequential_writes();
            uint64_t ran_wri_local = alloc.random_writes();
            auto start = now();
            Index::add_contact(&root.h, contact, &alloc);
            op1 += diffnow(start);
            seq_wri += alloc.sequential_writes() - seq_wri_local;
            ran_wri = alloc.random_writes() - ran_wri_local;
        }

        auto [u, v, interv] = make_query(root.h.n, root.h.tau);

        {
            auto start = now();
            dont_optimize(Index::can_reach(root.h, u, v, interv, alloc));
            op2 += diffnow(start);
        }

        {
            auto start = now();
            dont_optimize(
                Index::reconstruct_journey(root.h, u, v, interv, alloc));
            op3 += diffnow(start);
        }
    }

    op1 /= N_TESTS;
    op2 /= N_TESTS;
    op3 /= N_TESTS;
    seq_wri /= N_TESTS;
    ran_wri /= N_TESTS;

    std::cout << root.h.n << ','     // number of vertices
              << root.h.tau << ','   // number of timestamps
              << root.h.delta << ',' // duration to traverse a contact
              << op1.time << ','     // add_contact time (s)
              << op1.seq_acc << ','  // add_contact sequential reads
              << op1.ran_acc << ','  // add _contact random reads
              << seq_wri << ','      // sequetial disk writes
              << ran_wri << ','      // random disk writes
              << op1.hits << ','     // add_contact cache hits
              << op2.time << ','     // can_reach time (s)
              << op2.seq_acc << ','  // can_reach sequential reads
              << op2.ran_acc << ','  // can_reach random reads
              << op2.hits << ','     // can_reach cache hits
              << op3.time << ','     // reconstruct_journey time (s)
              << op3.seq_acc << ','  // reconstruct_journey seq reads
              << op3.ran_acc << ','  // reconstruct_journey ran reads
              << op3.hits            // reconstruct_journey cache hits
              << std::endl;
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<icontact>
{
    std::vector<icontact> icontacts;

    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(uint32_t t1 = 1; t1 < t; ++t1)
    {
        for(uint32_t u = 0; u < n; ++u)
        {
            for(uint32_t v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        icontacts.emplace_back(u, v,
                                               interval {g[u * n + v], t1});
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                icontacts.emplace_back(u, v, interval {g[u * n + v], t});
            }
        }
    }

    return icontacts;
}

auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}

auto make_contact(uint32_t n, uint32_t tau) -> contact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }

    uint32_t t = next_probability() * tau;
    if(t == tau)
    {
        --t;
    }

    return {u, v, t};
}

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};
