#include <libdisk/allocator.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libtgraph-disk-reachability/ttc-compressed.hxx>
#include <libtgraph-disk-reachability/ttc-compressed2-filtered.hxx>
#include <libtgraph-disk-reachability/ttc-compressed2.hxx>
#include <libtgraph-disk-reachability/ttc-compressed3.hxx>
#include <libtgraph-disk-reachability/ttc-compressed4.hxx>
#include <libtgraph-disk-reachability/ttc-compressed6.hxx>
#include <libtgraph-disk-reachability/ttc-compressed7.hxx>
#include <libtgraph-disk-reachability/ttc-expanded-bycol.hxx>
#include <libtgraph-disk-reachability/ttc-expanded-filtered.hxx>
#include <libtgraph-disk-reachability/ttc-expanded.hxx>
#include <libtgraph-disk-reachability/ttc-trees.hxx>

#include <benchmarks/lifetime-dataset/cxxopts.hxx>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <unistd.h>
#include <chrono>
#include <cmath>
#include <csignal>

using namespace cxxopts;
using graph_disk_structures::adj_matrix;
using tgraph_disk_reachability::interval;
using tgraph_disk_reachability::ttc_compressed;
using tgraph_disk_reachability::ttc_compressed2;
using tgraph_disk_reachability::ttc_compressed2_filtered;
using tgraph_disk_reachability::ttc_compressed3;
using tgraph_disk_reachability::ttc_compressed4;
using tgraph_disk_reachability::ttc_compressed6;
using tgraph_disk_reachability::ttc_compressed7;
using tgraph_disk_reachability::ttc_expanded;
using tgraph_disk_reachability::ttc_expanded_filtered;
using tgraph_disk_reachability::ttc_trees;

const char SIMPLE8B[] = "simple8b";
const char SIMPLE8B_RLE[] = "simple8b_rle";

const uint32_t PAGE_SIZE = 4096;
const uint32_t FIVE_MINUTES = 60 * 5;
const uint32_t FIVE_HOURS = 60 * 60 * 5;

using icontact = std::tuple<uint32_t, uint32_t, interval>;
using tclock =
    std::chrono::time_point<std::chrono::high_resolution_clock>;

template<typename Index, template<size_t> typename Alloc>
void run(const std::string& prefix, const std::string& index_filename,
         const std::string& dataset_filename,
         const std::string& index_type);

auto main(int argc, char* argv[]) -> int
{
    Options options("driver", "Temporal Graph Disk Lifetime Creation");
    options.positional_help("filename").show_positional_help();
    // clang-format off
    options.add_options()
        ("prefix", "execution prefix column", value<std::string>())
        ("index_type", "trees or expanded or expanded_filtered, or compressed[1|2|3]-simple8b(_rle)",
            value<std::string>())
        ("index_filename", "index filename", value<std::string>())
        ("dataset_filename", "dataset filename", value<std::string>())
        ("h,help", "Print usage");
    // clang-format on
    auto result = options.parse(argc, argv);

    if(result.count("help") > 0)
    {
        std::cout << options.help() << std::endl;
        return 0;
    }

    auto prefix = result["prefix"].as<std::string>();
    auto index_filename = result["index_filename"].as<std::string>();
    auto dataset_filename =
        result["dataset_filename"].as<std::string>();
    auto index_type = result["index_type"].as<std::string>();

    if(index_type == "expanded")
    {
        run<ttc_expanded<disk::allocator, PAGE_SIZE>, disk::allocator>(
            prefix, index_filename, dataset_filename, index_type);
    }
    else if(index_type == "expanded_filtered")
    {
        run<ttc_expanded_filtered<disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed1-simple8b")
    {
        run<ttc_compressed<SIMPLE8B, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed1-simple8b_rle")
    {
        run<ttc_compressed<SIMPLE8B_RLE, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed2-simple8b")
    {
        run<ttc_compressed2<SIMPLE8B, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed2-simple8b_rle")
    {
        run<ttc_compressed2<SIMPLE8B_RLE, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed2-filtered-simple8b_rle")
    {
        run<ttc_compressed2_filtered<SIMPLE8B_RLE, disk::allocator,
                                     PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed3-simple8b")
    {
        run<ttc_compressed3<SIMPLE8B, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed3-simple8b_rle")
    {
        run<ttc_compressed3<SIMPLE8B_RLE, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed4-simple8b")
    {
        run<ttc_compressed4<SIMPLE8B, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed6-simple8b")
    {
        run<ttc_compressed6<SIMPLE8B, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed6-simple8b_rle")
    {
        run<ttc_compressed6<SIMPLE8B_RLE, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    else if(index_type == "compressed7-simple8b")
    {
        run<ttc_compressed7<SIMPLE8B, disk::allocator, PAGE_SIZE>,
            disk::allocator>(prefix, index_filename, dataset_filename,
                             index_type);
    }
    // else if(index_type == "trees")
    // {
    //     run<ttc_trees<adj_matrix, disk::allocator, PAGE_SIZE>,
    //         disk::allocator>(prefix, index_filename,
    //         dataset_filename,
    //                          index_type);
    // }
    else
    {
        std::cerr << "invalid -i option" << std::endl;
    }
}

auto make_query(uint32_t n, uint32_t tau) -> icontact;

struct state
{
    double time = 0;
    uint64_t seq_acc = 0;
    uint64_t ran_acc = 0;
    uint64_t hits = 0;
    tclock clock;

    auto operator+=(const state& other) -> state&
    {
        time += other.time;
        seq_acc += other.seq_acc;
        ran_acc += other.ran_acc;
        hits += other.hits;
        return *this;
    }
};

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};

template<typename Index, template<size_t> typename Alloc,
         size_t PAGE_SIZE>
auto create_index(uint32_t n, uint32_t tau, uint32_t delta, uint32_t z,
                  Alloc<PAGE_SIZE>* alloc)
{
    using tgraph_disk_reachability::ttc_expanded;
    using tgraph_disk_reachability::ttc_expanded_filtered;
    using tgraph_disk_reachability::ttc_trees;
    if constexpr(
        std::is_same_v<
            Index,
            ttc_trees<
                adj_matrix, Alloc,
                PAGE_SIZE>> || std::is_same_v<Index, ttc_expanded<Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_expanded_filtered<Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_compressed3<SIMPLE8B_RLE, Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_compressed3<SIMPLE8B, Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_compressed4<SIMPLE8B, Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_compressed6<SIMPLE8B, Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_compressed6<SIMPLE8B_RLE, Alloc, PAGE_SIZE>> || std::is_same_v<Index, ttc_compressed7<SIMPLE8B, Alloc, PAGE_SIZE>>)
    {
        return Index::create(n, tau, delta, alloc);
    }
    else
    {
        return Index::create(n, tau, delta, z, alloc);
    }
}

template<typename Index, template<size_t> typename Alloc>
void run(const std::string& prefix, const std::string& index_filename,
         const std::string& dataset_filename,
         const std::string& index_type)
{
    std::string dataset_name =
        std::filesystem::path(dataset_filename).stem().string();

    std::filesystem::remove(index_filename);
    Alloc<PAGE_SIZE> alloc(index_filename.c_str(), 0);

    auto now = [&alloc]() -> state
    {
        return {
            .time = 0,
            .seq_acc = alloc.sequential_accesses(),
            .ran_acc = alloc.random_accesses(),
            .hits = alloc.cache().hits(),
            .clock = std::chrono::high_resolution_clock::now(),
        };
    };

    auto diffnow = [&now](const auto& start) -> state
    {
        auto end = now();
        std::chrono::duration<double> diff = end.clock - start.clock;

        return {
            .time = diff.count(),
            .seq_acc = end.seq_acc - start.seq_acc,
            .ran_acc = end.ran_acc - start.ran_acc,
            .hits = end.hits - start.hits,
        };
    };

    std::ifstream in(dataset_filename);

    uint32_t n = 0;
    uint32_t tau = 0;
    uint64_t c = 0;
    char comma = ',';

    in >> comma >> n >> tau >> c;

    uint32_t print_after =
        std::max(1U, static_cast<uint32_t>(c * 0.001));
    std::cerr << "creating" << std::endl;
    auto h = create_index<Index>(n, tau, 1, std::sqrt(tau), &alloc);

    state op1 = {};
    // state op2 = {};
    // state op3 = {};

    uint32_t u = 0;
    uint32_t v = 0;
    uint32_t t = 0;
    c = 0;

    std::signal(SIGALRM, SIG_DFL);
    while(in >> u >> comma >> v >> comma >> t)
    {
        {
            alarm(FIVE_MINUTES);
            auto start = now();
            Index::add_contact(&h, {u, v, t}, &alloc);
            op1 += diffnow(start);
            alarm(0);
        }

        ++c;

        if(c % print_after == 0)
        {
            uint64_t seq_wri = alloc.sequential_writes();
            uint64_t ran_wri = alloc.random_writes();

            std::cout << prefix << ","       // execution prefix
                      << index_type << ","   // structure
                      << dataset_name << "," // dataset name
                      << n << ","            // n
                      << tau << ","          // tau
                      << 1 << ","            // delta
                      << c << ","            // contacts
                      << op1.time << ","     // add_contact time (s)
                      << op1.seq_acc
                      << "," // add_contact sequential reads
                      << op1.ran_acc << "," // add_contact random reads
                      << seq_wri << ","     // sequential disk writes
                      << ran_wri << ","     // random disk writes
                      << op1.hits           // add_contact cache hits
                      << std::endl;
        }

        if(op1.time > FIVE_HOURS)
        {
            break;
        }
    }
    std::cerr << "space at the end: " << Index::space(h, alloc)
              << std::endl;
    if constexpr(std::is_same_v<Index, ttc_compressed7<SIMPLE8B, Alloc,
                                                       PAGE_SIZE>>)
    {
        Index::has_any_full_parent(h);
    }
    std::cerr << "destroying" << std::endl;
    Index::destroy(&h, &alloc);
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

inline auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}
