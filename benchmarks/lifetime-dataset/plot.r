#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(dplyr)
library(tidyr)
library(xtable)

input_file <- "benchmarks/lifetime-dataset/creation-results-fast.csv"
dataset_dir <- "~/Documents/datasets/temporal-graphs/dynamicnetworks/"
output_dir <- "benchmarks/lifetime-dataset/output/"

d <- read.csv(input_file, header = FALSE)
d <- d[, 1:13]

colnames(d) <- c(
  "prefix",
  "structure",
  "dataset",
  "n",
  "tau",
  "delta",
  "contacts",
  "time",
  "seq_acc",
  "ran_acc",
  "seq_wri",
  "ran_wri",
  "hits"
)

# d <- subset(d, structure == "compressed6-simple8b_rle" | structure == "compressed2-simple8b_rle" | structure == "expanded_filtered")
# d <- subset(d, dataset == "copresence-InVS13" | dataset == "ia-enron-employees")
d[is.na(d)] <- 0
# d$structure <- factor(d$structure, labels = c("Compressed Array", "Compressed Directory", "Uncompressed"))

datasets <- data.frame(
  dataset = unique(d$dataset),
  contacts = rep(0, length(unique(d$dataset)))
)

for (i in 1:nrow(datasets)) {
  datasets[i, "contacts"] <- read.table(
    paste0(dataset_dir, datasets[i, "dataset"], ".edges"),
    nrows = 1
  )[4]
}

make_table <- function() {
  sd_or_zero <- function(val) ifelse(is.na(sd(val)), 0, sd(val))
  table <- d %>%
    group_by(prefix, structure, dataset, n, tau) %>%
    filter(contacts == max(contacts)) %>%
    group_by(structure, dataset, n, tau) %>%
    summarise(
      time = paste0(
        format(round(mean(time), 2), nsmall = 2),
        " \\pm ",
        format(round(sd_or_zero(time), 2), nsmall = 2)
      ),
      .groups = "drop"
    ) %>%
    left_join(datasets)

  table$density <- NA
  for (i in 1:nrow(table)) {
    inserted_contacts <- max(d[
      d$structure == table[[i, "structure"]] &
        d$dataset == table[[i, "dataset"]], "contacts"
    ])

    if (inserted_contacts < 0.9 * table[[i, "contacts"]]) {
      table[i, "time"] <- "\\textrm{-}"
    }

    total <- (table[i, "n"] - 1)^2 * table[i, "tau"]
    table[i, "density"] <- format(round(table[i, "contacts"] / total, 2), nsmall = 2)
  }
  table[table$density == "0.00", "density"] <- "0.01"

  for (j in 3:ncol(table)) {
    table[, j] <- paste0("$", table %>% pull(j), "$")
  }

  table[table$structure == "expanded_filtered", "structure"] <- "Array-Based Uncompressed"
  table[table$structure == "trees", "structure"] <- "Tree-Based Uncompressed"
  table[table$structure == "compressed2-simple8b_rle", "structure"] <- "Array-Based Compressed"
  table[table$structure == "compressed6-simple8b_rle", "structure"] <- "Directory-Based Compressed"
  colnames(table)[which(names(table) == "tau")] <- "\\tau"

  table <- table %>%
    pivot_wider(
      names_from = c(structure),
      values_from = time
    )


  if ("Array-Based Uncompressed" %in% colnames(table)) {
    table[is.na(table[, "Array-Based Uncompressed"]), "Array-Based Uncompressed"] <- "$\\textrm{-}$"
  }

  if ("Tree-Based Uncompressed" %in% colnames(table)) {
    table[is.na(table[, "Tree-Based Uncompressed"]), "Tree-Based Uncompressed"] <- "$\\textrm{-}$"
  }

  if ("Array-Based Compressed" %in% colnames(table)) {
    table[is.na(table[, "Array-Based Compressed"]), "Array-Based Compressed"] <- "$\\textrm{-}$"
  }

  if ("Directory-Based Compressed" %in% colnames(table)) {
    table[is.na(table[, "Directory-Based Compressed"]), "Directory-Based Compressed"] <- "$\\textrm{-}$"
  }

  print(
    xtable(table),
    type = "latex", sanitize.text.function = identity,
    file = paste0(output_dir, "/lifetime-dataset-table.tex"),
    include.rownames = FALSE
  )
}

make_plot <- function() {
  table <- d %>%
    group_by(structure, dataset, contacts) %>%
    summarise(
      time_mean = mean(time),
      time_sd = sd(time),
      .groups = "drop"
    ) %>%
    slice(seq(1, n(), by = 20))

  table[table$structure == "expanded_filtered", "structure"] <- "Array-Based Uncompressed"
  table[table$structure == "trees", "structure"] <- "Tree-Based Uncompressed"
  table[table$structure == "compressed2-simple8b_rle", "structure"] <- "Array-Based Compressed"
  table[table$structure == "compressed6-simple8b_rle", "structure"] <- "Directory-Based Compressed"

  g <- ggplot(
    table,
    aes(x = contacts, y = time_mean, color = factor(structure))
  ) +
    geom_errorbar(aes(
      ymin = time_mean - time_sd,
      ymax = time_mean + time_sd
    ), width = 0.001) +
    geom_line(size = 1) +
    facet_wrap(vars(dataset), scales = "free", nrow = 1) +
    labs(x = "Number of contacts", y = "Wall-Clock Time (s)") +
    guides(color = guide_legend(title = "Data Structure:")) +
    theme(legend.position = "bottom", text = element_text(size=15))
    # theme(text = element_text(size=20),
    #     axis.text.x = element_text(angle=90, hjust=1))

  ggsave(g, file = paste0(output_dir, "/lifetime-dataset-graphs.pdf"), height = 5, width = 12)
}

make_plot()
make_table()
