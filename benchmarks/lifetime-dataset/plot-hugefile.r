#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
# library(ggpmisc)
library(scales)
library(grid)
library(ff)
library(ffbase)
library(matrixStats)
library(gridExtra)


theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1.6)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(size = rel(1.2)),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            # panel.spacing.y = unit(-1, "lines"),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.text = element_text(margin = margin(r = 10)),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(5, 10, 2, 2), "mm"),
            strip.background = element_blank(),
            strip.text = element_text(face = "bold", hjust = 0, vjust = 0.3)
        ))
}

scientific_10 <- function(x) {
    parse(
        text = gsub(
            "e\\+*", " %*% 10^",
            scales::scientific_format()(x)
        )
    )
}

get_averages <- function(df, cnames) {
    colnames(df) <- cnames
    do.call(
        data.frame,
        aggregate(
            . ~ structure + n + tau + delta + contacts,
            df,
            function(x) c(mean = mean(x), sd = sd(x))
        )
    )
}

input_file <- "benchmarks/lifetime/lifetime-results.csv"
input_file_sorted <- "benchmarks/lifetime/lifetime-results-sorted.csv"
output_file <- "benchmarks/lifetime/lifetime-results-aggregated.csv"
output_dir <- "benchmarks/lifetime/output/"


agg <- NULL

if (file.exists(output_file)) {
    agg <- read.csv2.ffdf(
        file = output_file,
        colClasses = c(
            "factor",
            rep("numeric", 2),
            "factor",
            rep("numeric", 15)
        )
    )
} else {
    if (!file.exists(input_file_sorted)) {
        dir.create("tmp", showWarnings = FALSE)
        system(paste0(
            "sort -t';' -n -k5,4 -Ttmp ",
            input_file,
            " | sort -t';' --stable -k1,1 -o",
            input_file_sorted,
            " -Ttmp"
        ))
        unlink("tmp", recursive = TRUE)
    }
    rest <- NULL
    cnames <- c(
        "structure",
        "n",
        "tau",
        "delta",
        "contacts",
        "op1_time",
        "op1_seq_acc",
        "op1_ran_acc",
        "op1_seq_wri",
        "op1_ran_wri",
        "op1_hits",
        "op2_time",
        "op2_seq_acc",
        "op2_ran_acc",
        "op2_hits",
        "op3_time",
        "op3_seq_acc",
        "op3_ran_acc",
        "op3_hits"
    )
    con <- file(input_file_sorted, open = "r")
    while (length(lines <- readLines(con, n = 100000, warn = FALSE)) > 0) {
        df <- tryCatch({
                m <- do.call(rbind, strsplit(lines, ";"))
                cbind(
                    factor(m[, 1]),
                    as.data.frame(apply(m[, 2:ncol(m)], 2, as.numeric))
                )
            },
            error = function(cond) {
                message(cond)
                return(NULL)
            },
            warning = function(cond) {
                message(cond)
                return(NULL)
            }
        )

        if (!is.null(df)) {
            i <- nrow(df)
            last <- df[i, 1:5]
            while (all(df[i, 1:5] == last)) {
                i <- i - 1
            }
            rest <- df[(i + 1):nrow(df), ]
            df <- df[1:i, ]

            agg <- ffdfappend(
                agg,
                as.ffdf(get_averages(df, cnames)),
                adjustvmode = FALSE
            )
        }
        print(c(df[i, 5], df[i, 4]))

    }
    agg <- ffdfappend(
        agg,
        as.ffdf(get_averages(rest, cnames)),
        adjustvmode = FALSE
    )
    close(con)

    write.csv2.ffdf(agg, file = output_file)
}

g <- ggplot(
    agg[seq(1, nrow(agg), by = 100), ],
    aes(
        x = contacts,
        y = op1_time.mean,
        color = factor(structure)
        # color = factor(delta),
        # linetype = factor(structure)
    )
) +
    scale_x_continuous(labels = number) +
    scale_y_continuous(labels = number) +
    # geom_errorbar(aes(
    #     ymin = op1_time.mean - op1_time.sd,
    #     ymax = op1_time.mean + op1_time.sd
    # ), width = 10, position = position_dodge(10), linetype = "solid") +
    geom_line(size = 1) +
    # geom_abline(slope = 1, intercept = 0) +
    # scale_color_manual(values = c("red", "blue")) +
    # scale_linetype_manual(values = c("dotted", "dashed")) +
    labs(
        x = "Number of contacts",
        y = "op1_time"
    ) +
    # guides(color = "none", linetype = "none") +
    theme_Publication()

ggsave("lifetime-time.pdf", g)


# g1 <- ggplot(
#     agg[seq(1, nrow(agg), by = 1000), ],
#     aes(
#         x = contacts,
#         y = ran_wri.mean,
#         color = factor(delta),
#         linetype = factor(structure)
#     )
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_errorbar(aes(
#         ymin = ran_wri.mean - ran_wri.sd,
#         ymax = ran_wri.mean + ran_wri.sd
#     ), width = 10, position = position_dodge(10), linetype = "solid") +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     scale_color_manual(values = c("red", "blue")) +
#     scale_linetype_manual(values = c("dotted", "dashed")) +
#     labs(
#         x = "Number of contacts",
#         y = "op1_ran_acc"
#     ) +
#     guides(color = "none", linetype = "none") +
#     theme_Publication()

# g1


# d <- read.csv2(input_file, dec = ".")
# colnames(d) <-
#     cnames <- c(
#         "structure",
#         "contacts",
#         "delta",
#         "space",
#         "seq_wri",
#         "ran_wri",
#         "op1_time", "op2_time", "op3_time",
#         "op1_seq_acc", "op2_seq_acc", "op3_seq_acc",
#         "op1_ran_acc", "op2_ran_acc", "op3_ran_acc",
#         "op1_hits", "op2_hits", "op3_hits"
#     )

# d$structure <- as.factor(d$structure)
# d$delta <- as.factor(d$delta)

# # d2 <- d[d$structure != "ttc_splitjoin2", ]

# g1 <- ggplot(
#     d[d$contacts < 10000, ],
#     aes(
#         x = contacts,
#         y = op1_time,
#         color = structure
#     )
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     facet_grid(delta ~ .) +
#     theme_Publication()

# g1

