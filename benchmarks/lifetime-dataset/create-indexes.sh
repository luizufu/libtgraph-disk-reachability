#!/bin/bash

indir='/home/luiz/Documents/datasets/temporal-graphs/dynamicnetworks'
outdir='/hdd/luiz/experiments/lifetime-dataset'
exec='/home/luiz/Documents/codes/cpp/libtgraph-disk-reachability/benchmarks/lifetime-dataset/driver'
# datafile='/home/luiz/Documents/codes/cpp/libtgraph-disk-reachability/benchmarks/lifetime-dataset/datasets.list'
cachefile='/home/luiz/Documents/codes/cpp/libtgraph-disk-reachability/benchmarks/lifetime-dataset/cache'
outfile="/home/luiz/Documents/codes/cpp/libtgraph-disk-reachability/benchmarks/lifetime-dataset/creation-results-test.csv"
logfile='/home/luiz/Documents/codes/cpp/libtgraph-disk-reachability/benchmarks/lifetime-dataset/log'

mkdir -p $outdir
cd $outdir || exit
touch $cachefile

# rm -f tmp.sorting
# datasets=($(cat $datafile))
# i=1
# for d in ${datasets[@]}; do
#     header=($(head -1 "$indir/$d" | tr ' ' '\n'))
#     if (($(echo "l(${header[2]}^2 * ${header[3]})/l(2) < 64" | bc -l))); then
#         score=$(echo "${header[2]}^2 * ${header[3]}" | bc -l)
#         echo "$i $score"  >> tmp.sorting
#     fi
#     i=$((i + 1))
# done
# indices=($(sort -n -k2 tmp.sorting | cut -d ' ' -f1))
# rm -f tmp.sorting

# for i in "${indices[@]}"; do
#     echo "${datasets[$i]}" >> sorted-datasets.list
# done

datasets=(
	"copresence-InVS13.edges"              # 95 20129 394247
	"ia-enron-employees.edges"             # 150 14832 47088
	"contacts-prox-high-school-2013.edges" # 327 7375 188508
	# "email-dnc.edges"                      # 1867 18682 37421
	# "fb-messages.edges"                    # 1899 58911 59835
	# "ia-movielens-user2tags-10m.edges"     # 6528 81921 95575
	# "ia-chess.edges"                       # 7301 100 65053
	# "ia-escorts-dynamic.edges"             # 10106 1913 50619
	# "ca-cit-HepTh.edges"                   # 22908 219 2673133
)

indexes=(
	# "expanded"
	"expanded_filtered"
	# "compressed1-simple8b"
	# "compressed1-simple8b_rle"
	# "compressed2-simple8b"
	"compressed2-simple8b_rle"
	# "compressed2-filtered-simple8b_rle"
	# "compressed3-simple8b"
	# "compressed3-simple8b_rle"
	# "compressed4-simple8b"
	# "compressed6-simple8b"
	"compressed6-simple8b_rle"
	# "compressed7-simple8b"
)

# function sorted_datasets() {
# }

function shuffle() {
	head -1 "$1" >"$1.new"
	tail -n +2 "$1" | shuf >>"$1.new"
	mv "$1.new" "$1"
}

for i in $(seq 1 10); do
	for dataset in "${datasets[@]}"; do
		shuffle "$indir/$dataset"
		for index in "${indexes[@]}"; do
			if grep -q "$i-$index-$dataset" $cachefile; then
				continue
			fi

			echo "$exec
                    --prefix $i
                    --index_type $index
                    --index_filename \"$outdir/$index-$dataset\"
                    --dataset_filename \"$indir/$dataset\"
                    >> $outfile
                    2>> $logfile"

			$exec \
				--prefix "$i" \
				--index_type "$index" \
				--index_filename "$outdir/$index-$dataset" \
				--dataset_filename "$indir/$dataset" \
				>>$outfile 2>>$logfile
			rm -f "$outdir/$index-$dataset"

			if [ $? -eq 0 ]; then
				echo "$i-$index-$dataset" >>$cachefile
			else
				break
			fi
		done
	done
done
