library(ggplot2)
library(ggpubr)
library(ggpmisc)
library(scales)

theme_publication <- function(base_size = 8, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.8),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(10, 5, 5, 5), "mm"),
            strip.background = element_rect(colour = "#f0f0f0", fill = "#f0f0f0"),
            strip.text = element_text(face = "bold")
        ))
}

y_labels <- c(
    "Time (ns)",
    "Disk pages",
    "Disk usage",
    "Disk sequential accesses",
    "Disk sequential writes",
    "Disk random accesses",
    "Disk random writes",
    "Cache size",
    "Cache hits",
    "Binary searches",
    "Sequential searches",
    "Tuple inserts",
    "Tuple updates",
    "Tuple erases",
    "Disk sequential + random accesses",
    "Disk sequential + random writes",
    "Binary + sequential searches",
    "Tuple inserts - erases",
    "Tuple inserts + updates + erases"
)

firstup <- function(x) {
    substr(x, 1, 1) <- toupper(substr(x, 1, 1))
    x
}

make_plot <- function(str, exec, y_lab, n, v) {
    df <- data.frame(
        x = seq(from = 1, to = n, length.out = length(v)),
        y = v
    )
    percentage_discarded <- sprintf("%.1f%%", 100 * (n - length(v)) / n)

    g1 <- ggplot(df, aes(x = x, y = y)) +
        geom_point(size = 0.2, alpha = 0.02) +
        labs(
            title = paste0(firstup(str)),
            subtitle = paste0("Execution ", exec),
            x = "Contacts",
            y = y_lab
            # caption = paste(percentage_discarded, "entries discarded")
        ) +
        theme_publication()

    g2 <- ggplot(df, aes(x = x, y = cumsum(y))) +
        geom_line() +
        labs(
            title = paste0(firstup(str)),
            subtitle = paste0("Execution ", exec),
            x = "Contacts",
            y = paste(y_lab, "(Cumulative)")
            # caption = paste(percentage_discarded, "entries discarded")
        ) +
        theme_publication()

    list(g1, g2)
}

extract_data <- function(line,
                         reduce = c("no", "discard", "mean"),
                         length = 0) {
    cols <- unlist(strsplit(line, ";", fixed = T))

    df <- do.call(
        "rbind",
        lapply(
            strsplit(
                strsplit(cols[5], ",", fixed = T)[[1]],
                "|",
                fixed = T
            ),
            as.numeric
        )
    )

    nrow_before <- nrow(df)

    if (nrow_before > length) {
        if (reduce[1] == "discard") {
            df <- df[as.integer(
                seq(
                    from = 1,
                    to = nrow(df),
                    length.out = length
                )
            ), ]
        } else if (reduce[1] == "mean") {
            n <- nrow(df) / length
            df <- aggregate(
                df,
                list(rep(1:(nrow(df) %/% n + 1), each = n, len = nrow(df))),
                mean
            )[-1]
        }
    }

    df <- cbind(
        df,
        df[, 4] + df[, 5],
        df[, 6] + df[, 7],
        df[, 10] + df[, 11],
        df[, 12] - df[, 14],
        df[, 12] + df[, 13] + df[, 14]
    )

    list(
        data_structure = cols[1],
        n = cols[2], tau = cols[3],
        execution = cols[4],
        nrow_before =  nrow_before,
        lifetime = df
    )
}

empty_plots <- function() {
    vector(mode = "list", length = length(y_labels))
}

plot_g <- function(g, output_directory, output_filename) {
    ggsave(paste0(output_directory, "/", output_filename), g)
}

try_plot_graphics <- function(output_directory, plots) {
    if (!is.null(plots)) {
        dir.create(output_directory, showWarnings = FALSE)

        for (i in seq_along(y_labels)) {
            pages <- ggarrange(plotlist = plots[[i]], nrow = 1, ncol = 1)
            # pages <- ggarrange(plotlist = plots[[i]], nrow = 2, ncol = 2)
            fname <- paste0(output_directory, "/", paste0(y_labels[i], ".pdf"))
            ggexport(pages, filename = fname)
        }
    }
}

input_file <- "benchmarks/lifetime2.csv"
output_prefix <- "benchmarks/plots/lifetime3-"

conn <- file(input_file, open = "r")
reduce_method <- "none"
max_points <- 10000
plots <- NULL
last_n <- 0
last_tau <- 0

line <- readLines(conn, n = 1, warn = FALSE)
# while (length(line <- readLines(conn, n = 1, warn = FALSE)) > 0) {
#     cols <- NULL
#     tryCatch({
#         cols <- unlist(strsplit(line, ";", fixed = T))
#     })
#
#     cat(paste(cols[2], cols[3], "\n"))
#     if (cols[2] == 16 && cols[3] == 128) {
#         break
#     }
# }
#
while (length(line <- readLines(conn, n = 1, warn = FALSE)) > 0) {
    data <- tryCatch({
        extract_data(line, reduce = reduce_method, max_points)
    }, warning = function(cond) {
        message("Can not proccess current line")
        return(NULL)
    }, error = function(cond) {
        message("Can not proccess current line")
        return(NULL)
    })

    if (is.null(data)) {
        next
    }

    cat(data$data_structure, " ", data$n, " ", data$tau, "\n")
    if (data$n != 128 | data$data_structure != "ttc3<by_row>") {
        next
    }

    # if (data$n != last_n | data$tau != last_tau) {
    #     try_plot_graphics(paste0(output_prefix, last_n, "-", last_tau), plots)
    #     plots <- empty_plots()
    # }
    direc <- paste0(output_prefix, data$n, "-", data$tau)
    dir.create(direc, showWarnings = FALSE)
    for (i in seq_along(y_labels)) {
        l <- make_plot(
                       data$data_structure,
                       data$execution,
                       y_labels[i],
                       data$nrow_before,
                       data$lifetime[, i]
        )
        plot_g(l[[1]], direc, paste(y_labels[i], ".pdf"))
        plot_g(l[[2]], direc, paste(y_labels[i], "-sum.pdf"))
    }


    # for (i in seq_along(y_labels)) {
    #     plots[[i]] <- c(
    #         plots[[i]],
    #         make_plot(
    #             data$data_structure,
    #             data$execution,
    #             y_labels[i],
    #             data$nrow_before,
    #             data$lifetime[, i]
    #         )
    #     )
    # }

    last_n <- data$n
    last_tau <- data$tau
}

try_plot_graphics(paste0(output_prefix, last_n, "-", last_tau), plots)
close(conn)
