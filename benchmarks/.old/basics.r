library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)

theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(10, 5, 5, 5), "mm"),
            strip.background = element_rect(colour = "#f0f0f0", fill = "#f0f0f0"),
            strip.text = element_text(face = "bold")
        ))
}

data <- read.csv("benchmarks/ttc3.csv", sep = ";")
data <- data[data$data_structure != "" & data$operation != "", ]
data <- data[data$contacts > 128, ]
# data <- data[data$data_structure != "baseline", ]
data$time <- data$time * 1000000

nops <- c("bulkload_creation", "add_contact", "can_reach", "get_journey")
dops <- c("bulk_insertions", "single_insertions", "queries", "queries")
my_formula <- y ~ x

extract_x <- function(d, j) {
    if (j == 1) {
        return(d[, "bulk_insertions"])
    }

    if (j == 2) {
        return(d[, "bulk_insertions"] + (d[, "single_insertions"] / 2))
    }

    return(d[, "bulk_insertions"] + d[, "single_insertions"])
}

for (j in 1:4) {
    d <- data[data$operation == nops[j], ]
    d$samples <- d[, dops[j]]
    d[d$samples == 0, "samples"] <- 1

    plots <- list()
    plots_size <- 0

    d1 <- d[d$time > 0, ]
    if (nrow(d1) > 0) {
        d1$x <- extract_x(d1, j)
        d1$y <- if (j == 1) d1$time else d1$time/d1$samples
        plots_size <- plots_size + 1
        plots[[plots_size]] <- ggplot(
            d1,
            aes(x = x, y = y, color = data_structure)
        ) +
            geom_point() +
            geom_smooth(method = lm, se = FALSE, formula = my_formula) +
            stat_poly_eq(
                formula = my_formula,
                aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                parse = TRUE
            ) +
            scale_x_log10(labels = trans_format("log10", math_format(10^.x))) +
            scale_y_log10(labels = trans_format("log10", math_format(10^.x))) +
            labs(
                title = paste0(
                    toupper(nops[j]),
                    " operation"
                ),
                subtitle = (if(j == 1)
                    "Time to create a data structure of size |C| from scratch"
                else
                    paste0("Time to ",
                    (if(j == 2) "insert a contact into " else "query "),
                    "a data structure of size |C|")),
                x = "Contacts (|C|)",
                y = "Wall clock time (ms)",
                color = "Data structure: "
            ) +
            theme_Publication()
    }

    d1 <- d[d$disk_usage > 0, ]
    if (nrow(d1) > 0) {
        d1$x <- extract_x(d1, j)
        d1$y <- if (j == 1) d1$disk_usage else d1$disk_usage/d1$samples
        plots_size <- plots_size + 1
        plots[[plots_size]] <- ggplot(
            d1, aes(x = x, y = y, color = data_structure)
        ) +
            geom_smooth(method = lm, se = FALSE, formula = my_formula) +
            stat_poly_eq(
                formula = my_formula,
                aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                parse = TRUE
            ) +
            scale_x_log10(labels = trans_format("log10", math_format(10^.x))) +
            scale_y_log10(labels = trans_format("log10", math_format(10^.x))) +
            labs(
                title = paste0(
                    toupper(nops[j]),
                    " operation"
                ),
                subtitle = (if(j == 1)
                    "Space to store a data structure of size |C|"
                else
                    paste0("Increasing space after ",
                    (if(j == 2) "inserting a contact into " else "querying "),
                    "a data structure of size |C|")),
                x = "Contacts (|C|)",
                y = "Space (bytes)",
                color = "Data structure: "
            ) +
            theme_Publication()
    }

    d1 <- melt(
        d[, c(
            unique(dops), "data_structure", "samples",
            "disk_sequential_accesses", "disk_random_accesses",
            "disk_sequential_writes", "disk_random_writes"
        )],
        id.vars = c(unique(dops), "data_structure", "samples")
    )
    d1$access <- ifelse(grepl("accesses", d1$variable), "Read", "Write")
    d1$type <- ifelse(grepl("sequential", d1$variable), "sequential", "random")
    d1 <- d1[d1$value > 0, ]

    if (nrow(d1) > 0) {
        d1$x <- extract_x(d1, j)
        d1$y <- if (j == 1) d1$value else d1$value/d1$samples
        plots_size <- plots_size + 1
        plots[[plots_size]] <- ggplot(d1, aes(
            x = x, y = y, color = data_structure, linetype = type
        )) +
            geom_smooth(method = lm, se = FALSE, formula = my_formula) +
            stat_poly_eq(
                formula = my_formula,
                aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                parse = TRUE
            ) +
            scale_x_log10(labels = trans_format("log10", math_format(10^.x))) +
            scale_y_log10(labels = trans_format("log10", math_format(10^.x))) +
            labs(
                title = paste0(
                    toupper(nops[j]),
                    " operation"
                ),
                subtitle = (if(j == 1)
                    "Number of I/O's to create a data structure of size |C| from scratch"
                else
                    paste0("Number of I/O's to ",
                    (if(j == 2) "insert a contact into " else "query "),
                    "a data structure of size |C|")),
                x = "Contacts (|C|)",
                y = "I/O's",
                color = "Data structure: ",
                linetype = "I/O type: "
            ) +
            facet_grid(cols = vars(access)) +
            theme_Publication()
    }

    d1 <- melt(
        d[, c(
            unique(dops), "data_structure", "samples",
            "binary_searches", "sequential_searches"
        )],
        id.vars = c(unique(dops), "data_structure", "samples")
    )
    d1$variable <-
        ifelse(d1$variable == "binary_searches", "binary", "sequential")
    d1 <- d1[d1$value > 0, ]

    if (nrow(d1) > 0) {
        d1$x <- extract_x(d1, j)
        d1$y <- if (j == 1) d1$value else d1$value/d1$samples
        plots_size <- plots_size + 1
        plots[[plots_size]] <- ggplot(d1, aes(
            x = x, y = y, color = data_structure, linetype = variable
        )) +
            geom_smooth(method = lm, se = FALSE, formula = my_formula) +
            stat_poly_eq(
                formula = my_formula,
                aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                parse = TRUE
            ) +
            scale_x_log10(labels = trans_format("log10", math_format(10^.x))) +
            scale_y_log10(labels = trans_format("log10", math_format(10^.x))) +
            labs(
                title = paste0(
                    toupper(nops[j]),
                    " operation"
                ),
                subtitle = (if(j == 1)
                    "Number of search operations to create a data structure of size |C| from scratch"
                else
                    paste0("Number of search operations to ",
                    (if(j == 2) "insert a contact into " else "query "),
                    "a data structure of size |C|")),
                x = "Contacts (|C|)",
                y = "Search operations",
                color = "Data structure: ",
                linetype = "Search type: "
            ) +
            theme_Publication()
    }

    d1 <- melt(
        d[, c(
            unique(dops), "data_structure", "samples",
            "tree_insertions", "tree_erases"
        )],
        id.vars = c(unique(dops), "data_structure", "samples")
    )
    d1$variable <-
        ifelse(d1$variable == "tree_insertions", "insert", "delete")
    d1 <- d1[d1$value > 0, ]

    if (nrow(d1) > 0) {
        d1$x <- extract_x(d1, j)
        d1$y <- if (j == 1) d1$value else d1$value/d1$samples
        plots_size <- plots_size + 1
        plots[[plots_size]] <- ggplot(d1, aes(
            x = x, y = y, color = data_structure, linetype = variable
        )) +
            geom_smooth(method = lm, se = FALSE, formula = my_formula) +
            stat_poly_eq(
                formula = my_formula,
                aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
                parse = TRUE
            ) +
            scale_x_log10(labels = trans_format("log10", math_format(10^.x))) +
            scale_y_log10(labels = trans_format("log10", math_format(10^.x))) +
            labs(
                title = paste0(
                    toupper(nops[j]),
                    " operation"
                ),
                subtitle = (if(j == 1)
                    "Number of update operations to create a data structure of size |C| from scratch"
                else
                    paste0("Number of update operations to ",
                    (if(j == 2) "insert a contact into " else "query "),
                    "a data structure of size |C|")),
                x = "Contacts (|C|)",
                y = "Update in r-tuple collections",
                color = "Data structure: ",
                linetype = "Update type: "
            ) +
            theme_Publication()
    }

    pages <- ggarrange(plotlist = plots, nrow = 1, ncol = 1)
    ggexport(pages, filename = paste0(nops[j], ".pdf"))
}
