#include <array>

#include <libgraph-disk-structures/adj-list.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libgraph-disk-structures/edge-grid.hxx>
#include <libtgraph-disk-reachability/baseline.hxx>
#include <libtgraph-disk-reachability/ttc.hxx>
#include <libtgraph-disk-reachability/ttc2.hxx>
#include <libtgraph-disk-reachability/ttc3.hxx>
#include <libtgraph-disk-reachability/ttc4.hxx>

#include <vector>
#include <algorithm>
#include <benchmarks/basics/type_list.hxx>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <type_traits>
#include <chrono>

constexpr size_t page_size = 4096;
constexpr size_t cache_pages = 1;

struct configuration
{
    uint32_t n = 0;
    uint32_t tau = 0;
    uint32_t delta = 1;
    uint32_t executions = 0;
};

struct entry
{
    double time = 0;
    double disk_pages = 0;
    double disk_usage = 0;
    double disk_sequential_accesses = 0;
    double disk_sequential_writes = 0;
    double disk_random_accesses = 0;
    double disk_random_writes = 0;
    double cache_size = 0;
    double cache_hits = 0;
    double binary_searches = 0;
    double sequential_searches = 0;
    double tuple_inserts = 0;
    double tuple_updates = 0;
    double tuple_erases = 0;
};

struct lifetime
{
    uint32_t execution_id = 0;
    std::vector<entry> data;
};

struct benchmark
{
    std::string data_structure;
    configuration config = {};
    std::vector<lifetime> entries;
};

using namespace tgraph_disk_reachability;
using namespace graph_disk_structures;

using structures =
    type_list<baseline<page_size>, ttc<adj_matrix, page_size>, ttc2<page_size>,
              ttc3<by_row, page_size>, ttc3<by_column, page_size>>;

auto make_complete_graph(uint32_t n, uint32_t t) -> std::vector<contact>;

template<typename Index>
auto run(const configuration& config, std::vector<contact>* contacts)
    -> benchmark;

auto main() -> int
{
    std::cout << "data_structure;"
              << "n;"
              << "tau;"
              << "execution;"
              << "lifetime" << std::endl;

    std::vector<std::pair<uint32_t, uint32_t>> parameters = {
        /* {4, 4},      {4, 8},      {8, 4},      {8, 8},      {4, 16}, */
        /* {16, 4},     {8, 16},     {16, 8},     {16, 16},    {4, 32}, */
        {32, 4},     {8, 32},     {32, 8},     {16, 32},    {32, 16},
        {32, 32},    {4, 64},     {64, 4},     {8, 64},     {64, 8},
        {16, 64},    {64, 16},    {32, 64},    {64, 32},    {64, 64},
        {4, 128},    {128, 4},    {8, 128},    {128, 8},    {16, 128},
        {128, 16},   {32, 128},   {128, 32},   {64, 128},   {128, 64},
        {128, 128},  {4, 256},    {256, 4},    {8, 256},    {256, 8},
        {16, 256},   {256, 16},   {32, 256},   {256, 32},   {64, 256},
        {256, 64},   {128, 256},  {256, 128},  {256, 256},  {4, 512},
        {512, 4},    {8, 512},    {512, 8},    {16, 512},   {512, 16},
        {32, 512},   {512, 32},   {64, 512},   {512, 64},   {128, 512},
        {512, 128},  {256, 512},  {512, 256},  {512, 512},  {4, 1024},
        {1024, 4},   {8, 1024},   {1024, 8},   {16, 1024},  {1024, 16},
        {32, 1024},  {1024, 32},  {64, 1024},  {1024, 64},  {128, 1024},
        {1024, 128}, {256, 1024}, {1024, 256}, {512, 1024}, {1024, 512},
        {1024, 1024}};

    for(auto parameter: parameters)
    {
        configuration config = {
            .n = parameter.first,
            .tau = parameter.second,
            .delta = 1,
            .executions = 2,
        };

        auto contacts = make_complete_graph(config.n, config.tau);

        for_<0, size_v<structures>>(
            [&](auto i)
            {
                using st = typename get<i, structures>::type;
                std::cerr << type_name<st>() << std::endl;
                auto b = run<st>(config, &contacts);

                for(const auto& lt: b.entries)
                {
                    std::cout << b.data_structure << ';' << b.config.n << ';'
                              << b.config.tau << ';' << lt.execution_id << ';';

                    std::cout << lt.data[0].time << '|'
                              << lt.data[0].disk_pages << '|'
                              << lt.data[0].disk_usage << '|'
                              << lt.data[0].disk_sequential_accesses << '|'
                              << lt.data[0].disk_sequential_writes << '|'
                              << lt.data[0].disk_random_accesses << '|'
                              << lt.data[0].disk_random_writes << '|'
                              << lt.data[0].cache_size << '|'
                              << lt.data[0].cache_hits << '|'
                              << lt.data[0].binary_searches << '|'
                              << lt.data[0].sequential_searches << '|'
                              << lt.data[0].tuple_inserts << '|'
                              << lt.data[0].tuple_updates << '|'
                              << lt.data[0].tuple_erases;

                    for(uint32_t i = 1; i < lt.data.size(); ++i)
                    {
                        std::cout << ',' << lt.data[i].time << '|'
                                  << lt.data[i].disk_pages << '|'
                                  << lt.data[i].disk_usage << '|'
                                  << lt.data[i].disk_sequential_accesses << '|'
                                  << lt.data[i].disk_sequential_writes << '|'
                                  << lt.data[i].disk_random_accesses << '|'
                                  << lt.data[i].disk_random_writes << '|'
                                  << lt.data[i].cache_size << '|'
                                  << lt.data[i].cache_hits << '|'
                                  << lt.data[i].binary_searches << '|'
                                  << lt.data[i].sequential_searches << '|'
                                  << lt.data[i].tuple_inserts << '|'
                                  << lt.data[i].tuple_updates << '|'
                                  << lt.data[i].tuple_erases;
                    }

                    std::cout << std::endl;
                }
            });
    }
}

auto generator() -> std::mt19937&;

template<typename Index>
auto run(const configuration& config, std::vector<contact>* contacts)
    -> benchmark
{
    benchmark b {
        .data_structure =
            std::is_same_v<Index, baseline<page_size>>        ? "baseline"
            : std::is_same_v<Index, ttc<adj_list, page_size>> ? "ttc<adj_list>"
            : std::is_same_v<Index, ttc<adj_matrix, page_size>>
                ? "ttc<adj_matrix>"
            : std::is_same_v<Index, ttc<edge_grid, page_size>>
                ? "ttc<edge_grid>"
            : std::is_same_v<Index, ttc2<page_size>>         ? "ttc2<adj_list>"
            : std::is_same_v<Index, ttc3<by_row, page_size>> ? "ttc3<by_row>"
            : std::is_same_v<Index, ttc3<by_column, page_size>>
                ? "ttc3<by_column>"
            : std::is_same_v<Index, ttc4<adj_list, page_size>>
                ? "ttc4<adj_list>"
            : std::is_same_v<Index, ttc4<adj_matrix, page_size>>
                ? "ttc4<adj_matrix>"
            : std::is_same_v<Index, ttc4<adj_matrix, page_size>>
                ? "ttc4<edge_grid>"
                : "",
        .config = config,
    };

    auto now = []()
    {
        return std::chrono::high_resolution_clock::now();
    };

    auto diffnow = [&now](const auto& start)
    {
        std::chrono::duration<double, std::milli> diff = now() - start;
        return diff.count();
    };

    for(uint32_t i = 0; i < config.executions; ++i)
    {
        std::shuffle(contacts->begin(), contacts->end(), generator());

        std::filesystem::remove("disk_memory");
        disk::allocator<page_size> alloc("disk_memory", cache_pages);

        auto h = Index::create(config.n, config.tau, config.delta, &alloc);

        lifetime lt;
        lt.execution_id = i;

        for(auto& contact: *contacts)
        {
            Index::reset_benchmark();
            alloc.reset_benchmark();

            auto start = now();
            Index::add_contact(&h, contact, &alloc);
            auto time = diffnow(start);

            entry e;
            e.time = time;
            e.disk_pages = alloc.pages();
            e.disk_usage = alloc.size();
            e.disk_sequential_accesses = alloc.sequential_accesses();
            e.disk_sequential_writes = alloc.sequential_writes();
            e.disk_random_accesses = alloc.random_accesses();
            e.disk_random_writes = alloc.random_writes();
            e.cache_size = alloc.cache().size();
            e.cache_hits = alloc.cache().hits();
            e.binary_searches = Index::binary_searches();
            e.sequential_searches = Index::sequential_searches();
            e.tuple_inserts = Index::tuple_inserts();
            e.tuple_updates = Index::tuple_updates();
            e.tuple_erases = Index::tuple_erases();

            lt.data.push_back(e);
        }

        b.entries.push_back(lt);
    }

    std::filesystem::remove("disk_memory");
    return b;
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto make_complete_graph(uint32_t n, uint32_t tau) -> std::vector<contact>
{
    std::vector<contact> contacts;

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            for(uint32_t t = 0; t < tau; ++t)
            {
                contacts.push_back({u, v, t});
            }
        }
    }

    return contacts;
}
