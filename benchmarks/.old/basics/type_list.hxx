#pragma once

#include <string_view>
#include <cstdint>

// forward
template<typename... TS>
struct type_list
{
};

template<typename H>
struct size_;

template<template<typename...> class TL, typename... Ts>
struct size_<TL<Ts...>>
{
    constexpr static auto value = sizeof...(Ts);
};

template<typename H>
constexpr size_t size_v = size_<H>::value;

template<size_t idx, class... Types>
class extract
{
    static_assert(idx < sizeof...(Types), "index out of bounds");

    template<size_t i, size_t n, class... Rest>
    struct extract_impl;

    template<size_t i, size_t n, class T, class... Rest>
    struct extract_impl<i, n, T, Rest...>
    {
        using type = typename extract_impl<i + 1, n, Rest...>::type;
    };

    template<size_t n, class T, class... Rest>
    struct extract_impl<n, n, T, Rest...>
    {
        using type = T;
    };

public:
    using type = typename extract_impl<0, idx, Types...>::type;
};

template<size_t idx, class TypeList>
struct get;

template<size_t idx, template<class...> class TypeList, class... Types>
struct get<idx, TypeList<Types...>>
{
    using type = typename extract<idx, Types...>::type;
};

template<size_t Start, size_t End, typename F>
constexpr void for_(F&& f)
{
    if constexpr(Start < End)
    {
        f(std::integral_constant<size_t, Start>());
        for_<Start + 1, End>(f);
    }
}

template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}
