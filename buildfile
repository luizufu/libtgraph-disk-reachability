./: {*/ -build/ -ssd/} doc{README.md} manifest
./: benchmarks/: include = false

# Don't install tests.
#
tests/: install = false
benchmarks/: install = false
