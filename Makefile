all:
	b

.PHONY: test clean db fmt update

test:
	b test

clean:
	b clean
	rm -rf cppcheck.out clang-tidy.out

db: clean
	b -v |& compiledb

fmt: .clang-format
	find . -regex '.*\.\(cxx\|hxx\)' -exec clang-format -style=file -i {} \;

update:
	bdep fetch
	bdep sync -u -r
	b clean
	rm -rf cppcheck.out clang-tidy.out
	b -v |& compiledb
