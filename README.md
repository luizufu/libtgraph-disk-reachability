# libgraph-disk-structures

C++ library

## dependencies

cppcoro
libgraph-disk-structures
boost (for tests)


## after commands

    bdep init -C {build-out}/libtgraph-disk-reachability-clang @clang cc config.cxx=clang++ \
        config.cxx.coptions=-stdlib=libc++ -march=native                             \
        config.cc.poptions=-I{build-unpkg}/include                       \
        config.cc.loptions=-L{build-unpkg}/lib

where {build-out} can be relative and {build-unpkg} not
