n <- 4
tau <- 8
w <- 32

f1 <- function(u, t, v) (u * tau + t) * n + v
f1r <- function(u, t, v) (u * tau + (tau - (t + 1))) * n + v
f2 <- function(u, t, v) (v * n + u) * tau + t
f2r <- function(u, t, v) (v * n + u) * tau + (tau - t - 1)

f22r <- function(u, t, v) n * tau * (v + 1) - (tau * u + t + 1)
f3 <- function(u, t, v) (t * n + u) * n + v
f4 <- function(u, t, v) (u * n + v) * tau + t
f4r <- function(u, t, v) (u * n + v) * tau + tau - t - 1

f5 <- function(u, v) ((u * n + v) * ceiling(tau / w))

printf <- function() {
    for (u in 0:(n - 1)) {
        for (t in 0:(tau - 1)) {
            cat(u, "-", t, "    ")
            for (v in 0:(n - 1)) {
                cat(format(f4r(u, t, v), width = 2), " ")
            }
            cat("\n")
        }
    }
}

# z <- 2

# g1 <- function(u, tminus, z) u * ceiling(tau / z) + floor(tminus / z)
# g1r <- function(u, tminus, z) u * ceiling(tau / z) + floor((tau - tminus - 1) / z)
# printg <- function() {
#     cat("   |  ")
#     for (tminus in 0:(tau - 1)) {
#         cat(format(tminus, width = 2), "       ")
#     }
#     cat("\n-----")
#     for (tminus in 0:(tau + 1)) {
#         cat("-------")
#     }
#     cat("\n")
#     for (u in 0:(n - 1)) {
#         cat(format(u, width = 2), "|  ")
#         for (t in 0:(tau - 1)) {
#             bid <- g1r(u, t, z)
#             i <- (tau - t - 1) %% z
#             cat(format(bid, width = 2), "-", format(i, width = 2), "  ")
#         }
#         cat("\n")
#     }
# }
# printg()
printf()
